module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: '91.215.136.254',
      username: 'root',
      // pem: './path/to/pem'
      password: 'MBc63AMG'
      // or neither for authenticate from ssh-agent
    }
  },

  meteor: {
      name: 'kipyatcom',
      path: '..',
      volumes: {
        '/photodocs':'/photodocs'
      },
      dockerImage: 'abernix/meteord:base',
      servers: {
        one: {}
      },
      buildOptions: {
        serverOnly: true,
      },
      env: {
        ROOT_URL: 'http://91.215.136.254/',
        MONGO_URL: 'http://91.215.136.254:3001/'
      },
      //dockerImage: 'kadirahq/meteord'
      deployCheckWaitTime: 120,
      enableUploadProgressBar: true
  },

  mongo: {
    port: 27017,
    version: '3.4.1',
    servers: {
      one: {}
    }
  }
};
