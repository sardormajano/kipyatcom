import React, {Component} from 'react';
import 'cropperjs/dist/cropper.css';

import {DocsCollection} from '/api/docs';
import {mfUploadFile, dataURLtoBlob, fileToBinaryString} from '/client/lib/general';

export default class Select extends Component {
  componentDidMount() {
    const width = this.props.width || 430,
          height = this.props.height || 495;

    this.cropper = new Cropper(this.image, {
      cropBoxResizable: true,
      aspectRatio: width / height,
      minContainerWidth: width,
      minContainerHeight: height,
      viewMode: 0,
      ready: () => {
        this.cropper.setCropBoxData({
           width: width || 430,
           height: height || 495,
        });
      }
    });
  }

  componentDidUpdate() {
    const {context, stateName, width, height} = this.props,
          {getLink, state} = context;

    if(!state[stateName]) return ;

    if(this.currentImg === state[stateName])
      return;

    this.cropper.replace(state[stateName]);
    this.currentImg = state[stateName];

    this.cropper.setCropBoxData({
       width: width || 430,
       height: height || 495,
    });
  }

  saveHandler(e) {
    const {context, stateName} = this.props;

    const currentCanvas = this.cropper.getCroppedCanvas({
      width: this.props.width || 430,
      height: this.props.height || 495,
    });

    currentCanvas.toBlob(blob => {
      fileToBinaryString(blob, binaryString => {
        context.setState({
          [stateName]: currentCanvas.toDataURL(),
          [`binary${stateName.capitalize()}`]: binaryString
        });
      });
    });
  }

  render() {
    const {id, context, stateName} = this.props,
          {getLink, state} = context;

    return (
      <div
        className="modal fade"
        id={this.props.id}
        role="dialog"
        aria-labelledby="modalLabel"
        tabIndex={-1}
      >
        <div
          className="modal-dialog modal-lg"
          role="document"
        >
          <div
            className="modal-content"
            style={{width: `${this.props.width + 30}px`}}
          >
            <div className="modal-header">
              <h5 className="modal-title" id="modalLabel"><b>Отредактировать фото</b></h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div className="modal-body">
              <div className="img-container">
                <img
                  ref={image => this.image = image}
                  src={state[stateName]}
                  alt="Picture"
                  width={this.props.width}
                  height={this.props.height}
                />
              </div>
            </div>
            <div className="modal-footer">
              <button
                onClick={this.saveHandler.bind(this)}
                type="button"
                className="btn btn-default"
                data-dismiss="modal"
              >
                Сохранить
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
