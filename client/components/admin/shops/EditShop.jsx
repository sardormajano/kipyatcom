import React, {Component} from 'react';
import {Link} from 'react-router';

import Select from '../../stateless/Select';
import Select2 from '../../stateless/Select2';
import FormGroup from '../../stateless/FormGroup';
import TextArea from '../../stateless/TextArea';

import CropperModal from '../CropperModal';
import EditableGallery from '../EditableGallery';

export default class EditShop extends Component {
  render() {
    const {context} = this.props,
          {cover, cover2, position, _id} = context.state,
          {shops, tags} = context.props,
          filteredShops = shops.filter(shop => shop.position === position && shop._id !== _id),
          filteredTags = tags.filter(t => t.category === 2);

    return (
      <div>
        <Link
          to='/admin/shops'
          className='btn btn-warning'
        >
          Вернуться к списку заведений
        </Link>
        <br /><hr /><br />
        <div className="row">
          <div className="col-sm-4 text-center">
            <div className="row">
              Лого<br/><br/>
              <a href="#" className="thumbnail">
                <img
                  width={150}
                  src={cover}
                  alt="..." />
              </a>
              <a
                href="#"
                className="btn btn-primary"
                onClick={(e) => {context.changeLogoInput.click()}}
              >
                <i className="fa fa-plus"/>{' '}Изменить
              </a>
              <input
                style={{display: 'none'}}
                type='file'
                onChange={context.changeCoverHandler.bind(context)}
                ref={cli => context.changeLogoInput = cli}
                multiple
              />
            </div>
            <hr />
            <div className="row" style={{display: context.state.isMain ? '' : 'none'}}>
              Лого для главной страницы<br/><br/>
              <a href="#" className="thumbnail">
                <img
                  width={150}
                  src={cover2}
                  alt="..." />
              </a>
              <a
                href="#"
                className="btn btn-primary"
                onClick={(e) => {context.changeLogo2Input.click()}}
              >
                <i className="fa fa-plus"/>{' '}Изменить
              </a>
              <input
                style={{display: 'none'}}
                type='file'
                onChange={context.changeCover2Handler.bind(context)}
                ref={cl2i => context.changeLogo2Input = cl2i}
                multiple
              />
            </div>
          </div>
          <div className="col col-sm-8">
            <FormGroup
              context={context}
              stateName='name'
              label='Название заведения:'
            />
            <div className="form-group">
              <label>Тип заведения (тэги):</label>
              <Select2
                placeholder='Выберите тип(ы)'
                context={context}
                options={filteredTags}
                subscription='tags'
                stateName='tags'
              />
            </div>
            <FormGroup
              context={context}
              stateName='type2'
              label='Тип заведения:'
            />
            <FormGroup
              context={context}
              stateName='address'
              label='Адрес:'
            />
            <FormGroup
              context={context}
              stateName='phone'
              label='Телефоны:'
            />
            <FormGroup
              context={context}
              stateName='kitchen'
              label='Кухня:'
            />
            <FormGroup
              context={context}
              stateName='bill'
              label='Средний чек:'
            />
            <FormGroup
              context={context}
              stateName='workHours'
              label='Режим работы:'
            />
            <FormGroup
              context={context}
              stateName='website'
              label='Вебсайт:'
            />
            <hr />
            <FormGroup
              context={context}
              stateName='menu'
              label='Меню'
            />
            <input
              ref={pdfLoader => this.pdfLoader = pdfLoader}
              type="file"
              style={{display: 'none'}}
              onChange={context.addMenu.bind(context)}
            />
            <a
              onClick={(e) => {e.preventDefault(); this.pdfLoader.click()}}
              href="#"
              className="btn btn-primary"
            >
              Загрузить PDF
            </a>
            <hr />
          </div>
        </div>
        <div className="row">
          <TextArea
            label='Краткое описание:'
            stateName='description'
            context={context}
          />
        </div>
        <div className="form-group">
          <div className="checkbox">
            <label>
              <br/>
              <input
                type="checkbox"
                checked={context.state.isMain}
                onChange={e => context.setState({isMain: e.currentTarget.checked})}
              /> На главную
            </label>
          </div>
        </div>
        <div style={{display: context.state.isMain ? '':'none'}}>
          <FormGroup
            context={context}
            stateName='position'
            label='Позиция:'
          />
          <div
            style={{display: filteredShops.length ? '':'none'}}
            className="alert alert-danger"
            role="alert"
          >
            <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true" />
            <span className="sr-only">Error:</span>
            Позиция совпадает с заведением: {filteredShops[0] ? filteredShops[0].name : ''}
          </div>
        </div>
        <hr />
        <EditableGallery context={context} />
        <hr />
        <div className="row">
          <div className={`col col-sm-${context.state.mapCode ? 5 : 12}`}>
            <div className="form-group">
              <label htmlFor="shopNameModal">Карта:</label>
              <input
                type="text"
                className="form-control"
                name="shopNameModal"
                placeholder="Введите код"
                value={context.state.mapCode}
                onChange={e => context.setState({mapCode: e.currentTarget.value})}
              />
            </div>
          </div>
          <div
            className="col col-sm-7"
            dangerouslySetInnerHTML={{ __html: context.state.mapCode }}
            style={{height: context.state.mapCode ? 400 : 'auto'}}
          >
          </div>
        </div>
        <hr />
        <a
          href='#'
          onClick={context.editShopHandler.bind(context)}
          className='btn btn-primary'
        >
          Сохранить изменения
        </a>

        <CropperModal
          width={458}
          height={458}
          id='logo-cropper'
          context={context}
          stateName='cover'
        />
        <CropperModal
          width={215}
          height={100}
          id='logo-cropper2'
          context={context}
          stateName='cover2'
        />
      </div>
    );
  }
}
