import React, {Component} from 'react';
import {Link} from 'react-router';
import {idToName} from '../../../lib/collectionsRelated';

import ConfirmButton from '../../stateless/ConfirmButton';
import FormGroup from '../../stateless/FormGroup';

export default class Shops extends Component {

  componentDidUpdate() {
    const confirmButtons = $(this.cw).find('.confirm-button');

    if(!confirmButtons.length) return ;

    $(confirmButtons).confirmation({
      title: 'Вы уверены?',
      btnOkLabel: 'Да.',
      btnCancelLabel: 'Нет.',
      placement: this.props.placement || 'top',
      onConfirm: () => {

      }
    });
  }

  renderShops(){
    const pageSize = 8,
          {context} = this.props,
          {searchWord, page, pagesCount} = context.state,
          {deleteShop} = context,
          {shops, shopTypes} = context.props,
          offset = (page - 1) * pageSize;

    return shops
      .slice(offset, offset + pageSize)
      .filter(shop => {
        const date = moment.unix(shop.date).format('ll');

        return (new RegExp(searchWord, 'i')).test(shop.name) ||
                (new RegExp(searchWord, 'i')).test(shop.type2)
      })
      .map((shop, index) => {
        return (
        <div
          className="col-xs-18 col-sm-6 col-md-3"
          key={index}
        >
          <div className="thumbnail" style={{height: '420px', maxHeight: '420px'}}>
            <img
              src={shop.cover}
              style={{width: '50%', margin: 'auto'}}
            />
            <div className="caption">
              <h4>{shop.name}</h4><hr />
              <p style={{height: '4.3em'}}>
                <b>Тип:</b> {`${shop.type2}`}<br /><br />
              </p>
              <Link
                to={`/admin/edit-shop/${shop._id}`}
                className="btn btn-sm btn-info"
                role="button"
              >
                Редактировать
              </Link>{' '}
              <ConfirmButton
                itemId={shop._id}
                context={context}
                className='btn-sm confirm-button'
              >
                Удалить
              </ConfirmButton>{' '}
              <a
                href='#'
                className="btn btn-info btn-sm"
                onClick={e => {
                  e.preventDefault();
                  context.toggleVisibility(shop._id);
                }}
              >
                {shop.hidden ?  'Показать' : 'Скрыть'}
              </a>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    const {context} = this.props,
          {createShop, updateShop} = context,
          {shops} = context.props;

    return (
      <div ref={cw => this.cw = cw}>
        <h1>
          Заведения {' '}
          <Link to="/admin/new-shop" className="btn btn-primary">
            Добавить новое
          </Link> {' '}
        </h1>
        <div className="row">
          <input
            className="form-control"
            placeholder='поиск'
            value={context.state.searchWord}
            onChange={e => context.setState({searchWord: e.currentTarget.value})}
          />
        </div>
        <hr />
        <div className="row">
          <div className="form-inline">
            <div className="form-group">
              <input
                type="button"
                className="form-control"
                value="Пред."
                onClick={e => {
                  if(context.state.page === 1)
                    return ;
                  context.pageChangeHandler(context.state.page - 1);
                  context.setState({page: context.state.page - 1});
                }}
              />
            </div>{' '}
            <div className="form-group">
              <input
                className="form-control"
                value={context.state.page}
                onChange={e => {
                  const value = parseInt(e.currentTarget.value);

                  if(value > context.state.pagesCount ||
                      value < 1)
                    return ;

                  context.setState({page: value});
                  context.pageChangeHandler(value);
                }}
              />
            </div>{' '}
            <div className="form-group">
              <input
                type="button"
                className="form-control"
                value="След."
                onClick={e => {
                  if(parseInt(context.state.page) === parseInt(context.state.pagesCount))
                    return ;
                  context.pageChangeHandler(context.state.page + 1);
                  context.setState({page: context.state.page + 1});
                }}
              />
            </div>{' '}
            <div className="form-group">
              <input disabled className="form-control" value={`из ${context.state.pagesCount}`} />
            </div>
          </div>
        </div>
        <hr /><br />
        <div className="row">
          {this.renderShops()}
        </div>
      </div>
    );
  }
}
