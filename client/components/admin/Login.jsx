import React, {Component} from 'react';

export default class Login extends Component {
    render() {
        const {context} = this.props;

        return (
            <div className="container" style={{marginTop: '100px'}}>
                <div className="row">
                <div className="col-md-4 col-md-offset-4">
                    <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Авторизация</h3>
                    </div>
                    <div className="panel-body">
                        <form acceptCharset="UTF-8" role="form">
                        <fieldset>
                            <div className="form-group">
                                <input 
                                    className="form-control" 
                                    placeholder="Ваш email" 
                                    name="email" 
                                    type="text" 
                                    value={context.state.email}
                                    onChange={e => context.setState({email: e.target.value})}
                                />
                            </div>
                            <div className="form-group">
                                <input 
                                    className="form-control" 
                                    placeholder="Пароль" 
                                    name="password" 
                                    type="password" 
                                    value={context.state.password}
                                    onChange={e => context.setState({password: e.target.value})}
                                />
                            </div>
                            <input 
                                className="btn btn-lg btn-success btn-block" 
                                type="submit" 
                                defaultValue="Войти"
                                onClick={context.loginHandler.bind(context)} 
                            />
                        </fieldset>
                        </form>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}