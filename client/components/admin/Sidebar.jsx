import React, {Component} from 'react';
import {Link} from 'react-router';

import SidebarLink from './SidebarLink';

export default class Sidebar extends Component {
  componentWillMount() {
    Meteor.subscribe('cities');
  }

  renderMortalUsersLinks(pathname) {
    if(Roles.userIsInRole(Meteor.userId(), 'contentManager')) {
      return [
        <SidebarLink key='5' pathname={pathname} to="/admin/albums" title="Фотоальбомы"/>,
        <SidebarLink key='6' pathname={pathname} to="/admin/announcements" title="Анонсы"/>,
        <SidebarLink key='7' pathname={pathname} to="/admin/shops" title="Заведения"/>,
        <SidebarLink key='8' pathname={pathname} to="/admin/videos" title="Видео"/>,
        <SidebarLink key='9' pathname={pathname} to="/admin/slides" title="Слайдер"/>,
        <SidebarLink key='10' pathname={pathname} to="/admin/banners" title="Баннеры"/>,
      ];
    }
    else if(Roles.userIsInRole(Meteor.userId(), 'journalist')){
      return [
        <SidebarLink pathname={pathname} to="/admin/articles" title="Статьи"/>
      ];
    }
    else if(Roles.userIsInRole(Meteor.userId(), 'master')) {
      return [
        <SidebarLink key='5' pathname={pathname} to="/admin/albums" title="Фотоальбомы"/>,
        <SidebarLink key='6' pathname={pathname} to="/admin/announcements" title="Анонсы"/>,
        <SidebarLink key='7' pathname={pathname} to="/admin/shops" title="Заведения"/>,
        <SidebarLink key='8' pathname={pathname} to="/admin/articles" title="Статьи"/>,
        <SidebarLink key='9' pathname={pathname} to="/admin/videos" title="Видео"/>,
        <SidebarLink key='10' pathname={pathname} to="/admin/slides" title="Слайдер"/>,
        <SidebarLink key='11' pathname={pathname} to="/admin/banners" title="Баннеры"/>,
        <SidebarLink key='12' pathname={pathname} to="/admin/contacts" title="Контакты"/>,
      ];
    }
  }

  renderMasterLinks(pathname) {
    if(Roles.userIsInRole(Meteor.userId(), 'master')) {
      return [
        <SidebarLink key='0' pathname={pathname} to="/admin/tags" title="Тэги"/>,
        <SidebarLink key='1' pathname={pathname} to="/admin/cities" title="Города"/>,
        <SidebarLink key='2' pathname={pathname} to="/admin/artists" title="Фотографы"/>,
        <SidebarLink key='3' pathname={pathname} to="/admin/users" title="Пользователи"/>
      ];
    }
    else {
      return [
        <SidebarLink key='0' pathname={pathname} to="/admin/tags" title="Тэги"/>
      ];
    }
  }

  renderCurrentCity() {
    const {context} = this.props,
          currentCity = this.props.cities.filter(item => item._id === context.state.currentCity)[0];

      return  (
        <li>
          <div className="dropdown">
            <button
              className="dropdown-toggle col-sm-12 text-left"
              type="button"
              style={{
                backgroundColor: '#f98561',
                fontWeight: 'bold',
                color: 'white',
                border: 'none'
              }}
              id="dropdownMenu1"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="true"
            >
              <div>&nbsp;{currentCity ? currentCity.name : 'Город не выбран'}&nbsp;
              <span className="caret" /> </div>
            </button>
            <ul className="dropdown-menu col-sm-12" aria-labelledby="dropdownMenu1">
              {this.renderAllCities()}
            </ul>
          </div>
        </li>
      );
  }

  renderAllCities() {
    const {context} = this.props;

    if (this.props.cities.length > 0)
      return this.props.cities.map((city, index) => {
        return (
          <li key={index}>
              <a
                style={{height: '3em', padding: '1em'}}
                href="#"
                onClick={context.changeCity.bind(context,city._id)}
              >
                {city.name}
              </a>
          </li>
        )
      });
    else
      return ;
  }

  render() {
    const {pathname} = this.props;

    return (
        <div id="sidebar-wrapper">
            <ul className="sidebar-nav">
                <li className="sidebar-brand ">
                    <Link to="/">
                        <img style={{WebkitUserSelect: 'none', backgroundPosition: '0px 0px, 10px 10px', backgroundSize: '20px 20px', backgroundImage: 'linear-gradient(45deg, #eee 25%, transparent 25%, transparent 75%, #eee 75%, #eee 100%),linear-gradient(45deg, #eee 25%, white 25%, white 75%, #eee 75%, #eee 100%)'}} src="http://kipyat.com/img/logo.gif" />
                    </Link>
                </li>
                <br />
                <br />
                {this.renderCurrentCity()}
                <br /><br /><br />
                {this.renderMortalUsersLinks(pathname)}
                <br/>
                {this.renderMasterLinks(pathname)}

                <li>
                    <Link
                        to="#"
                        onClick={e=> {e.preventDefault(); Meteor.logout();}}
                        style={{color: 'coral'}}>
                            Выйти <i className="fa fa-sign-out"></i>
                    </Link>
                </li>
            </ul>
        </div>
    );
  }
}
