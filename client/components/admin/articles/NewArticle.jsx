import React, {Component} from 'react';
import {Link} from 'react-router';

import Select from '../../stateless/Select';
import Select2 from '../../stateless/Select2';
import FormGroup from '../../stateless/FormGroup';
import TextArea from '../../stateless/TextArea';
import DateTimePicker from '../../stateless/DateTimePicker';
import Editor from '../../stateless/Editor';

import moment from 'moment';

import CropperModal from '../CropperModal';
import {idToName} from '../../../lib/collectionsRelated';

export default class NewArticle extends Component {
  renderPhotos() {
    const {context} = this.props,
          {state} = context,
          {sliderPhotos} = state,
          sliderPhotosPerRow = 4;
    let row = [],
        rows = [];

    sliderPhotos.forEach((item, index) => {
      if(row.length < sliderPhotosPerRow) {
        row.push(
          <div
            key={index}
            onClick={context.removePhoto.bind(context, item)}
            title="Нажмите на фото, чтобы удалить"
            className={`gallery_product deletable col-lg-${12 / sliderPhotosPerRow} col-md-${12 / sliderPhotosPerRow} col-sm-6 col-xs-12 filter hdpe`}
          >
            <img
              style={{width: '100%'}}
              src={item}
              className="img-responsive"
            />
          </div>
        );
      }

      if(row.length === sliderPhotosPerRow){
        rows.push(
          <div
            key={rows.length}
            className="row"
          >
            {row}
          </div>
        );
        row = [];
      }
    });

    if(row.length) {
      rows.push(
        <div
          key={rows.length}
          className="row"
        >
          {row}
        </div>
      );
    }

    return rows;
  }

  renderAlbumsList() {
    const {context} = this.props,
          {albums} = context.props;

    return albums.map((album, index) => {
      return (
        <a
          key={index}
          href="#"
          onClick={e => {
            e.preventDefault();
            e.currentTarget.classList.toggle('active');
            context.toggleAlbum.call(context, album._id);
          }}
          className="list-group-item"
        >
          <h4 className="list-group-item-heading">
            {album.title} <small>{moment(album.date).calendar()}</small>
          </h4>
          <p className="list-group-item-text">
            {album.subtitle}
          </p>
        </a>
      );
    });
  }

  renderAlbums() {
    const {context} = this.props,
          {albums, articles, shops} = context.props,
          {albumIds} = context.state;

    return albums.filter(album => albumIds.includes(album._id))
            .map((album, index) => {
              return (
                <div
                  className="col-xs-18 col-sm-6 col-md-3"
                  key={index}
                >
                  <div className="thumbnail" style={{height: '550px', maxHeight: '550px'}}>
                    <img
                      src={album.cover}
                      style={{
                        width: '70%',
                        margin: 'auto'
                      }}
                    />
                    <div className="caption">
                      <h4>{album.title}</h4><hr />
                      <p>
                        <b>Подзаголовок:</b> {`${album.subtitle}`}<br /><br />
                        <b>Заведение:</b> {idToName(album.shopId, shops)}
                      </p>
                      <a
                        href="#"
                        className="btn btn-danger btn-xs"
                        role="button"
                        onClick={e => {e.preventDefault(); context.toggleAlbum.call(context, album._id)}}
                      >
                        Удалить
                      </a>
                    </div>
                  </div>
                </div>
              );
            });
  }

  render() {
    const {context} = this.props,
          {cover, mainPosition, topPosition} = context.state,
          {albums, articles, tags} = context.props,
          journalists = context.props.journalists.map(author => {
            return {
              _id: author._id,
              name: `${author.profile.first_name} ${author.profile.last_name}`
            }
          }),
          mainArticles = articles.filter(article => article.mainPosition === mainPosition),
          topArticles = articles.filter(article => article.topPosition === topPosition),
          filteredTags = tags.filter(t => t.category === 3),
          albumsForSelect2 = albums.map(album => {
            const theDate = moment.unix(album.date).format('ll');

            return {
              _id: album._id,
              name: `${album.title} - ${album.subtitle} - ${theDate}`
            }
          });

    return (
      <div>
        <Link
          to='/admin/articles'
          className='btn btn-warning'
        >
          Вернуться к списку статей
        </Link>
        <br /><hr /><br />
        <div className="row">
          <div className="col-sm-4 text-center">
            Выберите обложку<br/><br/>
            <a href="#" className="thumbnail">
              <img
                width={150}
                src={cover}
                alt="..." />
            </a>
            <a
              href="#"
              className="btn btn-primary"
              onClick={(e) => {context.logoInput.click()}}
            >
              <i className="fa fa-plus"/>{' '}Изменить
            </a>
            <input
              style={{display: 'none'}}
              type='file'
              onChange={(e) => {
                context.changeCover(e.currentTarget, 'cover', 'logo-cropper')
              }}
              ref={cli => context.logoInput = cli}
            />
          </div>
          <div className="col col-sm-8">
            <div className="form-group">
              <label>Тэги:</label>
              <Select2
                placeholder='Выберите тэг(и)'
                context={context}
                options={filteredTags}
                stateName='tags'
              />
            </div>
            <FormGroup
              context={context}
              stateName='title'
              label='Заголовок:'
            />
            <FormGroup
              context={context}
              stateName='subtitle'
              label='Подзаголовок:'
            />
            <DateTimePicker
              label='Выберите дату и время:'
              context={context}
              stateName='date'
            />
            <div className="row">
              <div className="col-sm-4">
                <div className="form-group">
                  <label>Выберите автора:</label>
                  <Select
                    context={context}
                    options={journalists}
                    stateName='authorId'
                    placeholder='выберите автора'
                  />
                </div>
              </div>
              <div className="col-sm-4">
                <FormGroup
                  context={context}
                  stateName='customAuthor'
                  label='Разовый автор:'
                />
              </div>
              <div className="col-sm-4">
                <div className="checkbox">
                  <label>
                    <br/>
                    <input
                      type="checkbox"
                      checked={context.state.noAuthor}
                      onChange={e => context.setState({noAuthor: e.currentTarget.checked})}
                    /> Без автора
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="row"><label>Фото для слайдера:</label></div>
          <div className="col-sm-offset-1 col-sm-2 text-center">
            Фото-1<br/><br/>
            <a href="#" className="thumbnail">
              <img
                width={150}
                src={context.state.photo1}
                alt="..." />
            </a>
            <a
              href="#"
              className="btn btn-primary"
              onClick={(e) => {e.preventDefault(); context.photo1Input.click();}}
            >
              <i className="fa fa-plus"/>{' '}Изменить
            </a>
            <input
              style={{display: 'none'}}
              type='file'
              onChange={(e) => {
                context.changeCover(e.currentTarget, 'photo1', 'photo1-cropper')
              }}
              ref={cli => context.photo1Input = cli}
            />
          </div>
          <div className="col-sm-2 text-center">
            Фото-2<br/><br/>
            <a href="#" className="thumbnail">
              <img
                width={150}
                src={context.state.photo2}
                alt="..." />
            </a>
            <a
              href="#"
              className="btn btn-primary"
              onClick={(e) => {e.preventDefault(); context.photo2Input.click();}}
            >
              <i className="fa fa-plus"/>{' '}Изменить
            </a>
            <input
              style={{display: 'none'}}
              type='file'
              onChange={(e) => {
                context.changeCover(e.currentTarget, 'photo2', 'photo2-cropper')
              }}
              ref={cli => context.photo2Input = cli}
            />
          </div>
          <div className="col-sm-2 text-center">
            Фото-3<br/><br/>
            <a href="#" className="thumbnail">
              <img
                width={150}
                src={context.state.photo3}
                alt="..." />
            </a>
            <a
              href="#"
              className="btn btn-primary"
              onClick={(e) => {e.preventDefault(); context.photo3Input.click();}}
            >
              <i className="fa fa-plus"/>{' '}Изменить
            </a>
            <input
              style={{display: 'none'}}
              type='file'
              onChange={(e) => {
                context.changeCover(e.currentTarget, 'photo3', 'photo3-cropper')
              }}
              ref={cli => context.photo3Input = cli}
            />
          </div>
          <div className="col-sm-2 text-center">
            Фото-4<br/><br/>
            <a href="#" className="thumbnail">
              <img
                width={150}
                src={context.state.photo4}
                alt="..." />
            </a>
            <a
              href="#"
              className="btn btn-primary"
              onClick={(e) => {e.preventDefault(); context.photo4Input.click();}}
            >
              <i className="fa fa-plus"/>{' '}Изменить
            </a>
            <input
              style={{display: 'none'}}
              type='file'
              onChange={(e) => {
                context.changeCover(e.currentTarget, 'photo4', 'photo4-cropper')
              }}
              ref={cli => context.photo4Input = cli}
            />
          </div>
          <div className="col-sm-2 text-center">
            Фото-5<br/><br/>
            <a href="#" className="thumbnail">
              <img
                width={150}
                src={context.state.photo5}
                alt="..." />
            </a>
            <a
              href="#"
              className="btn btn-primary"
              onClick={(e) => {e.preventDefault(); context.photo5Input.click();}}
            >
              <i className="fa fa-plus"/>{' '}Изменить
            </a>
            <input
              style={{display: 'none'}}
              type='file'
              onChange={(e) => {
                context.changeCover(e.currentTarget, 'photo5', 'photo5-cropper')
              }}
              ref={cli => context.photo5Input = cli}
            />
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="form-group">
            <label>Начало контента:</label>
            <Editor
              id="content-start"
              defaultValue=""
              height={300}
            />
          </div>
          <div className="row">
            <div className={`col col-sm-${context.state.videoCode ? 5 : 12}`}>
              <div className="form-group">
                <label htmlFor="shopNameModal">Код видео:</label>
                <input
                  type="text"
                  className="form-control"
                  name="shopNameModal"
                  placeholder="Введите код"
                  value={context.state.videoCode}
                  onChange={e => context.setState({videoCode: e.currentTarget.value})}
                />
              </div>
            </div>
            <div
              className="col col-sm-7 video-wrapper"
              dangerouslySetInnerHTML={{ __html: context.state.videoCode }}
              style={{height: context.state.videoCode ? 400 : 'auto'}}
            >
            </div>
          </div>
          <div className="form-group">
            <label>Конец контента:</label>
            <Editor
              id="content-end"
              defaultValue=""
              height={300}
            />
          </div>
        </div>
        <hr/>
        <div className="row">
          <div className="form-group">
            <label>Альбомы:</label>
            <Select2
              placeholder='Выберите альбом(ы)'
              context={context}
              options={albumsForSelect2}
              stateName='albumIds'
            />
          </div>
          <div className="row">
            {this.renderAlbums()}
          </div>
        </div>
        <hr/>
        <div className="row">
          <div className="form-group">
            <div className="checkbox">
              <label>
                <br/>
                <input
                  type="checkbox"
                  checked={context.state.isMain}
                  onChange={e => context.setState({isMain: e.currentTarget.checked})}
                /> На главную
              </label>
            </div>
          </div>
          <div style={{display: context.state.isMain ? '' : 'none'}}>
            <FormGroup
              context={context}
              stateName='mainPosition'
              label='Позиция на главной'
            />
            <div
              style={{display: mainArticles.length ? '':'none'}}
              className="alert alert-danger"
              role="alert"
            >
              <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true" />
              <span className="sr-only">Error:</span>
              Позиция совпадает со статьей: {mainArticles[0] ? mainArticles[0].title : ''}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="form-group">
            <div className="checkbox">
              <label>
                <br/>
                <input
                  type="checkbox"
                  checked={context.state.isTop}
                  onChange={e => context.setState({isTop: e.currentTarget.checked})}
                /> В топ
              </label>
            </div>
          </div>
          <div style={{display: context.state.isTop ? '' : 'none'}}>
            <FormGroup
              context={context}
              stateName='topPosition'
              label='Позиция в топе'
            />
            <div
              style={{display: topArticles.length ? '':'none'}}
              className="alert alert-danger"
              role="alert"
            >
              <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true" />
              <span className="sr-only">Error:</span>
              Позиция совпадает со статьей: {topArticles[0] ? topArticles[0].title : ''}
            </div>
          </div>
        </div>
        <hr />
        <a
          href='#'
          onClick={context.createArticleHandler.bind(context)}
          className='btn btn-primary'
        >
          Добавить статью
        </a>

        <CropperModal
          width={900}
          height={465}
          id='logo-cropper'
          context={context}
          stateName='cover'
        />
        <CropperModal
          width={900}
          height={465}
          id='photo1-cropper'
          context={context}
          stateName='photo1'
        />
        <CropperModal
          width={900}
          height={465}
          id='photo2-cropper'
          context={context}
          stateName='photo2'
        />
        <CropperModal
          width={900}
          height={465}
          id='photo3-cropper'
          context={context}
          stateName='photo3'
        />
        <CropperModal
          width={900}
          height={465}
          id='photo4-cropper'
          context={context}
          stateName='photo4'
        />
        <CropperModal
          width={900}
          height={465}
          id='photo5-cropper'
          context={context}
          stateName='photo5'
        />
      </div>
    );
  }
}
