import React, {Component} from 'react';
import {Link} from 'react-router';
import {idToName} from '../../../lib/collectionsRelated';
import ConfirmButton from '../../stateless/ConfirmButton';

export default class Articles extends Component {

  componentDidUpdate() {
    const confirmButtons = $('.confirm-button');

    if(!confirmButtons.length) return ;

    $(confirmButtons).confirmation({
      title: 'Вы уверены?',
      btnOkLabel: 'Да.',
      btnCancelLabel: 'Нет.',
      placement: this.props.placement || 'top',
      onConfirm: () => {

      }
    });
  }

  renderArticles(){
    const {context} = this.props,
          {deleteArticle} = context,
          {articles, shops} = context.props;

    return articles.map((article, index) => {
      return (
        <div
          className="col-xs-18 col-sm-6 col-md-3"
          key={index}
        >
          <div className="thumbnail" style={{height: '550px', maxHeight: '550px'}}>
            <img
              src={article.cover}
              style={{width: '100%'}}
            />
            <div className="caption">
              <h4>{article.title}</h4><hr />
              <p>
                <b>Подзаголовок:</b> {`${article.subtitle}`}<br /><br />
              </p>
              <Link
                to={`/admin/edit-article/${article._id}`}
                className="btn btn-info"
                role="button"
              >
                Редактировать
              </Link>{' '}
              <ConfirmButton
                context={context}
                itemId={article._id}
                className='btn-sm confirm-button'
              >
                Удалить
              </ConfirmButton>{' '}
              <a
                href='#'
                className="btn btn-info btn-sm"
                onClick={e => {
                  e.preventDefault();
                  context.toggleVisibility(article._id);
                }}
              >
                {article.hidden ?  'Показать' : 'Скрыть'}
              </a>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    const {context} = this.props,
          {createArticle, updateArticle} = context;
    return (
      <div>
        <h1>Статьи <Link to="/admin/new-article" className="btn btn-primary">Добавить новую</Link></h1>
        <hr /><br />
        <div className="row">
          {this.renderArticles()}
        </div>
      </div>
    );
  }
}
