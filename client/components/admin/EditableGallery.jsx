import React, {Component} from 'react';

import ConfirmButton from '../stateless/ConfirmButton';

import {mfLinkToId} from '/client/lib/general';

export default class EditableGallery extends Component {
  renderPhotos() {
    const {context} = this.props,
          {state} = context,
          {photos, photosToDelete, thumbnails, previews} = state,
          photosPerRow = 12;
    let row = [],
        rows = [];

    photos.forEach((item, index) => {
      let photoId;

      if(!state.version) {
        photoId = mfLinkToId(item);
      }

      if(row.length < photosPerRow) {
        const checked = photosToDelete.includes(item),
              faClass = `fa fa-3x fa-${checked ? 'check-square' : 'square-o'} ${checked ? '' : 'text-primary'}`;

        let imgSrc;

        if(previews && previews[photoId])
          imgSrc = previews[photoId];
        else if(thumbnails && thumbnails[photoId])
          imgSrc = thumbnails[photoId];
        else if(!state.version)
          imgSrc = item;
        else if(state.version === 1)
          imgSrc = item.thumbnail || item.photo;

        row.push(
          <div
            key={index}
            className={`gallery_product deletable col-lg-${12 / photosPerRow} col-md-${12 / photosPerRow} col-sm-6 col-xs-12 filter hdpe`}
          >
            <img
              style={{width: '100%'}}
              src={imgSrc}
              className="img-responsive"
            />
            <button
              className="btn btn-success pic-number"
              type="button">
              <b>{index + 1} / {photos.length}</b>
            </button>
            <div
              className="pic-checkbox"
              onClick={context.togglePhoto.bind(context, item)}
            >
              <i
                style={{color: checked ? 'red' : ''}}
                className={faClass}
              />
            </div>
          </div>
        );
      }

      if(row.length === photosPerRow){
        rows.push(
          <div
            key={rows.length}
            className="row"
          >
            {row}
          </div>
        );
        row = [];
      }
    });

    if(row.length) {
      rows.push(
        <div
          key={rows.length}
          className="row"
        >
          {row}
        </div>
      );
    }

    return rows;
  }

  render() {
    const {context} = this.props,
          {photosToDelete, photos} = context.state,
          removeButtonClass = photosToDelete.length ? '' : 'hidden',
          selectAllButtonClass = photos.length ? '' : 'hidden';

    return (
      <div>
        <div className="row">
          <button
            type="button"
            className="btn btn-success"
            onClick={e => {
              e.preventDefault();
              context.newPhotoInput.click();
            }}
          >
            {context.state.buttonLabel || 'Добавить фото'}
          </button>
          <input
            style={{display: 'none'}}
            type='file'
            ref={np => context.newPhotoInput = np}
            onChange={context.addPhoto.bind(context)}
            multiple
          />{' '}
          <button
            type="button"
            className={`btn btn-success ${selectAllButtonClass}`}
            onClick={context.toggleAllPhotos.bind(context)}
          >
            Выбрать все
          </button>
          {' '}
          <ConfirmButton
            functionName='removePhotos'
            context={context}
            className={removeButtonClass}
          >
            Удалить
          </ConfirmButton>
        </div>
        {this.renderPhotos()}
      </div>
    );
  }
}
