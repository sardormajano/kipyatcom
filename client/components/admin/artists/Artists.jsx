import React, {Component} from 'react';

import {Link} from 'react-router';

export default class Artists extends Component {

  renderArtists(){
    const {context } = this.props;
    return context.props.artists.map((artist, index)=> {
      return (
        <tr key={index}>
          <td>{index + 1}</td>
          <td>{artist.name}</td>
          <td>{artist.surname}</td>
          <td>
            <div className="btn-group" role="group">
              <Link
                type="button"
                className="btn btn-warning"
                to={`/admin/edit-artist/${artist._id}`}
              >
                <i className="fa fa-pencil" />
              </Link>
              <button
                type="button"
                className="btn btn-danger"
                data-id={artist._id}
                onClick={context.deleteArtist.bind(context)}
              >
                <i className="fa fa-trash" />
              </button>
            </div>
          </td>
        </tr>
      )
    });
  }

  render() {
    const {context} = this.props,
          {createArtist, updateArtist} = context;
    return (
      <div>
        <h1>Фотографы</h1>
        <div>
          <div className="panel panel-default">
            <div className="panel-heading">
              <Link
                className="btn btn-success"
                to="/admin/new-artist"
              >
                Добавить&nbsp;<i className="fa fa-plus"></i>
              </Link>
            </div>
            <div className="panel-body">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Действия</th>
                  </tr>
                </thead>
                <tbody>
                  {this.renderArtists()}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div className="modal fade" role="dialog" id="artistModal">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title">Добавление фотографа</h4>
              </div>
              <div className="modal-body">
                <form id="create-artist-form">
                  <div className="form-group">
                    <label htmlFor="artistNameModal">Имя фотографа</label>
                    <input type="text" className="form-control" name="artistNameModal" placeholder="Имя" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="artistSurnameModal">Фамилия</label>
                    <input type="text" className="form-control" name="artistSurnameModal" placeholder="Имя" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" className="btn btn-primary" onClick={createArtist.bind(context)} data-dismiss="modal">Сохранить</button>
              </div>
            </div>
          </div>
        </div>

        <div className="modal fade" role="dialog" id="artistEditModal">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title">Редактирование фотографа</h4>
              </div>
              <div className="modal-body">
                <form id="edit-artist-form">
                  <div className="form-group">
                    <label htmlFor="artistNameModal">Имя фотографа</label>
                    <input type="text" className="form-control" name="name" placeholder="Имя" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="artistSurnameModal">Фамилия</label>
                    <input type="text" className="form-control" name="surname" placeholder="Имя" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" className="btn btn-primary" onClick={updateArtist.bind(context)} data-dismiss="modal">Сохранить</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
