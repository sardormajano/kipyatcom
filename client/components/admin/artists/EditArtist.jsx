import React, {Component} from 'react';
import {Link} from 'react-router';

import Select from '../../stateless/Select';
import Select2 from '../../stateless/Select2';
import FormGroup from '../../stateless/FormGroup';
import TextArea from '../../stateless/TextArea';

import CropperModal from '../CropperModal';
import EditableGallery from '../EditableGallery';

export default class EditArtist extends Component {

  render() {
    const {context} = this.props,
          {artists} = context.props,
          {cover, cover2} = context.state;

    return (
      <div>
        <Link
          to='/admin/artists'
          className='btn btn-warning'
        >
          Вернуться к списку фотографов
        </Link>
        <br /><hr /><br />
        <div className="row">
          <div
            className="col-sm-4 text-center"
            style={{borderRight: '1px solid lightgrey', padding: '2em'}}
          >
            <div
              className="row"
            >
              Аватар<br/><br/>
              <a href="#" className="thumbnail">
                <img
                  width={150}
                  src={cover}
                  alt="..." />
              </a>
              <a
                href="#"
                className="btn btn-primary"
                onClick={(e) => {e.preventDefault();context.changeLogoInput.click()}}
              >
                <i className="fa fa-plus"/>{' '}Изменить
              </a>
              <input
                style={{display: 'none'}}
                type='file'
                onChange={context.changeCoverHandler.bind(context)}
                ref={cli => context.changeLogoInput = cli}
                multiple
              />
            </div>
            <hr />
            <div className="row">
              Фотография<br/><br/>
              <a href="#" className="thumbnail">
                <img
                  width={150}
                  src={cover2}
                  alt="..." />
              </a>
              <a
                href="#"
                className="btn btn-primary"
                onClick={(e) => {e.preventDefault();context.changeLogo2Input.click()}}
              >
                <i className="fa fa-plus"/>{' '}Изменить
              </a>
              <input
                style={{display: 'none'}}
                type='file'
                onChange={context.changeCover2Handler.bind(context)}
                ref={cl2i => context.changeLogo2Input = cl2i}
                multiple
              />
            </div>
          </div>
          <div className="col col-sm-8">
            <FormGroup
              context={context}
              stateName='name'
              label='Имя'
            />
            <FormGroup
              context={context}
              stateName='surname'
              label='Фамилия'
            />
            <hr />
          </div>
        </div>
        <hr />
        <a
          href='#'
          onClick={context.editArtistHandler.bind(context)}
          className='btn btn-primary'
        >
          Сохранить
        </a>
        <CropperModal
          width={150}
          height={150}
          id='logo-cropper'
          context={context}
          stateName='cover'
        />
      </div>
    );
  }
}
