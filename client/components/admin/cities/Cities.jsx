import React, {Component} from 'react';

export default class Cities extends Component {

  renderCities(){
    const {context } = this.props;
    return context.props.cities.map((city, index)=> {
      return (
        <tr key={index}>
          <td>{index + 1}</td>
          <td>{city.name}</td>
          <td>{city.domainPrefix}</td>
          <td>
            <div className="btn-group" role="group">
              <button
                type="button"
                className="btn btn-warning"
                data-id={city._id}
                data-toggle="modal"
                data-target="#cityEditModal"
                onClick={context.preUpdateCity.bind(context)}
              >
                <i className="fa fa-pencil" />
              </button>
              <button
                type="button"
                className="btn btn-danger"
                data-id={city._id}
                onClick={context.deleteCity.bind(context)}
              >
                <i className="fa fa-trash" />
              </button>
            </div>
          </td>
        </tr>
      )
    });
  }

  render() {
    const {context} = this.props,
          {createCity, updateCity} = context;
    return (
      <div>
        <h1>Города</h1>
        <div>
          <div className="panel panel-default">
            <div className="panel-heading">
              <a className="btn btn-success" data-toggle="modal" data-target="#cityModal">Добавить&nbsp;<i className="fa fa-plus"></i></a>
            </div>
            <div className="panel-body">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Префикс домена</th>
                    <th>Действия</th>
                  </tr>
                </thead>
                <tbody>
                  {this.renderCities()}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div className="modal fade" role="dialog" id="cityModal">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title">Добавление города</h4>
              </div>
              <div className="modal-body">
                <form id="create-city-form">
                  <div className="form-group">
                    <label htmlFor="cityNameModal">Название города</label>
                    <input type="text" className="form-control" name="cityNameModal" placeholder="Название" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="cityNameModal">Префикс домена для города</label>
                    <input type="text" className="form-control" name="domainPrefixModal" placeholder="Префиск домена" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" className="btn btn-primary" onClick={createCity.bind(context)} data-dismiss="modal">Сохранить</button>
              </div>
            </div>
          </div>
        </div>

        <div className="modal fade" role="dialog" id="cityEditModal">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title">Редактирование фотографа</h4>
              </div>
              <div className="modal-body">
                <form id="edit-city-form">
                  <div className="form-group">
                    <label htmlFor="cityNameModal">Название города</label>
                    <input type="text" className="form-control" name="name" placeholder="Название" />
                  </div>
                  <div className="form-group">
                    <label htmlFor="cityNameModal">Префикс домена для города</label>
                    <input type="text" className="form-control" name="domainPrefix" placeholder="Префикс домена" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" className="btn btn-primary" onClick={updateCity.bind(context)} data-dismiss="modal">Сохранить</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
