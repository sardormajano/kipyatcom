import React, {Component} from 'react';
import {Link} from 'react-router';
import FormGroup from '../../stateless/FormGroup';
import EditableGallery from '../EditableGallery';

export default class EditModalBanner extends Component {
  render() {
    const {context} = this.props,
          {off, name} = context.state;

    let turnButton;

    if(off) {
      turnButton = (
        <a
          href='#'
          onClick={() => context.setState({off: false})}
          className='btn btn-primary'
        >
          Включить Баннер
        </a>
      );
    } else {
      turnButton = (
        <a
          href='#'
          onClick={() => context.setState({off: true})}
          className='btn btn-danger'
        >
          Отключить Баннер
        </a>
      );
    }

    return (
      <div>
        <Link
          to='/admin/banners'
          className='btn btn-warning'
        >
          Вернуться к списку баннеров
        </Link>
        <br /><hr /><br />
        Название: <strong>{name}</strong><br /><br />
        <div className="row">
          {turnButton}<br /><br />
          <br/>
          <div className={`col col-sm-${context.state.content ? 5 : 12}`}>
            <FormGroup
              context={context}
              stateName='delayTime'
              label='Время задержки (в милисекундах):'
            />
            <hr />
            <EditableGallery context={context}/>
            <br/><br/><br/><br/>
            <hr />
            <div className="form-group">
              <label htmlFor="shopNameModal">Код:</label>
              <input
                type="text"
                className="form-control"
                placeholder="Введите код"
                value={context.state.content}
                onChange={e => context.setState({content: e.currentTarget.value})}
              />
            </div>
          </div>
          <div
            className="col col-sm-7 video-wrapper"
            dangerouslySetInnerHTML={{ __html: context.state.content }}
            style={{height: context.state.content ? 400 : 'auto'}}
          >
          </div>
          <hr />
          <a
            href='#'
            onClick={context.editBannerHandler.bind(context)}
            className='btn btn-primary'
          >
            Сохранить
          </a>
        </div>
      </div>
    );
  }
}
