import React, {Component} from 'react';
import {Link} from 'react-router';
import {idToName} from '../../../lib/collectionsRelated';
import ConfirmButton from '../../stateless/ConfirmButton';

const categoryNames = [
  "Главная",
  "Фото",
  "Анонсы",
  "Заведения",
  "Статьи",
  "Видео",
];

export default class Banners extends Component {
  renderTabLinks() {
    const {context} = this.props,
          {currentCategory} = context.state;

    return categoryNames.map((name, index) => {
      return (
        <li
          key={index}
          role="presentation"
          className={`${currentCategory === index ? 'active' : ''}`}
        >
          <a
            href="#"
            onClick={(e) => {e.preventDefault(); context.setState({currentCategory: index})}}
          >
            {name}
          </a>
        </li>
      );
    });
  }

  renderTab(category) {
    const optionalPart = (
      <div>
        <div className="row">
          <Link className="link-to-banner" to={`/admin/edit-banner/${category}-a`}><img src="/img/banner-position/b-a.jpg" alt="" className="col-sm-12"/></Link>
        </div><br/>
        <div className="row">
          <Link className="link-to-banner" to={`/admin/edit-banner/${category}-3-1`}><img src="/img/banner-position/b-3-1.jpg" alt="" className="col-sm-6"/></Link>
          <Link className="link-to-banner" to={`/admin/edit-banner/${category}-3-2`}><img src="/img/banner-position/b-3-2.jpg" alt="" className="col-sm-6"/></Link>
        </div><br/>
        <div className="row">
          <Link className="link-to-banner" to={`/admin/edit-banner/${category}-b`}><img src="/img/banner-position/b-b.jpg" alt="" className="col-sm-12"/></Link>
        </div><br/>
        <div className="row">
          <Link className="link-to-banner" to={`/admin/edit-banner/${category}-4-1`}><img src="/img/banner-position/b-4-1.jpg" alt="" className="col-sm-6"/></Link>
          <Link className="link-to-banner" to={`/admin/edit-banner/${category}-4-2`}><img src="/img/banner-position/b-4-2.jpg" alt="" className="col-sm-6"/></Link>
        </div><br/>
      </div>
    );

    return (
      <div>
        <label>Баннеры на странице "{categoryNames[category]}"</label>
        <hr /><br />
        <div className="row" style={{margin: '2% 10%'}}>
          <div className="row">
            <Link className="link-to-banner" to={`/admin/edit-banner/${category}-1-1`}><img src="/img/banner-position/b-1-1.jpg" alt="" className="col-sm-6"/></Link>
            <Link className="link-to-banner" to={`/admin/edit-banner/${category}-1-2`}><img src="/img/banner-position/b-1-2.jpg" alt="" className="col-sm-6"/></Link>
          </div><br/>
          <div className="row">
            <Link><img src="/img/banner-position/top-menu.jpg" alt="" className="col-sm-12"/></Link>
          </div><br/>
          <div className="row">
            <Link className="link-to-banner" to={`/admin/edit-banner/${category}-2-1`}><img src="/img/banner-position/b-2-1.jpg" alt="" className="col-sm-6"/></Link>
            <Link className="link-to-banner" to={`/admin/edit-banner/${category}-2-2`}><img src="/img/banner-position/b-2-2.jpg" alt="" className="col-sm-6"/></Link>
          </div><br/>
          {category === 0 ? optionalPart : ''}
          <div className="row">
            <Link><img src="/img/banner-position/articles.jpg" alt="" className="col-sm-12"/></Link>
          </div><br/>
          <div className="row">
            <Link className="link-to-banner" to={`/admin/edit-banner/${category}-5-1`}><img src="/img/banner-position/b-5-1.jpg" alt="" className="col-sm-6"/></Link>
            <Link className="link-to-banner" to={`/admin/edit-banner/${category}-5-2`}><img src="/img/banner-position/b-5-2.jpg" alt="" className="col-sm-6"/></Link>
          </div><br/>
          <div className="row">
            <Link className="link-to-banner" to={`/admin/edit-modal-banner/${category}-6`}><img src="/img/banner-position/b-6.jpg" alt="" className="col-sm-12"/></Link>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const {context} = this.props,
          {createBanner, updateBanner} = context,
          {currentCategory} = context.state;

    return (
      <div>
        <div className="panel-body">
          <ul className="nav nav-tabs">
            {this.renderTabLinks()}
          </ul>
          <br />
          <br />
          {this.renderTab(currentCategory)}
        </div>
      </div>
    );
  }
}
