import React, {Component} from 'react';
import {Link} from 'react-router';
import FormGroup from '../../stateless/FormGroup';
import EditableGallery from '../EditableGallery';

export default class EditBanner extends Component {
  renderLinks() {
    const {context} = this.props,
          {photos} = context.state,
          length = photos.length,
          links = [];

    for(let i = 0; i < length; i++) {
      links.push(
        <div
          className={`form-group`}
          key={i}
        >
          <input
            name={`link-${i}`}
            onChange={e => {
              const tempLinks = context.state.links;
              tempLinks[i] = e.currentTarget.value;
              context.setState({links: tempLinks})
            }}
            type='text'
            className="form-control"
            placeholder={`ссылка-${i+1}`}
            value={context.state.links[i]}
          />
        </div>
      );
    }

    return links;
  }

  render() {
    const {context} = this.props,
          {off, name} = context.state;

    let turnButton;

    if(off) {
      turnButton = (
        <a
          href='#'
          onClick={() => context.setState({off: false})}
          className='btn btn-primary'
        >
          Включить Баннер
        </a>
      );
    } else {
      turnButton = (
        <a
          href='#'
          onClick={() => context.setState({off: true})}
          className='btn btn-danger'
        >
          Отключить Баннер
        </a>
      );
    }

    return (
      <div>
        <Link
          to='/admin/banners'
          className='btn btn-warning'
        >
          Вернуться к списку баннеров
        </Link>
        <br /><hr /><br />
        Название: <strong>{name}</strong><br /><br />
        <div className="row">
          {turnButton}<br /><br />
          {this.renderLinks()}
          <label htmlFor="bannerNameModal">Фото:</label>
          <hr />
          <EditableGallery context={context}/>
          <br/><br/><br/><br/>
          <div className="form-group">
            <div className="checkbox">
              <label>
                <br/>
                <input
                  type="checkbox"
                  checked={context.state.isTop}
                  onChange={e => context.setState({isTop: e.currentTarget.checked})}
                /> Топ
              </label>
            </div>
          </div>
          <hr />
          <a
            href='#'
            onClick={context.editBannerHandler.bind(context)}
            className='btn btn-primary'
          >
            Сохранить
          </a>
        </div>
      </div>
    );
  }
}
