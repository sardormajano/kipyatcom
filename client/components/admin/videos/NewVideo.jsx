import React, {Component} from 'react';
import {Link} from 'react-router';

import Select from '../../stateless/Select';
import Select2 from '../../stateless/Select2';
import FormGroup from '../../stateless/FormGroup';
import TextArea from '../../stateless/TextArea';
import DateTimePicker from '../../stateless/DateTimePicker';
import moment from 'moment';

import CropperModal from '../CropperModal';

import {idToName} from '../../../lib/collectionsRelated';

export default class NewVideo extends Component {

  render() {
    const {context} = this.props,
          {cover, mainPosition, topPosition} = context.state,
          {tags, videos} = context.props,
          authors = context.props.authors.map(author => {
            return {
              _id: author._id,
              name: `${author.profile.first_name} ${author.profile.last_name}`
            }
          }),
          filteredTags = tags.filter(t => t.category === 4),
          mainVideos = videos.filter(video => video.mainPosition === mainPosition),
          topVideos = videos.filter(video => video.topPosition === topPosition);

    return (
      <div>
        <Link
          to='/admin/videos'
          className='btn btn-warning'
        >
          Вернуться к списку видео
        </Link>
        <br /><hr /><br />
        <div className="row">
          <div className="col-sm-4 text-center">
            Выберите обложку<br/><br/>
            <a href="#" className="thumbnail">
              <img
                width={150}
                src={cover}
                alt="..." />
            </a>
            <a
              href="#"
              className="btn btn-primary"
              onClick={(e) => {e.preventDefault(); context.changeLogoInput.click();}}
            >
              <i className="fa fa-plus"/>{' '}Изменить
            </a>
            <input
              style={{display: 'none'}}
              type='file'
              onChange={context.changeCoverHandler.bind(context)}
              ref={cli => context.changeLogoInput = cli}
              multiple
            />
          </div>
          <div className="col col-sm-8">
            <FormGroup
              context={context}
              stateName='title'
              label='Заголовок:'
            />
            <FormGroup
              context={context}
              stateName='subtitle'
              label='Подзаголовок:'
            />
            <DateTimePicker
              label='Выберите дату и время:'
              context={context}
              stateName='date'
            />
            <div className="row">
              <div className="col-sm-4">
                <div className="form-group">
                  <label>Выберите автора:</label>
                  <Select
                    context={context}
                    options={authors}
                    stateName='authorId'
                    placeholder='выберите автора'
                  />
                </div>
              </div>
              <div className="col-sm-4">
                <FormGroup
                  context={context}
                  stateName='customAuthor'
                  label='Разовый автор:'
                />
              </div>
              <div className="col-sm-4">
                <div className="checkbox">
                  <label>
                    <br/>
                    <input
                      type="checkbox"
                      checked={context.state.noAuthor}
                      onChange={e => context.setState({noAuthor: e.currentTarget.checked})}
                    /> Без автора
                  </label>
                </div>
              </div>
            </div>
            <div className="form-group">
              <label>Тэги:</label>
              <Select2
                placeholder='Выберите тэг(и)'
                context={context}
                options={filteredTags}
                subscription='tags'
                subscriptionArgument={4}
                stateName='tags'
              />
            </div>
          </div>
          <div className="row">
            <div className="form-group">
              <div className="checkbox">
                <label>
                  <br/>
                  <input
                    type="checkbox"
                    checked={context.state.isMain}
                    onChange={e => context.setState({isMain: e.currentTarget.checked})}
                  /> На главную
                </label>
              </div>
            </div>
            <div style={{display: context.state.isMain ? '' : 'none'}}>
              <FormGroup
                context={context}
                stateName='mainPosition'
                label='Позиция на главной'
              />
              <div
                style={{display: mainVideos.length ? '':'none'}}
                className="alert alert-danger"
                role="alert"
              >
                <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true" />
                <span className="sr-only">Error:</span>
                Позиция совпадает со статьей: {mainVideos[0] ? mainVideos[0].title : ''}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="form-group">
              <div className="checkbox">
                <label>
                  <br/>
                  <input
                    type="checkbox"
                    checked={context.state.isTop}
                    onChange={e => context.setState({isTop: e.currentTarget.checked})}
                  /> В топ
                </label>
              </div>
            </div>
            <div style={{display: context.state.isTop ? '' : 'none'}}>
              <FormGroup
                context={context}
                stateName='topPosition'
                label='Позиция в топе'
              />
              <div
                style={{display: topVideos.length ? '':'none'}}
                className="alert alert-danger"
                role="alert"
              >
                <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true" />
                <span className="sr-only">Error:</span>
                Позиция совпадает со статьей: {topVideos[0] ? topVideos[0].title : ''}
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="row">
          <div className={`col col-sm-${context.state.videoCode ? 5 : 12}`}>
            <div className="form-group">
              <label htmlFor="shopNameModal">Код видео:</label>
              <input
                type="text"
                className="form-control"
                name="shopNameModal"
                placeholder="Введите код"
                value={context.state.videoCode}
                onChange={e => context.setState({videoCode: e.currentTarget.value})}
              />
            </div>
          </div>
          <div
            className="col col-sm-7 video-wrapper"
            dangerouslySetInnerHTML={{ __html: context.state.videoCode }}
            style={{height: context.state.videoCode ? 400 : 'auto'}}
          >
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12">
            <FormGroup
              context={context}
              stateName='extraLink'
              label='Ссылка на статью на сайте кипятком:'
            />
          </div>
        </div>
        <hr />
        <a
          href='#'
          onClick={context.createVideoHandler.bind(context)}
          className='btn btn-primary'
        >
          Добавить видео
        </a>

        <CropperModal
          width={900}
          height={465}
          id='logo-cropper'
          context={context}
          stateName='cover'
        />
      </div>
    );
  }
}
