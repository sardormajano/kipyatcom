import React, {Component} from 'react';
import {Link} from 'react-router';
import {idToName} from '../../../lib/collectionsRelated';
import ConfirmButton from '../../stateless/ConfirmButton';

export default class Videos extends Component {

  renderVideos(){
    const {context} = this.props,
          {deleteVideo} = context,
          {videos, shops} = context.props;

    return videos.map((video, index) => {
      return (
        <div
          className="col-xs-18 col-sm-6 col-md-3"
          key={index}
        >
          <div className="thumbnail" style={{height: '550px', maxHeight: '550px'}}>
            <img
              src={video.cover}
              style={{width: '100%'}}
            />
            <div className="caption">
              <h4>{video.title}</h4><hr />
              <p>
                <b>Подзаголовок:</b> {`${video.subtitle}`}<br /><br />
              </p>
              <Link
                to={`/admin/edit-video/${video._id}`}
                className="btn btn-info"
                role="button"
              >
                Редактировать
              </Link>{' '}
              <ConfirmButton
                onClick={deleteVideo.bind(context)}
                dataId={video._id}
                context={context}
              >
                Удалить
              </ConfirmButton>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    const {context} = this.props,
          {createVideo, updateVideo} = context;
    return (
      <div>
        <h1>Видео <Link to="/admin/new-video" className="btn btn-primary">Добавить новое</Link></h1>
        <hr /><br />
        <div className="row">
          {this.renderVideos()}
        </div>
      </div>
    );
  }
}
