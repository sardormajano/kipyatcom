import React, {Component} from 'react';
import {Link} from 'react-router';

import Select from '../../stateless/Select';
import Select2 from '../../stateless/Select2';
import FormGroup from '../../stateless/FormGroup';
import TextArea from '../../stateless/TextArea';
import DateTimePicker from '../../stateless/DateTimePicker';

import CropperModal from '../CropperModal';

export default class NewAlbum extends Component {
  render() {
    const {context} = this.props,
          {cover, mainPosition, topPosition, photos, thumbnails} = context.state,
          {albums, tags} = context.props,
          mainAlbums = albums.filter(album => album.mainPosition === mainPosition),
          topAlbums = albums.filter(album => album.topPosition === topPosition),
          filteredTags = tags.filter(t => t.category === 0);

    return (
      <form
        id="new-album"
      >
        <Link
          to='/admin/albums'
          className='btn btn-warning'
        >
          Вернуться к списку альбомов
        </Link>
        <br /><hr /><br />
        <div className="row">
          <div className="col-sm-4 text-center">
            Выберите обложку<br/><br/>
            <a href="#" className="thumbnail">
              <img
                width={150}
                src={cover}
                alt="..." />
            </a>
            <a
              href="#"
              className="btn btn-primary"
              onClick={(e) => {e.preventDefault(); context.changeLogoInput.click()}}
            >
              <i className="fa fa-plus"/>{' '}Изменить
            </a>
            <input
              style={{display: 'none'}}
              type='file'
              onChange={context.changeCoverHandler.bind(context)}
              ref={cli => context.changeLogoInput = cli}
            />
          </div>
          <div className="col col-sm-8">
            <div className="form-group">
              <label>Выберите заведение:</label>
              <Select2
                placeholder=''
                subscription='shops'
                single={true}
                context={context}
                options={context.props.shops}
                stateName='shopId'
              />
            </div>
            <FormGroup
              context={context}
              stateName='title'
              label='Заголовок:'
            />
            <FormGroup
              context={context}
              stateName='subtitle'
              label='Подзаголовок:'
            />
            <DateTimePicker
              label='Выберите дату и время:'
              context={context}
              stateName='date'
            />
            <div className="form-group">
              <label>Выберите фотографа:</label>
              <Select
                context={context}
                options={context.props.artists}
                stateName='artistId'
                placeholder='выберите фотографа'
              />
            </div><br /><br />
            <div className="row">
              <div className="col-md-3 col-sm-12">
                <div className='form-group'>
                  <label>Просмотры</label>
                  <input
                    className="form-control"
                    disabled='disabled'
                    value={context.state.views}
                  />
                </div>
              </div>
              <div className="col-md-3 col-sm-12">
                <FormGroup
                  context={context}
                  stateName='viewMultiplier'
                  label='Множитель:'
                />
              </div>
              <div className="col-md-3 col-sm-12">
                <div className='form-group'>
                  <label>Общее количество</label>
                  <input
                    className="form-control"
                    disabled='disabled'
                    value={parseInt(context.state.views) * parseInt(context.state.viewMultiplier)}
                  />
                </div>
              </div>
              <div className="col-md-3 col-sm-12">
                <div className="form-group">
                  <div className="checkbox">
                    <label>
                      <br/>
                      <input
                        type="checkbox"
                        checked={context.state.hideViews}
                        onChange={e => context.setState({hideViews: e.currentTarget.checked})}
                      /> Скрыть просмотры
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-group">
              <label>Тэги:</label>
              <Select2
                placeholder='Выберите тэг(и)'
                context={context}
                options={filteredTags}
                stateName='tags'
                subscription='tags'
              />
            </div>
            <div className="row">
              <div className="form-group">
                <div className="checkbox">
                  <label>
                    <br/>
                    <input
                      type="checkbox"
                      checked={context.state.isMain}
                      onChange={e => context.setState({isMain: e.currentTarget.checked})}
                    /> На главную
                  </label>
                </div>
              </div>
              <div style={{display: context.state.isMain ? '' : 'none'}}>
                <FormGroup
                  context={context}
                  stateName='mainPosition'
                  label='Позиция на главной'
                />
                <div
                  style={{display: mainAlbums.length ? '':'none'}}
                  className="alert alert-danger"
                  role="alert"
                >
                  <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true" />
                  <span className="sr-only">Error:</span>
                  Позиция совпадает со статьей: {mainAlbums[0] ? mainAlbums[0].title : ''}
                </div>
              </div>
            </div>
            <div className="row">
              <div className="form-group">
                <div className="checkbox">
                  <label>
                    <br/>
                    <input
                      type="checkbox"
                      checked={context.state.isTop}
                      onChange={e => context.setState({isTop: e.currentTarget.checked})}
                    /> В топ
                  </label>
                </div>
              </div>
              <div style={{display: context.state.isTop ? '' : 'none'}}>
                <FormGroup
                  context={context}
                  stateName='topPosition'
                  label='Позиция в топе'
                />
                <div
                  style={{display: topAlbums.length ? '':'none'}}
                  className="alert alert-danger"
                  role="alert"
                >
                  <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true" />
                  <span className="sr-only">Error:</span>
                  Позиция совпадает со статьей: {topAlbums[0] ? topAlbums[0].title : ''}
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr />
        <div className="form-group">
          <label htmlFor="exampleInputFile">Загрузить фото</label>
          <input name="photos-input" type="file" multiple onChange={context.addPhoto.bind(context)}/>
        </div>
        <hr />
        <a
          id="create-album-button"
          href='#'
          onClick={context.createAlbumHandler.bind(context)}
          className={`btn btn-primary ${context.state.photos.length ? '' : 'hidden'}`}
        >
          Создать альбом
        </a>

        <CropperModal
          width={430}
          height={495}
          id='logo-cropper'
          context={context}
          stateName='cover'
        />
      </form>
    );
  }
}
