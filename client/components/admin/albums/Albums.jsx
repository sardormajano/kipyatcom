import React, {Component} from 'react';
import {Link} from 'react-router';
import {idToName} from '../../../lib/collectionsRelated';
import ConfirmButton from '../../stateless/ConfirmButton';
import FormGroup from '../../stateless/FormGroup';

import moment from 'moment';

export default class Albums extends Component {

  componentDidUpdate() {
    const confirmButtons = $('.confirm-button');

    if(!confirmButtons.length) return ;

    $(confirmButtons).confirmation({
      title: 'Вы уверены?',
      btnOkLabel: 'Да.',
      btnCancelLabel: 'Нет.',
      placement: this.props.placement || 'top',
      onConfirm: () => {

      }
    });
  }

  renderAlbums(){
    const pageSize = 16,
          {context} = this.props,
          {deleteItem} = context,
          {searchWord, page, pagesCount} = context.state,
          {albums, shops} = context.props,
          offset = (page - 1) * pageSize,
          sWord = searchWord.toLowerCase();

    return albums.slice(0, pageSize).filter(album => {
      const date = moment.unix(album.date).format('ll');

      return album.title.toLowerCase().includes(sWord) ||
              album.subtitle.toLowerCase().includes(sWord) ||
              date.toLowerCase().includes(sWord) ||
              idToName(album.shopId, shops).toLowerCase().includes(sWord);
    }).map((album, index) => {
      const date = moment.unix(album.date).format('ll');

      return (
        <div
          className="col-xs-18 col-sm-6 col-md-3"
          key={index}
        >
          <div className="thumbnail" style={{height: '550px'}}>
            <img
              src={album.cover}
              style={{width: '50%', margin: 'auto'}}
            />
            <div
              className="caption"
            >
              <h4 style={{height: '3em'}}><b>{album.title}</b> - <br/><small>{date}</small></h4><hr />
              <p style={{height: '8em', textAlign: 'center'}}>
                <b>{album.subtitle}</b><br />
                <b>З-в:</b> {idToName(album.shopId, shops)}
              </p>
              <a
                href={`/admin/edit-album/${album._id}`}
                className="btn btn-info btn-sm"
                role="button"
              >
                Редактировать
              </a>&nbsp;
              <ConfirmButton
                context={context}
                itemId={album._id}
                className='btn-sm confirm-button'
              >
                Удалить
              </ConfirmButton>&nbsp;
              <a
                href='#'
                className="btn btn-info btn-sm"
                onClick={e => {
                  e.preventDefault();
                  context.toggleVisibility(album._id);
                }}
              >
                {album.hidden ?  'Показать' : 'Скрыть'}
              </a>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    const {context} = this.props,
          {createAlbum, updateAlbum} = context;
    return (
      <div>
        <h1>
          <div className="col-sm-4">
            Фотоальбомы {'  '}
            <Link to="/admin/new-album" className="btn btn-primary">Добавить новый</Link>
          </div>
          <br /><br />
          <div className="row">
            <input
              className="form-control"
              placeholder='поиск'
              value={context.state.searchWord}
              onChange={e => context.setState({searchWord: e.currentTarget.value})}
            />
          </div>
          <div className="row">
            <div className="form-inline">
              <div className="form-group">
                <input
                  type="button"
                  className="form-control"
                  value="Пред."
                  onClick={e => {
                    if(context.state.page === 1)
                      return ;
                    context.pageChangeHandler.call(context, context.state.page - 1);
                    context.setState({page: context.state.page - 1});
                  }}
                />
              </div>{' '}
              <div className="form-group">
                <input
                  className="form-control"
                  value={context.state.page}
                  onChange={e => {
                    const value = parseInt(e.currentTarget.value);

                    if(value > context.state.pagesCount ||
                        value < 1)
                      return ;

                    context.setState({page: value});
                    context.pageChangeHandler.call(context, value);
                  }}
                />
              </div>{' '}
              <div className="form-group">
                <input
                  type="button"
                  className="form-control"
                  value="След."
                  onClick={e => {
                    if(parseInt(context.state.page) === parseInt(context.state.pagesCount))
                      return ;

                    context.pageChangeHandler.call(context, context.state.page + 1);
                    context.setState({page: context.state.page + 1});
                  }}
                />
              </div>{' '}
              <div className="form-group">
                <input disabled className="form-control" value={`из ${context.state.pagesCount}`} />
              </div>
            </div>
          </div>
          <br />
        </h1>
        <hr /><br />
        <div className="row">
          {this.renderAlbums()}
        </div>
      </div>
    );
  }
}
