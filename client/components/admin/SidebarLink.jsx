import React, {Component} from 'react';
import {Link} from 'react-router';

export default class SidebarLink extends Component {
    render() {
        const {to, title, pathname} = this.props;

        return (
            <li>
                <Link to={to} className={pathname.includes(to) ? 'active' : ''}>{title}</Link>
            </li>
        );
    }
}
