import React, {Component} from 'react';

import Select from '../../stateless/Select';
import FormGroup from '../../stateless/FormGroup';
import {Link} from 'react-router';

export default class Users extends Component {
  renderUsers(){
    const {context } = this.props;
    return context.props.users.map((user, index)=> {
      return (
        <tr key={index}>
          <td>{index + 1}</td>
          <td>{user.profile.first_name}</td>
          <td>{user.profile.last_name}</td>
          <td>{user.roles[0]}</td>
          <td>
            <div className="btn-group" role="group">
              <Link
                type="button"
                className="btn btn-warning"
                to={`/admin/edit-user/${user._id}`}
              >
                <i className="fa fa-pencil" />
              </Link>
              <button
                type="button"
                className="btn btn-danger"
                data-id={user._id}
                onClick={context.deleteUser.bind(context)}
              >
                <i className="fa fa-trash" />
              </button>
            </div>
          </td>
        </tr>
      )
    });
  }

  render() {
    const {context} = this.props,
          {createUser, updateUser} = context,
          {roles} = context.props;

    return (
      <div>
        <h1>Пользователи</h1>
        <div>
          <div className="panel panel-default">
            <div className="panel-heading">
              <Link
                className="btn btn-success"
                to="/admin/new-user"
              >
                Добавить&nbsp;<i className="fa fa-plus"></i>
              </Link>
            </div>
            <div className="panel-body">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Имя</th>
                    <th>Фамилия</th>
                    <th>Роль</th>
                    <th>Действия</th>
                  </tr>
                </thead>
                <tbody>
                  {this.renderUsers()}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
