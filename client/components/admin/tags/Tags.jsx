import React, {Component} from 'react';


export default class Tags extends Component {

  renderTags(){
    const {context} = this.props,
          {currentCategory} = context.state,
          {tags} = context.props,
          currentTags = tags.filter(t => t.category === currentCategory);

    return currentTags.map((tag, index)=> {
      return (
        <tr key={index}>
          <td>{index + 1}</td>
          <td>{tag.name}</td>
          <td>
            <div className="btn-group" role="group">
              <button
                type="button"
                className="btn btn-warning"
                data-id={tag._id}
                data-toggle="modal"
                data-target="#tagEditModal"
                onClick={context.preUpdateTag.bind(context)}
              >
                <i className="fa fa-pencil" />
              </button>
              <button
                type="button"
                className="btn btn-danger"
                data-id={tag._id}
                onClick={context.deleteTag.bind(context)}
              >
                <i className="fa fa-trash" />
              </button>
            </div>
          </td>
        </tr>
      )
    });
  }

  render() {
    const {context} = this.props,
          {createTag, updateTag} = context;
    return (
      <div>
        <h1>Тэги</h1>
        <div>
          <div className="panel panel-default">
            <div className="panel-heading">
              <a className="btn btn-success" data-toggle="modal" data-target="#tagModal">Добавить&nbsp;<i className="fa fa-plus"></i></a>
            </div>
            <div className="panel-body">
              <ul className="nav nav-tabs">
                <li
                  role="presentation"
                  className={`${context.state.currentCategory === 0 ? 'active' : ''}`}>
                  <a
                    href="#"
                    onClick={(e) => {e.preventDefault(); context.setState({currentCategory: 0})}}
                  >
                    Фотоальбомы
                  </a>
                </li>
                <li
                  role="presentation"
                  className={`${context.state.currentCategory === 1 ? 'active' : ''}`}>
                  <a
                    href="#"
                    onClick={(e) => {e.preventDefault(); context.setState({currentCategory: 1})}}
                  >
                    Анонсы
                  </a>
                </li>
                <li
                  role="presentation"
                  className={`${context.state.currentCategory === 2 ? 'active' : ''}`}>
                  <a
                    href="#"
                    onClick={(e) => {e.preventDefault(); context.setState({currentCategory: 2})}}
                  >
                    Заведения
                  </a>
                </li>
                <li
                  role="presentation"
                  className={`${context.state.currentCategory === 3 ? 'active' : ''}`}>
                  <a
                    href="#"
                    onClick={(e) => {e.preventDefault(); context.setState({currentCategory: 3})}}
                  >
                    Статьи
                  </a>
                </li>
                <li
                  role="presentation"
                  className={`${context.state.currentCategory === 4 ? 'active' : ''}`}>
                  <a
                    href="#"
                    onClick={(e) => {e.preventDefault(); context.setState({currentCategory: 4})}}
                  >
                    Видео
                  </a>
                </li>
              </ul>
              <br />
              <br />
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Название тэга</th>
                    <th>Действия</th>
                  </tr>
                </thead>
                <tbody>
                  {this.renderTags()}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div className="modal fade" role="dialog" id="tagModal">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title">Добавление тэга</h4>
              </div>
              <div className="modal-body">
                <form id="create-tag-form">
                  <div className="form-group">
                    <label htmlFor="tagNameModal">Название тэга</label>
                    <input type="text" className="form-control" name="tagNameModal" placeholder="Тэг" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" className="btn btn-primary" onClick={createTag.bind(context)} data-dismiss="modal">Сохранить</button>
              </div>
            </div>
          </div>
        </div>

        <div className="modal fade" role="dialog" id="tagEditModal">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title">Редактирование тэга</h4>
              </div>
              <div className="modal-body">
                <form id="edit-tag-form">
                  <div className="form-group">
                    <label htmlFor="tagNameModal">Название тэга</label>
                    <input type="text" className="form-control" name="name" placeholder="Тэг" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" className="btn btn-primary" onClick={updateTag.bind(context)} data-dismiss="modal">Сохранить</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
