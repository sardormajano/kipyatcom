import React, {Component} from 'react';

import Select from '../../stateless/Select';
import FormGroup from '../../stateless/FormGroup';

import {idToName} from '../../../lib/collectionsRelated';

export default class Contacts extends Component {

  renderContacts(){
    const {context } = this.props;
    return context.props.contacts.map((contact, index)=> {
      return (
        <tr key={index}>
          <td>{index + 1}</td>
          <td>{idToName(contact.cityId, context.props.cities)}</td>
          <td>{contact.email}</td>
          <td>{contact.phone}</td>
          <td>
            <div className="btn-group" role="group">
              <button
                type="button"
                className="btn btn-warning"
                data-id={contact._id}
                data-toggle="modal"
                data-target="#contactEditModal"
                onClick={context.preUpdateContact.bind(context)}
              >
                <i className="fa fa-pencil" />
              </button>
              <button
                type="button"
                className="btn btn-danger"
                data-id={contact._id}
                onClick={context.deleteContact.bind(context)}
              >
                <i className="fa fa-trash" />
              </button>
            </div>
          </td>
        </tr>
      )
    });
  }

  render() {
    const {context} = this.props,
          {createContact, updateContact} = context;
    return (
      <div>
        <h1>Контакты</h1>
        <div>
          <div className="panel panel-default">
            <div className="panel-heading">
              <a className="btn btn-success" data-toggle="modal" data-target="#contactModal">Добавить&nbsp;<i className="fa fa-plus"></i></a>
            </div>
            <div className="panel-body">
              <table className="table table-hover">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Город</th>
                    <th>Email</th>
                    <th>Телефон</th>
                  </tr>
                </thead>
                <tbody>
                  {this.renderContacts()}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div className="modal fade" role="dialog" id="contactModal">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title">Добавление контакта</h4>
              </div>
              <div className="modal-body">
                <div className="form-group">
                  <label>Выберите город:</label>
                  <Select
                    context={context}
                    options={context.props.cities}
                    stateName='cityId'
                    placeholder='выберите город'
                  />
                </div><br /><br/>
                <FormGroup
                  context={context}
                  stateName='domain'
                  label='Домен:'
                />
                <FormGroup
                  context={context}
                  stateName='phone'
                  label='Телефон:'
                />
                <FormGroup
                  context={context}
                  stateName='email'
                  label='Email:'
                />
                <FormGroup
                  context={context}
                  stateName='insta'
                  label='Insta:'
                />
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" className="btn btn-primary" onClick={createContact.bind(context)} data-dismiss="modal">Сохранить</button>
              </div>
            </div>
          </div>
        </div>

        <div className="modal fade" role="dialog" id="contactEditModal">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title">Редактирование фотографа</h4>
              </div>
              <div className="modal-body">
                <div className="form-group">
                  <label>Выберите город:</label>
                  <Select
                    context={context}
                    options={context.props.cities}
                    stateName='cityId'
                    placeholder='выберите город'
                  />
                </div><br /><br/>
                <FormGroup
                  context={context}
                  stateName='domain'
                  label='Домен:'
                />
                <FormGroup
                  context={context}
                  stateName='phone'
                  label='Телефон:'
                />
                <FormGroup
                  context={context}
                  stateName='email'
                  label='Email:'
                />
                <FormGroup
                  context={context}
                  stateName='insta'
                  label='Insta:'
                />
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" className="btn btn-primary" onClick={updateContact.bind(context)} data-dismiss="modal">Сохранить</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
