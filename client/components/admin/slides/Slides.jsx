import React, {Component} from 'react';
import {Link} from 'react-router';
import {idToName} from '../../../lib/collectionsRelated';
import ConfirmButton from '../../stateless/ConfirmButton';

export default class Slides extends Component {

  componentDidUpdate() {
    const confirmButtons = $(this.cw).find('.confirm-button');

    if(!confirmButtons.length) return ;

    $(confirmButtons).confirmation({
      title: 'Вы уверены?',
      btnOkLabel: 'Да.',
      btnCancelLabel: 'Нет.',
      placement: this.props.placement || 'top',
      onConfirm: () => {

      }
    });
  }

  renderSlides(){
    const {context} = this.props,
          {deleteSlide} = context,
          {slides, shops} = context.props;

    return slides.map((slide, index) => {
      return (
        <div
          className="col-xs-18 col-sm-6 col-md-3"
          key={index}
        >
          <div className="thumbnail" style={{height: '510px'}}>
            <img
              src={slide.cover}
              style={{width: '70%', margin: 'auto'}}
            />
            <div className="caption">
              <h4>{slide.title}</h4><hr />
              <p>
                <b>Ссылка:</b> {`${slide.link}`}<br /><br />
              </p>
              <Link
                to={`/admin/edit-slide/${slide._id}`}
                className="btn btn-info btn-sm"
                role="button"
              >
                Редактировать
              </Link>{' '}
              <ConfirmButton
                className='btn-sm confirm-button'
                itemId={slide._id}
                context={context}
              >
                Удалить
              </ConfirmButton>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    const {context} = this.props,
          {createSlide, updateSlide} = context;
    return (
      <div ref={cw => this.cw = cw}>
        <h1>Слайды <Link to="/admin/new-slide" className="btn btn-primary">Добавить новый</Link></h1>
        <hr /><br />
        <div className="row">
          {this.renderSlides()}
        </div>
      </div>
    );
  }
}
