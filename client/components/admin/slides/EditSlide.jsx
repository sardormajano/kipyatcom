import React, {Component} from 'react';
import {Link} from 'react-router';

import Select from '../../stateless/Select';
import Select2 from '../../stateless/Select2';
import FormGroup from '../../stateless/FormGroup';
import TextArea from '../../stateless/TextArea';
import DateTimePicker from '../../stateless/DateTimePicker';

import CropperModal from '../CropperModal';

export default class EditSlide extends Component {

  render() {
    const {context} = this.props,
          {cover} = context.state;

    return (
      <div>
        <Link
          to='/admin/slides'
          className='btn btn-warning'
        >
          Вернуться к списку альбомов
        </Link>
        <br /><hr /><br />
        <div className="row">
          <div className="col-sm-4 text-center">
            Выберите фото для слайда<br/><br/>
            <a href="#" className="thumbnail">
              <img
                width={150}
                src={cover}
                alt="..." />
            </a>
            <a
              href="#"
              className="btn btn-primary"
              onClick={(e) => {context.changeLogoInput.click()}}
            >
              <i className="fa fa-plus"/>{' '}Изменить
            </a>
            <input
              style={{display: 'none'}}
              type='file'
              onChange={context.changeCoverHandler.bind(context)}
              ref={cli => context.changeLogoInput = cli}
              multiple
            />
          </div>
          <div className="col col-sm-8">
            <FormGroup
              context={context}
              stateName='link'
              label='Ссылка:'
            />
          </div>
        </div>
        <hr />
        <a
          href='#'
          onClick={context.editSlideHandler.bind(context)}
          className='btn btn-primary'
        >
          Сохранить
        </a>

        <CropperModal
          width={550}
          height={736}
          id='logo-cropper'
          context={context}
          stateName='cover'
        />
      </div>
    );
  }
}
