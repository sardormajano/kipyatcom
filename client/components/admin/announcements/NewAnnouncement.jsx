import React, {Component} from 'react';
import {Link} from 'react-router';

import Select from '../../stateless/Select';
import Select2 from '../../stateless/Select2';
import FormGroup from '../../stateless/FormGroup';
import TextArea from '../../stateless/TextArea';
import DateTimePicker from '../../stateless/DateTimePicker';
import Editor from '../../stateless/Editor';

import CropperModal from '../CropperModal';

export default class NewAnnouncement extends Component {

  render() {
    const {context} = this.props,
          {cover, mainPosition, topPosition} = context.state,
          {announcements, tags} = context.props,
          mainAnnouncements = announcements.filter(announcement => announcement.mainPosition === mainPosition),
          topAnnouncements = announcements.filter(announcement => announcement.topPosition === topPosition),
          filteredTags = tags.filter(t => t.category === 1);

    return (
      <div>
        <Link
          to='/admin/announcements'
          className='btn btn-warning'
        >
          Вернуться к списку анонсов
        </Link>
        <br /><hr /><br />
        <div className="row">
          <div className="col-sm-4 text-center">
            Выберите обложку<br/><br/>
            <a href="#" className="thumbnail">
              <img
                width={150}
                src={cover}
                alt="..." />
            </a>
            <a
              href="#"
              className="btn btn-primary"
              onClick={(e) => {e.preventDefault(); context.changeLogoInput.click()}}
            >
              <i className="fa fa-plus"/>{' '}Изменить
            </a>
            <input
              style={{display: 'none'}}
              type='file'
              onChange={context.changeCoverHandler.bind(context)}
              ref={cli => context.changeLogoInput = cli}
              multiple
            />
          </div>
          <div className="col col-sm-8">
            <div className="form-group">
              <label>Выберите заведение:</label>
              <Select
                context={context}
                options={context.props.shops}
                stateName='shopId'
                placeholder='выберите заведение'
              />
            </div><br/><br/>
            <FormGroup
              context={context}
              stateName='title'
              label='Заголовок:'
            />
            <FormGroup
              context={context}
              stateName='subtitle'
              label='Подзаголовок:'
            />
            <DateTimePicker
              label='Выберите дату и время:'
              context={context}
              stateName='date'
            />
            <div className="row">
              <div className="col-md-6 col-sm-12">
                <FormGroup
                  context={context}
                  stateName='linkName'
                  label='Название ссылки:'
                />
              </div>
              <div className="col-md-6 col-sm-12">
                <FormGroup
                  context={context}
                  stateName='link'
                  label='Ссылка:'
                />
              </div>
            </div>
            <div className="checkbox">
              <label>
                <input
                  type="checkbox"
                  checked={context.state.hasContent}
                  onChange={e => context.setState({hasContent: e.currentTarget.checked})}
                /> Контент
              </label>
            </div>
            <div className="form-group">
              <button
                type="button"
                className="btn btn-success"
                onClick={(e) => {e.preventDefault(); this.mp.click();}}
              >
                {context.state.mainPhoto || 'Добавьте главное фото'}
              </button>
              <input
                ref={mp => this.mp = mp}
                type="file"
                onChange={context.addMainPhoto.bind(context)}
                style={{display: 'none'}}
              />
            </div>
            <FormGroup
              context={context}
              stateName='author'
              label='Имя автора:'
            />
            <br /><br />
          </div>
        </div>
        <hr />
        <div className="row">
          <div className="form-group">
            <label>Начало контента:</label>
            <Editor
              id="content"
              defaultValue="Введите контент"
              height={300}
            />
          </div>
          <div className="form-group">
            <label>Тэги:</label>
            <Select2
              placeholder='Выберите тэг(и)'
              context={context}
              options={filteredTags}
              stateName='tags'
            />
          </div>
        </div>
        <div className="row">
          <div className="form-group">
            <div className="checkbox">
              <label>
                <br/>
                <input
                  type="checkbox"
                  checked={context.state.isMain}
                  onChange={e => context.setState({isMain: e.currentTarget.checked})}
                /> На главную
              </label>
            </div>
          </div>
          <div style={{display: context.state.isMain ? '' : 'none'}}>
            <FormGroup
              context={context}
              stateName='mainPosition'
              label='Позиция на главной'
            />
            <div
              style={{display: mainAnnouncements.length ? '':'none'}}
              className="alert alert-danger"
              role="alert"
            >
              <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true" />
              <span className="sr-only">Error:</span>
              Позиция совпадает со статьей: {mainAnnouncements[0] ? mainAnnouncements[0].title : ''}
            </div>
          </div>
        </div>
        <div className="row">
          <div className="form-group">
            <div className="checkbox">
              <label>
                <br/>
                <input
                  type="checkbox"
                  checked={context.state.isTop}
                  onChange={e => context.setState({isTop: e.currentTarget.checked})}
                /> В топ
              </label>
            </div>
          </div>
          <div style={{display: context.state.isTop ? '' : 'none'}}>
            <FormGroup
              context={context}
              stateName='topPosition'
              label='Позиция в топе'
            />
            <div
              style={{display: topAnnouncements.length ? '':'none'}}
              className="alert alert-danger"
              role="alert"
            >
              <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true" />
              <span className="sr-only">Error:</span>
              Позиция совпадает со статьей: {topAnnouncements[0] ? topAnnouncements[0].title : ''}
            </div>
          </div>
        </div>
        <hr />
        <a
          href='#'
          onClick={context.createAnnouncementHandler.bind(context)}
          className='btn btn-primary'
        >
          Добавить анонс
        </a>

        <CropperModal
          id='logo-cropper'
          context={context}
          stateName='cover'
          width={595}
          height={290}
        />
      </div>
    );
  }
}
