import React, {Component} from 'react';
import {Link} from 'react-router';
import {idToName} from '../../../lib/collectionsRelated';
import ConfirmButton from '../../stateless/ConfirmButton';

export default class Announcements extends Component {

  renderAnnouncements(){
    const {context} = this.props,
          {deleteAnnouncement} = context,
          {announcements, shops} = context.props;

    return announcements.map((announcement, index) => {
      return (
        <div
          className="col-xs-18 col-sm-6 col-md-3"
          key={index}
        >
          <div className="thumbnail" style={{height: '550px', maxHeight: '550px'}}>
            <img
              src={announcement.cover}
              style={{width: '100%'}}
            />
            <div className="caption">
              <h4>{announcement.title}</h4><hr />
              <p>
                <b>Подзаголовок:</b> {`${announcement.subtitle}`}<br /><br />
                <b>Заведение:</b> {idToName(announcement.shopId, shops)}
              </p>
              <Link
                to={`/admin/edit-announcement/${announcement._id}`}
                className="btn btn-info"
                role="button"
              >
                Редактировать
              </Link>{' '}
              <ConfirmButton
                onClick={deleteAnnouncement.bind(context)}
                dataId={announcement._id}
                context={context}
              >
                Удалить
              </ConfirmButton>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    const {context} = this.props,
          {createAnnouncement, updateAnnouncement} = context;
    return (
      <div>
        <h1>Анонсы <Link to="/admin/new-announcement" className="btn btn-primary">Добавить новый</Link></h1>
        <hr /><br />
        <div className="row">
          {this.renderAnnouncements()}
        </div>
      </div>
    );
  }
}
