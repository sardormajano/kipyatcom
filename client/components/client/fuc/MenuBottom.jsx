import React, {Component} from 'react';

import DynamicButton from '../../stateless/DynamicButton';

import {Link} from 'react-router';

export default class MenuBottom extends Component {
  componentDidMount() {
    var $menu = $(this.mb);

    $(window).scroll(function(){
      if ( $(this).scrollTop() > 252 && $menu.hasClass("default") ){
        $menu.fadeOut('fast',function(){
          $(this).removeClass("default")
               .addClass("fixed transbg")
               .fadeIn('fast');
        });
      } else if($(this).scrollTop() <= 252 && $menu.hasClass("fixed")) {
        $menu.fadeOut('fast',function(){
          $(this).removeClass("fixed transbg")
               .addClass("default")
               .fadeIn('fast');
        });
      }
    });

    $menu.hover(
      function(){
        if( $(this).hasClass('fixed') ){
          $(this).removeClass('transbg');
        }
      },
      function(){
        if( $(this).hasClass('fixed') ){
          $(this).addClass('transbg');
        }
      });
  }

  render() {
    const {context} = this.props,
          {pathname} = context.props.location;

    return (
      <div>
        <div
          ref={mb => this.mb = mb}
          id="menu-bottom"
          className="default"
        >
          <div id="menu-bottom-1">
            <div className="menu-bottom-2">
              <div className="logo_big_0">
                <Link className="logo_big logo_mobile_nav" to="/">
                </Link>
              </div>
              <div className="bottom-cell">
                <div className="bottom-cell_bottom">
                  <ul className="nav">
                    <li className="city">
                      <a>Астана <i className="fa fa-angle-down" aria-hidden="true" /></a>
                      <div style={{left: 0, borderRadius: '10px 10px 0px 0px', MozBorderRadius: '10px 10px 0px 0px', WebkitBorderRadius: '10px 10px 0px 0px', border: '0px solid #000000'}}>
                        <div className="nav-column">
                          <ul>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Астана</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Алматы</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Актау</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Актобе</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Қарағанды</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Кокшетау</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Костанай</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Петропавл</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Уральск</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
                <ul className="nav">
                  <li className="nav-search">
                    <form action="#">
                      <input type="text" placeholder="Поиск по сайту..." />
                      <input type="submit" defaultValue style={{color: 'transparent'}}/>
                    </form>
                  </li>
                </ul>
              </div>
              <ul className="menu">
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/']}
                    to='/'
                  >
                    <i className="fa fa-home" aria-hidden="true" />
                  </DynamicButton>
                </li>
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/albums']}
                    to='/albums'
                  >
                    Фото
                  </DynamicButton>
                </li>
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/announcements']}
                    to='/announcements'
                  >
                    Анонсы
                  </DynamicButton>
                </li>
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/shops']}
                    to='/shops'
                  >
                    Заведения
                  </DynamicButton>
                </li>
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/articles']}
                    to='/articles'
                  >
                    Статьи
                  </DynamicButton>
                </li>
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/videos']}
                    to='/videos'
                  >
                    Видео
                  </DynamicButton>
                </li>
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/contacts']}
                    to='/contacts'
                  >
                    Контакты
                  </DynamicButton>
                </li>
              </ul>
            </div>
          </div>
          <div className="header_adaptive">
            {/* Header */}
            <header id="header">
              <h1>
                <Link className="logo_big" to="/">
                </Link>
              </h1>
              <ul className="nav">
                <li className="city">
                  <a>Астана <i className="fa fa-angle-down" aria-hidden="true" /></a>
                  <div style={{left: 0}}>
                    <div className="nav-column">
                      <ul>
                        <li className="city2">
                          <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Астана</a>
                        </li>
                        <li className="city2">
                          <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Алматы</a>
                        </li>
                        <li className="city2">
                          <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Актау</a>
                        </li>
                        <li className="city2">
                          <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Актобе</a>
                        </li>
                        <li className="city2">
                          <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Қарағанды</a>
                        </li>
                        <li className="city2">
                          <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Кокшетау</a>
                        </li>
                        <li className="city2">
                          <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Костанай</a>
                        </li>
                        <li className="city2">
                          <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Петропавл</a>
                        </li>
                        <li className="city2">
                          <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Уральск</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </li>
              </ul>
              <nav className="links">
                <ul>
                  <li>
                    <a href="#" className="active"> <i className="fa fa-home" aria-hidden="true" />
                    </a>
                  </li>
                  <li><a href="#">Фото</a>
                  </li>
                  <li><a href="#">Анонсы</a>
                  </li>
                  <li><a href="#">Заведения</a>
                  </li>
                  <li><a href="#">Статьи</a>
                  </li>
                  <li><a href="#">Видео</a>
                  </li>
                  <li><a href="#">Контакты</a>
                  </li>
                </ul>
              </nav>
              <nav className="main">
                <ul>
                  <li className="menu">
                    <a className="fa-bars" href="#menu">Menu</a>
                  </li>
                </ul>
              </nav>
            </header>
          </div>
        </div>
        <div className="sotial_mob">
          <a className="sotial_icons_insta" target="_black" href="http://instagram.com/kipyatcom" style={{background: '#c82456'}}>
            <i className="fa fa-instagram" aria-hidden="true" />
          </a>
          <a className="sotial_icons_insta" target="_black" href="https://www.facebook.com/kipyatcom" style={{background: '#2e4da7'}}>
            <i className="fa fa-facebook" aria-hidden="true" style={{fontSize: '1.1rem', lineHeight: '2.1rem', float: 'left', textAlign: 'center', width: '100%'}} />
          </a>
          <a className="sotial_icons_insta" target="_black" href="https://vk.com/kipyatcom" style={{background: '#4c75a3'}}>
            <i className="fa fa-vk" aria-hidden="true" style={{fontSize: '1.1rem', lineHeight: '1.9rem', float: 'left', textAlign: 'center', width: '100%'}} />
          </a>
          <a className="sotial_icons_insta" target="_black" href="whatsapp://send?text=Здравствуйте!&phone=+77012270988&abid=+77012270988" style={{background: '#25d366'}}>
            <i className="fa fa-whatsapp" aria-hidden="true" style={{fontSize: '1.3rem', lineHeight: '1.9rem', float: 'left', textAlign: 'center', width: '100%'}} />
          </a>
        </div>
      </div>
    );
  }
}
