import React, {Component} from 'react';

import moment from 'moment';

import {Link} from 'react-router';

export default class HomeArticle extends Component {
  render() {
    const {context} = this.props,
          {articles} = context.props,
          theArticle = articles[0],
          date = theArticle ? moment.unix(theArticle.date).format('ll').split(' ') : [];

    return (
      <div className="col-xs-00  col-sm-00 col-md-00 col-lg-00 col-xl-00" style={{paddingTop: 10, paddingBottom: 10}}>
        <div className="heading-fancy heading-line">
          <h4>
            <Link to="/articles">
              <i className="fa fa-newspaper-o" aria-hidden="true" style={{padding: '0 5px 0 0'}} />
              Статьи
            </Link>
          </h4>
        </div>
        <div className="container">
          <div className="card cart-block">
            <Link
              to={`/single-article/${theArticle ? theArticle._id : ''}`}
              className="post-hov"
            >
              <div className="card-img">
                <div className="post-date-big">
                  <span>{date[0]}</span>
                  <p>{date[1]}</p>
                </div>
                <div className="post-thumbnail-content">
                  <h3>{theArticle ? theArticle.title.toUpperCase() : ''}</h3>
                  <p>{theArticle ? theArticle.subtitle.toUpperCase() : ''}</p>
                </div>
                <div
                  style={{height: '100%', width: '100%', backgroundSize: '100% 100%', backgroundColor: '#ccc', backgroundImage: `url(${theArticle ? theArticle.cover : ''})`}}
                  className="image-wrapper"
                >
                  <img alt src='/img/article/0.jpg' />
                </div>
              </div>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
