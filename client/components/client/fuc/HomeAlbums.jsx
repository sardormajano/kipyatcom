import React, {Component} from 'react';

import {Link} from 'react-router';

import moment from 'moment';
import {idToName} from '/client/lib/collectionsRelated';
import {createPositionCompareFunction} from '/client/lib/general.js';

export default class HomeAlbum extends Component {
  render() {
    const {context} = this.props,
          {albums, artists} = context.props,
          cols = [[],[],[]],
          compareFunction = createPositionCompareFunction('mainPosition', 'date'),
          mainAlbums = albums.filter(album => album.isMain).sort(compareFunction),
          nonMainAlbums = albums.filter(album => !album.isMain),
          finalAlbums = mainAlbums.concat(nonMainAlbums).slice(0, 6);

    finalAlbums.forEach((item, index) => {
      const remainder = index % 3,
            date = moment.unix(item.date).format('ll').split(' '),
            dayMonth = `${date[0]} ${date[1]}`,
            year = `${date[2]}`,
            albumThumbnail = (
              <div className="album_1" key={index}>
                <a className="fotograf">
                  <i className="fa fa-camera" aria-hidden="true" style={{padding: '0 3px 0 0'}} />
                  {idToName(item.artistId, artists)}
                </a>
                <a
                  className="View"
                  style={{display: item.hideViews ? 'none' : ''}}
                >
                  <i className="fa fa-eye" aria-hidden="true" style={{padding: '0 3px 0 0'}} />
                  {parseInt(item.views) * parseInt(item.viewMultiplier)}
                </a>
                <div className="post-entry-meta">
                  <div className="post-entry-meta-category">
                    <span className="label label-date">{dayMonth}</span>{' '}
                    <span className="label label-year">{year}</span>
                  </div>
                  <div className="post-entry-meta-title">
                    <h2>
                      <Link to={`/single-album/${item._id}`}>
                        {item.title}
                      </Link>
                    </h2>
                  </div>
                  <span className="post-title">{item.subtitle}</span>
                </div>
                <Link
                  className="album_pic crop"
                  to={`/single-album/${item._id}`}
                  style={{height: '100%', width: '100%', backgroundColor: '#ccc', backgroundImage: `url(${item.cover})`}}>
                  <img src='/img/albums/0.gif' />
                </Link>
              </div>
            );

      cols[remainder].push(albumThumbnail);
    });

    return (
      <div style={{paddingLeft: 0}}>
        <div className="col-xs-12  col-sm-7 col-md-7 col-xl-4-2 col-xxl-4-2">
          {cols[0]}
        </div>
        <div className="col-xs-12  col-sm-8-right col-md-8-right col-xl-4-2 col-xxl-4-2">
          {cols[1]}
        </div>
        <div className="col-xs-12  col-sm-8-right-2 col-md-8-right-2 col-xl-4-2 col-xxl-4-2">
          {cols[2]}
        </div>
      </div>
    );
  }
}
