import React, {Component} from 'react';

export default class Insta extends Component {
  componentWillMount() {
    Meteor.call('fetch.insta', (err, result) => {
      if(err) console.log(err);
      else {
        this.items = result.items;
        this.forceUpdate();
      }
    });
  }

  componentDidUpdate() {
    if(this.items && !this.prepared) {
      this.prepareCarousel();
      this.prepared = true;
    }
  }

  prepareCarousel() {
    const self = this,
          theCarousel = $(this.cw).find('.carousel');

    $('#insta-carousel .carousel-button-right').click(function(e){
      e.preventDefault();
    	self.right_carusel(theCarousel);
    	return false;
    });
    $('#insta-carousel .carousel-button-left').click(function(e){
      e.preventDefault();
    	self.left_carusel(theCarousel);
    	return false;
    });

    setInterval(function(){
  		if (!theCarousel.is('.hover'))
  			self.right_carusel(theCarousel);
  	}, 3000);

    theCarousel.on('mouseenter', function(){$(this).addClass('hover')})
    theCarousel.on('mouseleave', function(){$(this).removeClass('hover')})
  }

  left_carusel(carusel) {
     var block_width = $('#insta-carousel .carousel-items .carousel-block').outerWidth();
     $('#insta-carousel .carousel-items .carousel-block').eq(-1).clone().prependTo($('#insta-carousel .carousel-items'));
     $('#insta-carousel .carousel-items').css({"left":"-"+block_width+"px"});
     $('#insta-carousel .carousel-items .carousel-block').eq(-1).remove();
     $('#insta-carousel .carousel-items .carousel-items').animate({left: "0px"}, 200);

  }
  right_carusel(carusel) {
    var block_width = $(this.cw).find('.carousel-block').outerWidth();
    $(this.cw).find(".carousel-items").animate({
      left: "-" + block_width + "px"
    }, 200, function() {
      $('#insta-carousel .carousel-items .carousel-block').eq(0).clone().appendTo($('#insta-carousel .carousel-items'));
      $('#insta-carousel .carousel-items .carousel-block').eq(0).remove();
      $('#insta-carousel .carousel-items').css({"left": "0px"});
    });
  }

  renderInsta() {
    if(!this.items) {
      return <div></div>
    }
    else {
      return this.items.slice(0, 11).map((item, index) => {
        return (
          <div
            key={index}
            className="carousel-block"
          >
            <a href={item.link} target="_black">
              <img src={item.images.low_resolution.url} alt />
            </a>
          </div>
        );
      })
    }
  }

  render() {
    return (
      <div
        ref={cw => this.cw = cw}
        id="insta-carousel"
        className="carousel"
      >
        <div className="heading-fancy-insta heading-line-insta">
          <h4> <p className="insta_txt">Мы в instagram:</p> <i className="fa fa-instagram" aria-hidden="true" style={{padding: '0 3px 0 0'}} /> <a target="_blanc" href="http://instagram.com/kipyatcom">kipyatcom</a></h4>
        </div>
        <div className="carousel-button-left"><a href="#"> <i className="fa fa-arrow-left" aria-hidden="true" /> </a></div>
        <div className="carousel-button-right"><a href="#"> <i className="fa fa-arrow-right" aria-hidden="true" /> </a></div>
        <div className="carousel-wrapper">
          <div className="carousel-items">
            {this.renderInsta()}
          </div>
        </div>
      </div>
    );
  }
}
