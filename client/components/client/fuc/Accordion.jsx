import React, {Component} from 'react';

export default class Accordion extends Component {
  componentWillMount() {
    this.Accordion = function(el, multiple) {
      this.el = el || {};
      this.multiple = multiple || false;

      // Variables privadas
      var links = this.el.find('.link');
      // Evento
      links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }

    this.Accordion.prototype.dropdown = function(e) {
      var $el = e.data.el;
        $this = $(this),
        $next = $this.next();

      $next.slideToggle();
      $this.parent().toggleClass('open');

      if (!e.data.multiple) {
        $el.find('.submenu-b').not($next).slideUp().parent().removeClass('open');
      };
    }
  }

  componentDidMount() {
    const accordion = new this.Accordion($(this.accordion), false);
  }

  render() {
    return (
      <ul
        ref={ac => this.accordion = ac}
        id="accordion"
        className="accordion"
      >
        {this.props.children}
      </ul>
    );
  }
}
