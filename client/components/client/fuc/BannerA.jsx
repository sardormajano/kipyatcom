import React, {Component} from 'react';
import {getRandomInt} from '/client/lib/general.js';

export default class BannerA extends Component {
  render() {
    const {context} = this.props,
          {banners} = context.props,
          banner = banners.filter(item => item._id === '0-a')[0];

    if(!banner || banner.off) {
      return <div></div>;
    }

    const randomInt = getRandomInt(0, banner.photos.length);

    const image = banner ? banner.photos[randomInt] : '';

    if(!banner.photos.length) {
      return <div></div>;
    }

    return (
      <div className="content1-adv">
        <div className="content2-adv">
          <a href={banner.links[randomInt]} target="_blank">
            <img src={image} />
          </a>
        </div>
      </div>
    );
  }
}
