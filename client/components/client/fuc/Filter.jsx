import React, {Component} from 'react';
import ShopsAccordion from './ShopsAccordion';
import {Link, browserHistory} from 'react-router';

import {idToName} from '/client/lib/collectionsRelated';

export default class Filter extends Component {
  componentDidMount() {
    const {context} = this.props,
          inputDate = $(this.cw).find('input.date-a');

    inputDate.dateDropper();
    inputDate.on('change', (e) => {
      context.setState({filterDate: e.currentTarget.value});

    });
  }

  renderPhotoMan() {
    const {context} = this.props,
          {artists} = context.props,
          {filterArtist} = context.state;

    if(!artists) return [];

    return (
      <div className="select-photoman">
        <ul className="photoman-menu-a">
          <li>
            <a>
              <i className="fa fa-camera" aria-hidden="true" style={{padding: '0 5px 0 0', color: '#ccc'}} />
              {filterArtist.length ? idToName(filterArtist, artists) : 'Все фотографы'}
              <i className="fa fa-angle-down" aria-hidden="true" style={{padding: '0 0 0 5px'}} />
            </a>
            <ul className="subphotoman-a">
              <li>
                <a
                  href="#"
                  className={filterArtist.length ? "" : "subphotoman-active"}
                  onClick={e => {
                    e.preventDefault();
                    context.setState({filterArtist: ''});

                  }}
                >
                  <i className="fa fa-camera" aria-hidden="true" style={{padding: '0 5px 0 0', color: '#ccc'}} />
                  Все фотографы
                </a>
              </li>
              {this.renderArtists()}
            </ul>
          </li>
        </ul>
      </div>
    );
  }

  renderTags() {
    const {context} = this.props,
          {tags} = context.props,
          {filterTag} = context.state;

    return tags.map((tag, index) => {
      return (
        <li key={index}>
          <a
            href="#"
            className={filterTag.includes(tag._id) ? 'submenu-active' : ''}
            onClick={e => {
              e.preventDefault();
              context.setState({filterTag: context.state.filterTag === tag._id ? '' : tag._id});

            }}
          >
            {tag.name}
          </a>
        </li>
      );
    });
  }

  renderArtists() {
    const {context} = this.props,
          {artists} = context.props,
          {filterArtist} = context.state;

    return artists.map((artist, index) => {
      return (
        <li key={index}>
          <a
            href="#"
            className={filterArtist === artist._id ? "subphotoman-active" : "" }
            onClick={e => {
              e.preventDefault();
              context.setState({filterArtist: filterArtist === artist._id ? '' : artist._id});

            }}
          >
            <i className="fa fa-camera" aria-hidden="true" style={{padding: '0 5px 0 0', color: '#ccc'}} />
            {artist.name} {artist.surname}
          </a>
        </li>
      );
    });
  }

  render() {
    const {context} = this.props,
          {tags, artists} = context.props,
          {filterTag, filterArtist} = context.state,
          ndTagId = tags.length ? tags.filter(tag => tag.name === 'ночной дозор')[0]._id : '',
          shTagId = tags.length ? tags.filter(tag => tag.name === 'светская хроника')[0]._id : '',
          {path} = context.props.route,
          currentPageNames = {
            "albums": "Фото",
            "announcements": "Анонсы",
            "shops": "Заведения",
            "articles": "Статьи",
            "videos": "Видео",
          },
          currentPageTagNames = {
            "albums": "Все фотоотчеты",
            "announcements": "Все анонсы",
            "shops": "Все заведения",
            "articles": "Все статьи",
            "videos": "Все видео",
          };

    return (
      <div ref={cw => this.cw = cw}>
        <section className="filter-a">
          <a
            className="button-a"
            href="#"
            onClick={e => {
              e.preventDefault();
              browserHistory.pull();
            }}
          > <i className="fa fa-arrow-left" aria-hidden="true" /> </a>
          <ul className="mini-menu">
            <li><a href="#">{currentPageNames[path]} <i className="fa fa-angle-down" aria-hidden="true" /> </a>
              <ul className="submenu">
                {Object.keys(currentPageNames).map(key => {
                  return (
                    <li key={key}>
                      <Link
                        to={`/${key}`}
                      >
                        {currentPageNames[key]}
                      </Link>
                    </li>
                  );
                })}
              </ul>
            </li>
          </ul>
          <div className="date-calendar">
            {/* tag element */}
            <input
              className="date-a"
              type="text"
              data-lang="ru"
              data-format="d-m-Y"
              data-fx-mobile="true"
              data-large-mode="true"
            />
            {/* init dateDropper */}
          </div>
          <div className="search-a">
            <ul className="nav">
              <li className="nav-search">
                <form action="#">
                  <button
                    type="submit"
                    title="сбросить все"
                    onClick={e => {
                      e.preventDefault();
                      context.setState({
                        filterDate: '',
                        filterWord: '',
                        filterTag: '',
                        filterArtist: '',
                        albumsNumber: 8
                      });

                    }}
                  >
                    <i className="fa fa-refresh"></i>
                  </button>
                  <input
                    type="text"
                    placeholder="Поиск по заголовку..."
                    value={context.filterWord}
                    onChange={e => {
                      context.setState({filterWord: e.currentTarget.value});
                    }}
                  />
                </form>
              </li>
            </ul>
          </div>
          <div className="select-menu">
            <ul className="mini-menu-a">
              <li><a>{filterTag.length ? idToName(filterTag, tags) : currentPageTagNames[path]}<i className="fa fa-angle-down" aria-hidden="true" style={{padding: '0 0 0 5px'}} /> </a>
                <ul className="submenu-a">
                  <li>
                    <a
                      href="#"
                      className={!filterTag.length ? 'submenu-active' : ''}
                      onClick={e => {
                        e.preventDefault();
                        context.setState({filterTag: ''});

                      }}
                    >
                      {currentPageTagNames[path]}
                    </a>
                  </li>
                  {this.renderTags()}
                </ul>
              </li>
            </ul>
          </div>
          <div className="checkbox-a">
            <input
              id="check1"
              value={filterTag === ndTagId ? 'on' : 'off'}
              type="checkbox"
              onChange={e => {
                context.setState({filterTag: context.state.filterTag === ndTagId ? '' : ndTagId});
              }}
            />
            <label htmlFor="check1">Ночной дозор</label>
            <input
              id="check2"
              type="checkbox"
              value={filterTag === shTagId ? 'on' : 'off'}
              onChange={e => {
                context.setState({filterTag: context.state.filterTag === shTagId ? '' : shTagId});
              }}
            />
            <label htmlFor="check2">Светская хроника</label>
          </div>
          {this.renderPhotoMan()}
        </section>
        <section className="filter-b">
          <div className="poisk-a">
            <div className="search-a">
              <ul className="nav">
                <li className="nav-search">
                  <form
                    action="#"
                  >
                    <input
                      value={context.filterWord}
                      onChange={e => {
                        context.setState({filterWord: e.currentTarget.value});
                      }}
                      placeholder="Поиск по заголовку"
                      type="text"
                    />
                    <div className="date-calendar date-calendar-b">
                      {/* tag element */}
                      <input
                        className="date-a"
                        type="text"
                        data-lang="ru"
                        data-format="d-m-Y"
                        data-fx-mobile="true"
                        data-large-mode="true"
                      />
                      {/* init dateDropper */}
                    </div>

                    <input
                      value='Сброс'
                      type="submit"
                      onClick={e => {
                        e.preventDefault();
                        context.setState({
                          filterDate: '',
                          filterWord: '',
                          filterTag: '',
                          filterArtist: '',
                          albumsNumber: 8
                        });
                      }}
                    />
                  </form>
                </li>
              </ul>
            </div>
          </div>
          <ShopsAccordion context={context}/>
        </section>
      </div>
    );
  }
}
