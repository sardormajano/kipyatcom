import React, {Component} from 'react';

import {idToName} from '/client/lib/collectionsRelated';

export default class ShopsAccordion extends Component {
  componentDidMount() {
    var Accordion = function(el, multiple) {
      this.el = el || {};
      this.multiple = multiple || false;

      // Variables privadas
      var links = this.el.find('.link');
      // Evento
      links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }

    Accordion.prototype.dropdown = function(e) {
      var $el = e.data.el;
        $this = $(this),
        $next = $this.next();

      $next.slideToggle();
      $this.parent().toggleClass('open');

      if (!e.data.multiple) {
        $el.find('.submenu-b').not($next).slideUp().parent().removeClass('open');
      };
    }

    var accordion = new Accordion($(this.accordion), false);
  }

  renderTags() {
    const {context} = this.props,
          {tags} = context.props,
          {filterTag} = context.state;

    return tags.map((tag, index) => {
      return (
        <li key={index}>
          <a
            href="#"
            className={filterTag.includes(tag._id) ? 'submenu-active' : ''}
            onClick={e => {
              e.preventDefault();
              const parent = e.currentTarget.parentNode.parentNode,
                    grandParent = parent.parentNode;

              parent.style.display = 'none';
              grandParent.classList.remove('open');
              context.setState({filterTag: context.state.filterTag === tag._id ? '' : tag._id});
            }}
          >
            {tag.name}
          </a>
        </li>
      );
    });
  }

  renderArtists() {
    const {context} = this.props,
          {artists} = context.props,
          {filterArtist} = context.state;

    return artists.map((artist, index) => {
      return (
        <li key={index}>
          <a
            href="#"
            className={filterArtist === artist._id ? "subphotoman-active" : "" }
            onClick={e => {
              e.preventDefault();
              const parent = e.currentTarget.parentNode.parentNode,
                    grandParent = parent.parentNode;

              parent.style.display = 'none';
              grandParent.classList.remove('open');
              context.setState({filterArtist: context.state.filterArtist === artist._id ? '' : artist._id});
            }}
          >
            <i className="fa fa-camera" aria-hidden="true" style={{padding: '0 5px 0 0', color: '#ccc'}} />
            {artist.name} {artist.surname}
          </a>
        </li>
      );
    });
  }

  renderArtistsWrapper() {
    const {context} = this.props,
          {artists} = context.props,
          {filterArtist} = context.state;

    if(!artists) return [];

    return (
      <li>
        <div className="link"><i className="fa fa-camera" aria-hidden="true" />{filterArtist.length ? idToName(filterArtist, artists) : 'Все фотографы'}<i className="fa fa-chevron-down" /></div>
        <ul className="submenu-b">
          <li>
            <a
              href="#"
              className={filterArtist.length ? "" : "submenu-b-active"}
              onClick={e => {
                e.preventDefault();
                const parent = e.currentTarget.parentNode.parentNode,
                      grandParent = parent.parentNode;

                parent.style.display = 'none';
                grandParent.classList.remove('open');
                context.setState({filterArtist: ''});
              }}
            >
              <i className="fa fa-camera" aria-hidden="true" style={{padding: '0 5px 0 0', color: '#ccc'}} />
              Все фотографы
            </a>
          </li>
          {this.renderArtists()}
        </ul>
      </li>
    );
  }

  render() {
    const {context} = this.props,
          {tags, artists} = context.props,
          {filterTag, filterArtist} = context.state,
          {path} = context.props.route,
          currentPageTagNames = {
            "albums": "Все фотоотчеты",
            "announcements": "Все анонсы",
            "shops": "Все заведения",
            "articles": "Все статьи",
            "videos": "Все видео",
          };

    return (
      <ul
        ref={ac => this.accordion = ac}
        id="accordion"
        className="accordion"
      >
        <li>
          <div className="link"><i className="fa fa-glass" aria-hidden="true" />{filterTag.length ? idToName(filterTag, tags) : currentPageTagNames[path]}<i className="fa fa-chevron-down" /></div>
          <ul className="submenu-b">
            <li>
              <a
                href="#"
                className={!filterTag.length ? 'submenu-b-active' : ''}
                onClick={e => {
                  e.preventDefault();
                  const parent = e.currentTarget.parentNode.parentNode,
                        grandParent = parent.parentNode;

                  parent.style.display = 'none';
                  grandParent.classList.remove('open');
                  context.setState({filterTag: ''});
                }}
              >
                {currentPageTagNames[path]}
              </a>
            </li>
            {this.renderTags()}
          </ul>
        </li>
        {this.renderArtistsWrapper()}
      </ul>
    );
  }
}
