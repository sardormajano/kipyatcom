import React, {Component} from 'react';

export default class Modal extends Component {
  componentDidMount() {
    $.getScript('/js/modal-window.js', () => {
      $('body').on('click', this.props.triggerSelector, e => {
        e.preventDefault();
        
        const {context} = this.props,
              stateName = e.currentTarget.getAttribute('data-state-name'),
              stateValue = e.currentTarget.getAttribute('data-state-value');

        context.setState({[stateName]: stateValue});

        $(this.modal).modal().open();
      });

      // attach modal close handler
      $(this.modal).find('.close').on('click', e => {
        e.preventDefault();
        $.modal().close();
      });

      // below isn't important (demo-specific things)
      $(this.modal).find('.more-toggle').on('click', e => {
        e.stopPropagation();
        $(this.modal).find('.more').toggle();
      });
    });
  }

  render() {
    return (
      <div
        ref={modal => this.modal = modal}
        className={`modal ${this.props.className || ''}` }
        style={{display: this.props.context.state.showModal ? '' : 'none'}}
      >
        {this.props.children}
      </div>
    );
  }
}
