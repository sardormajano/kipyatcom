import React, {Component} from 'react';
import moment from 'moment';

import Modal from '../fuc/Modal';
import {Link} from 'react-router';

export default class HomeAnnouncements extends Component {
  render() {
    if(this.props.context.props.announcements.length < 2) {
      return <div></div>;
    }

    const {context} = this.props,
          {announcements} = context.props,
          {currentAnnouncement} = context.state,
          date1 = moment.unix(announcements[0].date).format('ll').split(' '),
          dayMonth1 = `${date1[0]} ${date1[1]}`,
          year1 = `${date1[2]}`,
          date2 = moment.unix(announcements[1].date).format('ll').split(' '),
          dayMonth2 = `${date2[0]} ${date2[1]}`,
          year2 = `${date2[2]}`,
          theAnnouncement = announcements.filter(a => a._id === currentAnnouncement)[0],
          date = theAnnouncement ? moment.unix(theAnnouncement.date).format('LLL').split(' ') : [];

    return (
      <div>
        <div
          className="col-xs-0  col-sm-0 col-md-0 col-lg-0 col-xl-0"
          style={{paddingTop: 10, paddingBottom: 10}}
        >
          <div className="heading-fancy heading-line">
            <h4>
              <Link to="/announcements">
                <i className="fa fa-bookmark" aria-hidden="true" style={{padding: '0 5px 0 0'}} />
                Анонсы
              </Link>
            </h4>
          </div>
          <div className="container">
            <div className="card cart-block">
              <a
                data-state-name='currentAnnouncement'
                data-state-value={announcements[0]._id}
                href="#"
                className="post-hov announcement-trigger"
              >
                <div className="card-img">
                  <div className="post-date-big">
                    <span>{date1[0]}</span>
                    <p>{date1[1]}</p>
                  </div>
                  <div className="post-thumbnail-content">
                    <h3>{announcements[0].title}</h3>
                    <p>{announcements[0].subtitle}</p>
                  </div>
                  <div
                    style={{height: '100%', width: '100%', backgroundSize: '100% 100%', backgroundColor: '#ccc', backgroundImage: `url(${announcements[0].cover})`}}
                    className="image-wrapper"
                  >
                    <img alt src='/img/anonsy/0.jpg' />
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
        <div className="col-xs-1  col-sm-1 col-md-1 col-lg-1 col-xl-1" style={{paddingTop: 10, paddingBottom: 10}}>
          <div className="heading-fancy heading-line">
            <h4>
              <Link to="/announcements">
                <i className="fa fa-bookmark-o" aria-hidden="true" style={{padding: '0 5px 0 0'}} />
                Афиши
              </Link>
            </h4>
          </div>
          <div className="container">
            <div className="card cart-block">
              <a
                data-state-name='currentAnnouncement'
                data-state-value={announcements[1]._id}
                href="#"
                className="post-hov announcement-trigger"
              >
                <div className="card-img">
                  <div className="post-date-big">
                    <span>{date2[0]}</span>
                    <p>{date2[1]}</p>
                  </div>
                  <div className="post-thumbnail-content">
                    <h3>{announcements[1].title}</h3>
                    <p>{announcements[1].subtitle}</p>
                  </div>
                  <div
                    style={{height: '100%', width: '100%', backgroundSize: '100% 100%', backgroundColor: '#ccc', backgroundImage: `url(${announcements[1].cover})`}}
                    className="image-wrapper"
                  >
                    <img alt src='/img/anonsy/0.jpg' />
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
        <Modal
          context={context}
          triggerSelector=".announcement-trigger"
        >
          <a href="#" className="close"> </a>
          <header id="header_anons_video">
            <h1><a href="#"><img src="img/logo_kipyatcom.svg" className="logo_big" /></a></h1>
            <h6><i className="fa fa fa-bookmark-o" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Анонсы </h6>
          </header>
          <div className="modal_info">
            <a className="pic_a"> <img src={theAnnouncement ? theAnnouncement.mainPhoto : ''} /></a>
            <div className="anons_head">
              <div className="date_anons">
                <a className>{`${date[0]} ${date[1]} ${date[2]}`}</a>
                <a className>{date[4]}</a>
                <a
                  className="city_anons"
                  style={{color: '#929292', fontWeight: 'normal', background: '#efefef'}}>
                    Астана
                </a>
              </div>
              <h5 className>{theAnnouncement ? theAnnouncement.title : ''}</h5>
              <p>{theAnnouncement ? theAnnouncement.subtitle : ''}</p>
            </div>
            <p dangerouslySetInnerHTML={{__html: theAnnouncement ? theAnnouncement.content : ''}}></p>
            <Link to="/announcements">
              <button
                className="btn-b"
                style={{
                  border: 'none',
                  padding: '0px 10px',
                  fontSize: '0.7rem',
                  backgroundColor: '#d9534f',
                  margin: '20px 0 0 0'
                }}
              >
                <i style={{color: 'white'}} className="fa fa-bookmark-o" aria-hidden="true" />
                <span style={{color: 'white'}}>Все Анонсы</span>
              </button>
            </Link>
            <div className="sharing">
              <div className="ya-share2" data-services="vkontakte,facebook,gplus,twitter,whatsapp,telegram" data-counter />
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}
