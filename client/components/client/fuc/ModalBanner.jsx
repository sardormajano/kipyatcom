import React, {Component} from 'react';

import {PATHNAMES} from '/client/lib/globalVars';

export default class ModalBanner extends Component {
  constructor(props) {
    super(props);
  }

  componentDidUpdate() {
    if(!this.ready)
      return ;

    const overlay = document.querySelector('#overlay');
    setTimeout(() => {
      if(overlay && !this.timeoutSet) {
        overlay.style.display = 'block';
        this.timeoutSet = true;
      }
    }, parseInt(this.delayTime));
  }

  render() {
    const {context} = this.props,
          {banners} = context.props,
          path = context.props.location.pathname.split('/')[1],
          modalBanner = banners.filter(item => item._id === `${path ? PATHNAMES[path] : '0'}-6`)[0];

    if(!modalBanner) {
      return <div></div>;
    }

    this.ready = true;
    this.delayTime = modalBanner.delayTime;

    return (
      <div id='overlay'>
        <div className="popup">
          <div dangerouslySetInnerHTML={{__html: modalBanner.content}}/>
          <button
            className="close"
            title="Закрыть"
            onClick={e => {
              document.querySelector('#overlay').style.display='none';
            }}
          />
        </div>
      </div>
    );
  }
}
