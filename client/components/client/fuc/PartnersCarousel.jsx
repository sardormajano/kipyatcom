import React, {Component} from 'react';
import { Link } from 'react-router';

export default class PartnersCarousel extends Component {
  componentDidMount() {
    Meteor.subscribe('shops.home', {
      onReady: () => {
        $.getScript('/js-slider-1/jquery.elastislide.js', () => {
          $(this.carousel).elastislide({
              imageW: 146,
              minItems: 2,
              margin: 2,
              border: 0,
              current: 0
          });
        });
      }
    });
  }

  render() {
    const {context} = this.props,
          {shops} = context.props,
          filteredShops = shops.filter(shop => shop.isMain).sort((a, b) => {
            if(a.position && b.position) {
              return a.position > b.position ? 1 : -1;
            }
            else if(a.position) {
              return 1;
            }
            else if(b.position) {
              return 1;
            }
            else {
              return a.name > b.name ? 1 : -1;
            }
          });

    return (
      <div ref={carousel => this.carousel = carousel} id="carousel" className="es-carousel-wrapper">
        <div className="es-carousel">
          <ul>
            {filteredShops.map((shop, index) => {
              return (
                <li
                  key={index}
                >
                  <Link to={`/single-shop/${shop._id}`}>
                    <img
                      src={shop.cover2}
                      alt="image01"
                    />
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
}
