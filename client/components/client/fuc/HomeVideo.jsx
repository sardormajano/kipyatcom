import React, {Component} from 'react';

import moment from 'moment';

import {Link} from 'react-router';

import Modal from '../fuc/Modal';

export default class HomeVideo extends Component {
  render() {
    const {context} = this.props,
          {videos} = context.props,
          theVideo = videos[0],
          date = theVideo ? moment.unix(theVideo.date).format('ll').split(' ') : [];

    return (
      <div className="col-xs-00  col-sm-00 col-md-00 col-lg-00 col-xl-01" style={{paddingTop: 10, paddingBottom: 10}}>
        <div className="heading-fancy heading-line">
          <h4>
            <Link to="/videos">
              <i className="fa fa-video-camera" aria-hidden="true" style={{padding: '0 5px 0 0'}} />
              Видео
            </Link>
          </h4>
        </div>
        <div className="container">
          <div className="card cart-block">
            <a
              href="#"
              className="post-hov video-trigger"
            >
              <div className="card-img">
                <div className="post-date-big">
                  <span>{date[0]}</span>
                  <p>{date[1]}</p>
                </div>
                <div className="post-play-icon">
                  <i className="fa fa-play" aria-hidden="true" />
                </div>
                <div className="post-thumbnail-content">
                  <h3>{theVideo ? theVideo.title.toUpperCase() : ''}</h3>
                  <p>{theVideo ? theVideo.subtitle.toUpperCase() : ''}</p>
                </div>
                <div
                  style={{height: '100%', width: '100%', backgroundSize: '100% 100%', backgroundColor: '#ccc', backgroundImage: `url(${theVideo ? theVideo.cover : ''})`}}
                  className="image-wrapper"
                >
                  <img alt src='/img/video/0.gif' />
                </div>
              </div>
            </a>
          </div>
        </div>
        <Modal
          context={context}
          triggerSelector=".video-trigger"
        >
          <a href="#" className="close"> </a>
          <header id="header_anons_video">
            <h1><a href="#"><img src="img/logo_kipyatcom.svg" className="logo_big" /></a></h1>
            <h6><i className="fa fa-video-camera" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Видео </h6>
          </header>
          <div className="modal_info">
            <div className="post_head">
              <div className="tag_1"> <a href="#" className>LifeStyle </a> <a href="#" className>День и ночь</a></div>
              <h5 className>{theVideo ? theVideo.title.toUpperCase() : ''}</h5>
              <p>{theVideo ? theVideo.subtitle.toUpperCase() : ''}</p>
              <div className="post_author">
                <div className="post-author-name">
                  <div className="date01">
                    <time className dateTime="18-05-2017">{date[0]} {date[1]}, {date[2]}</time>
                  </div>
                </div>
              </div>
            </div>
            <div className="video">

              <p>
                <span dangerouslySetInnerHTML={{__html: theVideo ? theVideo.videoCode : ''}}></span>
                <a href="https://vimeo.com/215980100">
                  {theVideo ? theVideo.title : ''} / {theVideo ? theVideo.subtitle : ''} / {date[0]} {date[1]}, {date[2]}
                </a>
                from
                <a href="https://vimeo.com/kipyatcom">kipyat.com</a>
                on
                <a href="https://vimeo.com">Vimeo</a>.
              </p>
            </div>
            <Link to="/videos">
              <button
                id="all-video-white-text"
                className="btn-b"
                style={{
                  border: 'none',
                  padding: '0px 10px',
                  fontSize: '0.7rem',
                  backgroundColor: '#d9534f',
                  margin: '20px 0 0 0'
                }}>
                <i style={{color: 'white'}} className="fa fa-bookmark-o" aria-hidden="true" />
                <span style={{color: 'white'}}>Все Видео</span>
              </button>
            </Link>
            <div className="sharing">
              <div className="ya-share2" data-services="vkontakte,facebook,gplus,twitter,whatsapp,telegram" data-counter />
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}
