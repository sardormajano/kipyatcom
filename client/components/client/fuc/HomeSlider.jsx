import React, {Component} from 'react';

export default class Slider extends Component {
  componentDidMount() {
    Meteor.subscribe('slides.home', {
      onReady: () => {
        this.subscriptionsReady = true;
        this.forceUpdate();
      }
    })
  }

  componentDidUpdate() {
    if(!this.subscriptionsReady) return ;

    $.getScript('/js-slider-1/jquery.glide.js', () => {
      var glide = $(this.slider).glide().data('api_glide');

      $(this.slider).glide({
        autoplay: 5000,
      });

      $(this.slider).find('.fa-arrow-right, .fa-arrow-left').remove();
    });
  }

  renderSlides() {
    const {context} = this.props,
          {slides} = context.props;

    if(!this.subscriptionsReady) {
      return [(
          <li className="slide-sl" key={0}>
            <a href='#'>
              <img src={slides[0] ? slides[0].cover : '/img/slider-preloader.gif'} width="100%" height="100%" />
            </a>
          </li>
        )];
    }

    return slides.map((slide, index) => {
      return (
        <li className="slide-sl" key={index}>
          <a href={slide.link}>
            <img src={slide.cover} width="100%" height="100%" />
          </a>
        </li>
      );
    })
  }

  render() {
    const {context} = this.props,
          {slides} = context.props;

    return (
      <div>
        <div className="slider-sl" ref={slider => this.slider = slider}>
          <ul className="slides">
            {this.renderSlides()}
          </ul>
        </div>
      </div>
    );
  }
}
