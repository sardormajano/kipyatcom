import React, {Component} from 'react';

export default class Footer extends Component {
  render() {
    return (
      <div className="content-line-footer col-xs-footer col-LG-footer col-XL-footer">
        <div className="footer-nav">
          <div className="footer-menu">
            <ul className="nav-footer-0">
              <li className="nav-footer2"><a href="#"> Главная</a>
              </li>
              <li className="nav-footer2"><a href="#"> Фото</a>
              </li>
              <li className="nav-footer2"><a href="#"> Анонсы</a>
              </li>
              <li className="nav-footer2"><a href="#"> Заведения</a>
              </li>
              <li className="nav-footer2"><a href="#"> Статьи</a>
              </li>
              <li className="nav-footer2"><a href="#"> Видео</a>
              </li>
              <li className="nav-footer2"><a href="#"> Контакты</a>
              </li>
              <li className="nav-footer2"><a href="#" className="nav-footer3"> Вакансии</a>
              </li>
              <li className="nav-footer2"><a href="#" className="nav-footer3"> Соглашение</a>
              </li>
              <li className="nav-footer2" style={{border: 'none !important'}}><a href="#" className="nav-footer3"> Info</a>
              </li>
            </ul>
          </div>
          <div className="footer-1">
            <p className>Веб-портал фотоотчетов
              <br /> освещающий события
              <br /> Вашего города</p>
            <a href="#" className="logo-footer">
              <img alt src="/img/logo_kipyatcom_astana.png" />
            </a>
          </div>
          <div className="footer-2">
            <a className="mail-txt" href="mailto:astana@kipyat.com"> email: <i className="fa fa-envelope-o" aria-hidden="true" style={{padding: '0 3px 0 10px'}} /> astana@kipyat.com</a>
            <p className="call-txt"> Заказать фотоотчет </p>
            <a href="tel:+7 701 227 09 88" className="phone_txt_number	phone_txt_number_bottom" style={{color: '#fff', fontSize: '1.3rem'}}> <i className="fa fa-phone" aria-hidden="true" style={{padding: '0 10px 0 0', color: '#afafaf'}} /> +7 701 227 09 88</a>
          </div>
          <div className="footer-3">
            <a className="smm2" href="https://www.instagram.com/kipyatcom" target="_blank"><i className="fa fa-instagram" aria-hidden="true" style={{padding: '0 3px 0 0', color: '#aaa'}} /> instagram.com/kipyatcom </a>
            <a className="smm" href="https://www.vk.com/kipyatcom" target="_blank"><i className="fa fa-vk" aria-hidden="true" style={{padding: '0 3px 0 0', color: '#aaa'}} /> vk.com/kipyatcom </a>
            <a className="smm" href="https://www.vimeo.com/kipyatcom" target="_blank"><i className="fa fa-facebook" aria-hidden="true" style={{padding: '0 3px 0 0', color: '#aaa'}} /> vimeo.com/kipyatcom </a>
            <a className="smm" href="https://www.twitter.com/kipyatcom" target="_blank"><i className="fa fa-twitter" aria-hidden="true" style={{padding: '0 3px 0 0', color: '#aaa'}} /> twitter.com/kipyatcom </a>
          </div>
          <p className="kipyatcom"> © 2014-2017 | "Кипятком" Фотопортал. | Все права защищены. | www.kipyat.com </p>
        </div>
      </div>
    );
  }
}
