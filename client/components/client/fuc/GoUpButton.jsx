import React, {Component} from 'react';

export default class GoUpButton extends Component {
  componentDidMount() {
    $(this.gub).hide();

    const self = this;

    $(window).scroll(function() {
      if ($(this).scrollTop() > 200) {
        $(self.gub).show();
      } else {
        $(self.gub).fadeOut();
      }
    });

    // scroll body to 0px on click
    $(this.gul).click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
  }

  render() {
    return (
      <p
        id="back-top"
        ref={gub => this.gub = gub}
        style={{display: 'block'}}
      >
        <a
          ref={gul => this.gul = gul}
          className="slideInUp animated"
        >
          <span></span>
          <i className="fa fa-angle-up" aria-hidden="true"></i>
        </a>
      </p>
    );
  }
}
