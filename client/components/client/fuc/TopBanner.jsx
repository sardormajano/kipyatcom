import React, {Component} from 'react';
import {getRandomInt} from '/client/lib/general.js';
import {PATHNAMES} from '/client/lib/globalVars';

export default class TopBanner extends Component {
  render() {
    const {context} = this.props,
          {banners} = context.props,
          path = context.props.location.pathname.split('/')[1],
          bannerLeft = banners.filter(item => item._id === `${path ? PATHNAMES[path] : '0'}-1-1`)[0],
          bannerRight = banners.filter(item => item._id === `${path ? PATHNAMES[path] : '0'}-1-2`)[0];

    console.log(!!path);

    if(!bannerLeft || !bannerRight || bannerLeft.off || bannerRight.off) {
      return <div></div>;
    }

    const randomIntLeft = getRandomInt(0, bannerLeft.photos.length),
          randomIntRight = getRandomInt(0, bannerRight.photos.length);

    const imageLeft = bannerLeft ? bannerLeft.photos[randomIntLeft] : '',
          imageRight = bannerRight ? bannerRight.photos[randomIntRight] : '';

    if(!bannerLeft.photos.length || !bannerRight.photos.length) {
      return <div></div>;
    }

    return (
      <div
        className="top_baner"
      >
        <span className="baner_top">
          <a
            className="ban_left"
            href={bannerLeft.isTop ? bannerLeft.links[randomIntLeft] : bannerRight.links[randomIntRight]}
            target="_blank"
          >
            <img src={bannerLeft.isTop ? imageLeft : imageRight} />
          </a>
        </span>
        <span className="baner_top">
          <a
            className="ban_right"
            target="_blank"
            href={bannerLeft.isTop ? bannerRight.links[randomIntRight] : bannerLeft.links[randomIntLeft]}
          >
            <img src={bannerLeft.isTop ? imageRight : imageLeft} />
          </a>
        </span>
      </div>
    );
  }
}
