import React, {Component} from 'react';
import {getRandomInt} from '/client/lib/general.js';
import {PATHNAMES} from '/client/lib/globalVars';

export default class Banner4 extends Component {
  render() {
    const {context} = this.props,
          {banners} = context.props,
          path = context.props.location.pathname.split('/')[1],
          bannerLeft = banners.filter(item => item._id === `${path ? PATHNAMES[path] : '0'}-4-1`)[0],
          bannerRight = banners.filter(item => item._id === `${path ? PATHNAMES[path] : '0'}-4-2`)[0];

    if(!bannerLeft || !bannerRight || bannerLeft.off || bannerRight.off) {
      return <div></div>;
    }

    const randomIntLeft = getRandomInt(0, bannerLeft.photos.length),
          randomIntRight = getRandomInt(0, bannerRight.photos.length);

    const imageLeft = bannerLeft ? bannerLeft.photos[randomIntLeft] : '',
          imageRight = bannerRight ? bannerRight.photos[randomIntRight] : '';

    if(!bannerLeft.photos.length || !bannerRight.photos.length) {
      return <div></div>;
    }

    return (
      <div className="baner4 baner4-XS-1 baner4-SM-1 baner4-MD-1 baner4-LG-1 baner4-XL-1">
        <span className="baner_top">
          <a
            className="ban_left"
            href={bannerLeft.isTop ? bannerLeft.links[randomIntLeft] : bannerRight.links[randomIntRight]}
            target="_blank"
          >
            <img src={bannerLeft.isTop ? imageLeft : imageRight} />
          </a>
        </span>
        <span className="baner_top">
          <a
            className="ban_right"
            href={bannerLeft.isTop ? bannerRight.links[randomIntRight] : bannerLeft.links[randomIntLeft]}
            target="_blank"
          >
            <img src={bannerLeft.isTop ? imageRight : imageLeft} />
          </a>
        </span>
      </div>
    );
  }
}
