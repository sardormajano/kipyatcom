import React, {Component} from 'react';

import {Link} from 'react-router';

import {addScript} from '/client/lib/scriptsRelated';

import DynamicButton from '../../stateless/DynamicButton';

export default class MenuBottom extends Component {
  componentDidMount() {
    addScript({src: '/js/menu-mob/skel.min.js'});
    addScript({src: '/js/menu-mob/util.js'});
    $.getScript('/js/menu-mob/util.js', () => addScript({src: '/js/menu-mob/main.js'}));
  }

  render() {
    const {context} = this.props,
          {pathname} = context.props.location;

    return (
      <div>
        <div className="header_container clearfix">
          <div className="logo_big_1">
            <Link
              className="logo_big"
              to="/"
            >

            </Link>
          </div>
          <div className="header_right">
            <div className="stretch-element-top">
              <div className="table-cell">
                <div className="table-cell_in">
                  <span className="phone_txt">Заказать фотоотчет </span>
                  <a href="tel:+7 701 227 09 88" className="phone_txt_number"> <i className="fa fa-phone" aria-hidden="true" style={{padding: '0 10px 0 0', color: '#afafaf'}} /> +7 701 227 09 88</a>
                </div>
                <div className="sotial_icons">
                  <a className="sotial_icons_insta" target="_black" href="http://instagram.com/kipyatcom">
                    <i className="fa fa-instagram" aria-hidden="true" />
                  </a>
                </div>
                <div className="sotial_icons">
                  <a className="sotial_icons_insta" target="_black" href="https://www.facebook.com/kipyatcom">
                    <i className="fa fa-facebook" aria-hidden="true" style={{fontSize: 17}} />
                  </a>
                </div>
                <div className="sotial_icons">
                  <a className="sotial_icons_insta" target="_black" href="https://vk.com/kipyatcom">
                    <i className="fa fa-vk" aria-hidden="true" style={{fontSize: 17}} />
                  </a>
                </div>
                {/* <div className="table-cell_in_right">
                  <a className="app" href="#"> <img src="/img/app2.png" className="app_img" /> </a>
                  <a className="app" href="#"> <img src="/img/app1.png" className="app_img" /> </a>
                </div> */}
                <div className="table-cell_in_right_language">
                  <a className="language" id="current" href="#">Русский</a>
                  <a className="language" href="#">English</a>
                </div>
              </div>
            </div>
          </div>
          <div className="header_right_bottom">
            <div className="stretch-element-bottom">
              <div className="bottom-cell">
                <div className="bottom-cell_bottom">
                  <ul className="nav">
                    <li className="city">
                      <a>Астана <i className="fa fa-angle-down" aria-hidden="true" /></a>
                      <div>
                        <div className="nav-column">
                          <ul>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Астана</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Алматы</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Актау</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Актобе</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Қарағанды</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Кокшетау</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Костанай</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Петропавл</a>
                            </li>
                            <li className="city2">
                              <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Уральск</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
                <ul className="nav">
                  <li className="nav-search">
                    <form action="#">
                      <input type="text" placeholder="Поиск по сайту..." />
                      <input id="header-search-submit" type="submit" defaultValue />
                    </form>
                  </li>
                </ul>
              </div>
              <ul className="menu">
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/']}
                    to='/'
                  >
                    <i className="fa fa-home" aria-hidden="true" />
                  </DynamicButton>
                </li>
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/albums']}
                    to='/albums'
                  >
                    Фото
                  </DynamicButton>
                </li>
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/announcements']}
                    to='/announcements'
                  >
                    Анонсы
                  </DynamicButton>
                </li>
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/shops']}
                    to='/shops'
                  >
                    Заведения
                  </DynamicButton>
                </li>
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/articles']}
                    to='/articles'
                  >
                    Статьи
                  </DynamicButton>
                </li>
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/videos']}
                    to='/videos'
                  >
                    Видео
                  </DynamicButton>
                </li>
                <li className="menu1">
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/contacts']}
                    to='/contacts'
                  >
                    Контакты
                  </DynamicButton>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="header_adaptive">
          {/* Header */}
          <header id="header">
            <h1><Link to="/" className="logo_big"></Link></h1>
            <ul className="nav" style={{borderRight: '1px solid #e4e4e4'}}>
              <li className="city">
                <a>Астана <i className="fa fa-angle-down" aria-hidden="true" /></a>
                <div style={{left: 0}}>
                  <div className="nav-column">
                    <ul>
                      <li className="city2">
                        <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Астана</a>
                      </li>
                      <li className="city2">
                        <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Алматы</a>
                      </li>
                      <li className="city2">
                        <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Актау</a>
                      </li>
                      <li className="city2">
                        <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Актобе</a>
                      </li>
                      <li className="city2">
                        <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Қарағанды</a>
                      </li>
                      <li className="city2">
                        <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Кокшетау</a>
                      </li>
                      <li className="city2">
                        <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Костанай</a>
                      </li>
                      <li className="city2">
                        <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Петропавл</a>
                      </li>
                      <li className="city2">
                        <a href="#"> <i className="fa fa-location-arrow" aria-hidden="true" /> Уральск</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
            </ul>
            <nav className="links">
              <ul>
                <li>
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/']}
                    to='/'
                  >
                    <i className="fa fa-home" aria-hidden="true" />
                  </DynamicButton>
                </li>
                <li>
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/albums']}
                    to='/albums'
                  >
                    Фото
                  </DynamicButton>
                </li>
                <li>
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/announcements']}
                    to='/announcements'
                  >
                    Анонсы
                  </DynamicButton>
                </li>
                <li>
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/shops']}
                    to='/shops'
                  >
                    Заведения
                  </DynamicButton>
                </li>
                <li>
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/articles']}
                    to='/articles'
                  >
                    Статьи
                  </DynamicButton>
                </li>
                <li>
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/videos']}
                    to='/videos'
                  >
                    Видео
                  </DynamicButton>
                </li>
                <li>
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/contacts']}
                    to='/contacts'
                  >
                    Контакты
                  </DynamicButton>
                </li>
              </ul>
            </nav>
            <nav className="main">
              <ul>
                <li className="search">
                  <a className="fa-search" href="#search" style={{color: 'rgb(170, 170, 170)', fontSize: '1rem', width: '4em'}}>Поиск по сайту...</a>
                  <form id="search" method="get" action="#">
                    <input type="text" name="query" placeholder="Поиск по сайту..." />
                  </form>
                </li>
                <li className="menu">
                  <a className="fa-bars" href="#menu">Menu</a>
                </li>
              </ul>
            </nav>
          </header>
          {/* Menu */}
          <section id="menu">
            {/* Search */}
            <section>
              <form className="search" method="get" action="#">
                <input type="text" name="query" placeholder="Поиск по сайту..." />
              </form>
            </section>
            {/* Search */}
            <section style={{padding: '0px 17px'}}>
              <div className="table-cell_in" style={{height: 'auto', borderRight: 'none'}}>
                <span className="phone_txt" style={{padding: '8px 8px 0px 8px'}}>Заказать фотоотчет </span>
                <a href="tel:+7 701 227 09 88" className="phone_txt_number"> <i className="fa fa-phone" aria-hidden="true" style={{padding: '0 10px 0 0', color: '#afafaf'}} /> +7 701 227 09 88</a>
              </div>
            </section>
            {/* Links */}
            <section style={{padding: '5px 30px'}}>
              <ul className="links">
                <li>
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/albums']}
                    to='/albums'
                  >
                    Фото
                  </DynamicButton>
                </li>
                <li>
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/announcements']}
                    to='/announcements'
                  >
                    Анонсы
                  </DynamicButton>
                </li>
                <li>
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/shops']}
                    to='/shops'
                  >
                    Заведения
                  </DynamicButton>
                </li>
                <li>
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/articles']}
                    to='/articles'
                  >
                    Статьи
                  </DynamicButton>
                </li>
                <li>
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/videos']}
                    to='/videos'
                  >
                    Видео
                  </DynamicButton>
                </li>
                <li>
                  <DynamicButton
                    pathname={pathname}
                    pathnames={['/contacts']}
                    to='/contacts'
                  >
                    Контакты
                  </DynamicButton>
                </li>
                <li>
                  <a href="#">
                    <p>Вакансии</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <p>Соглашение</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <p>Info</p>
                  </a>
                </li>
              </ul>
            </section>
            {/* Actions */}
            <section>
              <ul className="actions vertical">
                <li><a href="#" className="button big fit" style={{WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)'}}>Русский</a>
                </li>
                <li><a href="#" className="button big fit" style={{WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)'}}>English</a>
                </li>
              </ul>
            </section>
          </section>
        </div>
      </div>
    );
  }
}
