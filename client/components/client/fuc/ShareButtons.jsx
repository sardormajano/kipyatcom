import React, {Component} from 'react';

import {addScript} from '/client/lib/scriptsRelated.js';

export default class ShareButtons extends Component {
  componentDidMount() {
    addScript({src: 'http://yastatic.net/es5-shims/0.0.2/es5-shims.min.js'});
    
    addScript({src: 'http://yastatic.net/share2/share.js'});
  }

  render() {
    return (
      <div className="sharing">
        <div className="ya-share2" data-services="whatsapp,vkontakte,facebook,gplus,twitter,telegram" />
      </div>
    );
  }
}
