import React, {Component} from 'react';
import {getRandomInt} from '/client/lib/general.js';

export default class BannerB extends Component {
  render() {
    const {context} = this.props,
          {banners} = context.props,
          banner = banners.filter(item => item._id === '0-b')[0];

    if(!banner || banner.off) {
      return <div></div>;
    }

    const randomInt = getRandomInt(0, banner.photos.length);

    const image = banner ? banner.photos[randomInt] : '';

    if(!banner.photos.length) {
      return <div></div>;
    }

    return (
      <div className="container">
        <div className="card cart-block">
          <div className="card-img">
            <a href={banner.links[randomInt]} target="_blank" className="post-img">
              <img alt src={image} style={{opacity: 1}} />
            </a>
          </div>
        </div>
      </div>
    );
  }
}
