import React, {Component} from 'react';

import Header from '../fuc/Header';
import Footer from '../fuc/Footer';
import MenuBottom from '../fuc/MenuBottom';
import PartnersCarousel from '../fuc/PartnersCarousel';
import GoUpButton from '../fuc/GoUpButton';
import ShopsAccordion from '../fuc/ShopsAccordion';
import Insta from '../fuc/Insta';

import TopBanner from '../fuc/TopBanner';
import Banner2 from '../fuc/Banner2';
import BannerA from '../fuc/BannerA';
import Banner3 from '../fuc/Banner3';
import BannerB from '../fuc/BannerB';
import Banner4 from '../fuc/Banner4';
import Banner5 from '../fuc/Banner5';
import Filter from '../fuc/Filter';

import {Link} from 'react-router';

import moment from 'moment';
import {idToName} from '/client/lib/collectionsRelated';

import {createPositionCompareFunction} from '/client/lib/general.js';

export default class Articles extends Component {
  renderArticles(articles) {
    return articles.map((a, index) => {
      const date = moment.unix(a.date).format('LLL').split(' ');

      return (
        <div
          key={index}
          className="post_p"
          style={{}}
        >
          <div className="container">
            <div className="card cart-block">
              <a href={`/single-article/${a._id}`} className="post-hov">
                <div className="card-img">
                  <div className="post-date-big">
                    <span>{date[0]}</span>
                    <p>{date[1]}</p>
                  </div>
                  <div className="post-thumbnail-content">
                    <h3>{a.title.toUpperCase()}</h3>
                    <p>{a.subtitle.toUpperCase()}</p>
                  </div>
                  <div
                    className="image-wrapper"
                    style={{backgroundSize: '100% 100%', backgroundImage: `url(${a.cover})`}}
                  >
                    <img alt src='/img/article/0.jpg' />
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    const {context} = this.props,
          {articles} = context.props,
          {filterWord, filterTag, filterDate} = context.state,
          compareFunction = createPositionCompareFunction('topPosition', 'date'),
          topArticles = articles
                          .filter(a => a.isTop)
                          .sort(compareFunction),
          filteredArticles = articles.filter((article => {
            if(article.isTop)
              return false;

            //filtering by word
            if(!(new RegExp(filterWord, 'i')).test(article.title)
              && !(new RegExp(filterWord, 'i')).test(article.subtitle))
              return false;

            //filtering by tags
            if(filterTag.length) {
              if(!article.tags.includes(filterTag))
                return false;
            }

            //filtering by date
            if(filterDate.length) {
              const fDateArray = filterDate.split('-');

              const fDate = moment([fDateArray[2], parseInt(fDateArray[1]) - 1, fDateArray[0]]),
                    aDate = moment.unix(article.date);

              if(fDate.get('year') !== aDate.get('year')
                  || fDate.get('month') !== aDate.get('month')
                   || fDate.get('date') !== aDate.get('date'))
                return false;
            }

            return true;
          })).slice(0, context.state.articlesNumber);

    console.log(articles);

    return (
      <div className="ALLLL">
        <TopBanner context={context}/>
        <Header context={context}/>
        <Banner2 context={context}/>
        <Filter context={context}/>
        <section className="mbr-section mbr-after-navbar" id="pricing-table1-6">
          <div className="mbr-section mbr-section-nopadding mbr-price-table">
            <div className="content_wrapper">
              <div
                style={{display: !filteredArticles.length && context.state.articlesReady ? '' : 'none'}}
                className="heading-fancy2 heading-line2"
              >
        				<h4>
                  <i className="fa fa-frown-o" aria-hidden="true" style={{padding: '0 5px 0 0'}}></i>
                  Ничего не найдено
                </h4>
        			</div>
              <div
                style={{display: (filterWord.length || filterTag.length || filterDate.length) && filteredArticles.length ? 'none' : ''}}
                className="heading-fancy2 heading-line2"
              >
                <h4><i className="fa fa-thumb-tack" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Популярные Статьи</h4>
              </div>
              {(filterWord.length || filterTag.length || filterDate.length) && filteredArticles.length ? '' : this.renderArticles(topArticles)}
              <div className="heading-fancy2 heading-line2">
                <h4>
                  <i className="fa fa-newspaper-o" aria-hidden="true" style={{padding: '0 5px 0 0'}} />
                  {filterWord.length || filterTag.length || filterDate.length ? `Найдено ${context.state.foundNumber}` : 'Все статьи' }
                </h4>
              </div>
              {this.renderArticles(filteredArticles)}
            </div>
          </div>
          <div className="button_place">
            <p>
              <button
                className="btn-a"
                onClick={context.loadMoreHandler.bind(context)}
                dangerouslySetInnerHTML={{__html: context.state.loadMoreLabel}}
              />
            </p>
          </div>
        </section>
        <Banner4 context={context}/>
        <Insta />
        <PartnersCarousel context={context}/>
        <Footer />
        <MenuBottom context={context}/>
        <GoUpButton />
      </div>
    );
  }
}
