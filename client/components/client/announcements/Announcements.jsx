import React, {Component} from 'react';

import Header from '../fuc/Header';
import Footer from '../fuc/Footer';
import MenuBottom from '../fuc/MenuBottom';
import PartnersCarousel from '../fuc/PartnersCarousel';
import GoUpButton from '../fuc/GoUpButton';
import ShopsAccordion from '../fuc/ShopsAccordion';
import Insta from '../fuc/Insta';
import Modal from '../fuc/Modal';
import ShareButtons from '../fuc/ShareButtons';

import TopBanner from '../fuc/TopBanner';
import Banner2 from '../fuc/Banner2';
import BannerA from '../fuc/BannerA';
import Banner3 from '../fuc/Banner3';
import BannerB from '../fuc/BannerB';
import Banner4 from '../fuc/Banner4';
import Banner5 from '../fuc/Banner5';

import moment from 'moment';

import {createPositionCompareFunction} from '/client/lib/general';

export default class Announcements extends Component {
  renderAnnouncements(announcements) {
    const {context} = this.props;

    return announcements.map((a, index) => {
      const date = moment.unix(a.date).format('LLL').split(' ');

      return (
        <div key={index} className="post_p" style={{}}>
          <div className="container">
            <div className="card cart-block">
              <a
                className="btn-b"
                target="_blank"
                style={{
                  position: 'absolute',
                  zIndex: 500,
                  right: 0,
                  border: 'none',
                  padding: '0px 10px',
                  fontSize: '0.7rem',
                  display: a.link ? '' : 'none'
                }}
                href={a.link || '#'}
              >
                <i className="fa fa-ticket" aria-hidden="true" />
                {a.linkName || 'купить билет'}
              </a>
              <a
                href='#'
                data-state-name='currentAnnouncement'
                data-state-value={a._id}
                className="post-hov trigger"
              >
                <div className="card-img">
                  <div className="post-date-big">
                    <span>{date[0]}</span> <p>{date[1]}</p>
                  </div>
                  <div className="post-thumbnail-content">
                    <h3>{a.title.toUpperCase()}</h3>
                    <p>{a.subtitle.toUpperCase()}</p>
                  </div>
                  <div
                    style={{height: '100%', width: '100%', backgroundColor: '#ccc', backgroundImage: `url(${a.cover})`}}
                    className="image-wrapper"
                  >
                    <img alt src='/img/anonsy/0.jpg' />
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    const {context} = this.props,
          {announcements} = context.props,
          compareFunction = createPositionCompareFunction('topPosition', 'title'),
          topAnnouncements = announcements.filter(a => a.isTop).sort(compareFunction),
          theAnnouncement = announcements.filter(a => a._id === context.state.currentAnnouncement)[0],
          date = theAnnouncement ? moment.unix(theAnnouncement.date).format('LLL').split(' ') : [];


    return (
      <div className="ALLLL">
        <TopBanner context={context}/>
        <Header context={context}/>
        <Banner2 context={context}/>
        <section className="filter-a">
          <a className="button-a" href="#"> <i className="fa fa-arrow-left" aria-hidden="true" /> </a>
          <ul className="mini-menu">
            <li><a href="#">Анонсы<i className="fa fa-angle-down" aria-hidden="true" /> </a>
              <ul className="submenu">
                <li><a href="#">Фотоотчеты</a></li>
                <li><a href="#">Статьи</a></li>
                <li><a href="#">Заведения</a></li>
                <li><a href="#">Видео</a></li>
                <li><a href="#">Контакты</a></li>
              </ul>
            </li>
          </ul>
          <div className="date-calendar">
            {/* tag element */}
            <input className="date-a" type="text" data-lang="ru" data-format="d-m-Y" data-fx-mobile="true" data-large-mode="true" />
            {/* init dateDropper */}
          </div>
          <div className="search-a">
            <ul className="nav">
              <li className="nav-search">
                <form action="#">
                  <input type="text" placeholder="Поиск по заголовку..." />
                  <input type="submit" defaultValue="Найти" />
                </form>
              </li>
            </ul>
          </div>
          <div className="select-menu">
            <ul className="mini-menu-a">
              <li><a>Все анонсы<i className="fa fa-angle-down" aria-hidden="true" style={{padding: '0 0 0 5px'}} /> </a>
                <ul className="submenu-a">
                  <li><a href="#" className="submenu-active">Все анонсы</a></li>
                  <li><a href="#">Новости</a></li>
                  <li><a href="#">Fashion</a></li>
                  <li><a href="#">Звезды</a></li>
                  <li><a href="#">Красота и здоровье</a></li>
                  <li><a href="#">Стиль жизни</a></li>
                  <li><a href="#">Культура</a></li>
                  <li><a href="#">Спорт</a></li>
                  <li><a href="#">Шоу Бизнес</a></li>
                  <li><a href="#">Технологии</a></li>
                  <li><a href="#">Дом, дети</a></li>
                  <li><a href="#">Авто</a></li>
                  <li><a href="#">Город</a></li>
                </ul>
              </li>
            </ul>
          </div>
          <div className="checkbox-a">
            <input id="check1" type="checkbox" name="check" defaultValue="check1" />
            <label htmlFor="check1">Ночной дозор</label>
            <input id="check2" type="checkbox" name="check" defaultValue="check2" />
            <label htmlFor="check2">Светская хроника</label>
          </div>
        </section>
        <section className="filter-b">
          <div className="poisk-a">
            <div className="search-a">
              <ul className="nav">
                <li className="nav-search">
                  <form action="#">
                    <i className="fa fa-search" aria-hidden="true" />
                    <input type="text" placeholder="Поиск по заголовку..." />
                    <div className="date-calendar date-calendar-b">
                      {/* tag element */}
                      <input className="date-a" type="text" data-lang="ru" data-format="d-m-Y" data-fx-mobile="true" data-large-mode="true" />
                      {/* init dateDropper */}
                    </div>
                    <input type="submit" defaultValue="Найти" />
                  </form>
                </li>
              </ul>
            </div>
          </div>
          <ul id="accordion" className="accordion">
            <li>
              <div className="link"><i className="fa fa-glass" aria-hidden="true" />Все анонсы<i className="fa fa-chevron-down" /></div>
              <ul className="submenu-b">
                <li><a href="#" className="submenu-active">Все анонсы</a></li>
                <li><a href="#">Ночной дозор</a></li>
                <li><a href="#">Светская хроника</a></li>
                <li><a href="#">Новости</a></li>
                <li><a href="#">Fashion</a></li>
                <li><a href="#">Звезды</a></li>
                <li><a href="#">Красота и здоровье</a></li>
                <li><a href="#">Стиль жизни</a></li>
                <li><a href="#">Культура</a></li>
                <li><a href="#">Спорт</a></li>
                <li><a href="#">Шоу Бизнес</a></li>
                <li><a href="#">Технологии</a></li>
                <li><a href="#">Дом, дети</a></li>
                <li><a href="#">Авто</a></li>
                <li><a href="#">Город</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <section className="mbr-section mbr-after-navbar" id="pricing-table1-6">
          <div className="content_wrapper_dark">
            <div className="head-dark-1">
              <h4><i className="fa fa-camera" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Анонсы</h4>
            </div>
            <div className="head-dark-2">
              <h4><i className="fa fa-thumb-tack" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Популярные Анонсы </h4>
            </div>
            {this.renderAnnouncements(topAnnouncements)}
            <div className="head-dark">
              <h4><i className="fa fa-newspaper-o" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Последние анонсы</h4>
            </div>
            {this.renderAnnouncements(announcements)}
            <Modal
              context={context}
              triggerSelector=".trigger"
            >
              <a href="#" className="close"> </a>
              <header id="header_anons_video">
                <h1><a href="#"><img src="img/logo_kipyatcom.svg" className="logo_big" /></a></h1>
                <h6><i className="fa fa fa-bookmark-o" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Анонсы </h6>
              </header>
              <div className="modal_info">
                <a className="pic_a"> <img src={theAnnouncement ? theAnnouncement.mainPhoto : ''} /></a>
                <div className="anons_head">
                  <div className="date_anons">
                    <a className>{`${date[0]} ${date[1]} ${date[2]}`}</a>
                    <a className>{date[4]}</a>
                    <a
                      className="city_anons"
                      style={{color: '#929292', fontWeight: 'normal', background: '#efefef'}}>
                        Астана
                    </a>
                  </div>
                  <h5 className>{theAnnouncement ? theAnnouncement.title : ''}</h5>
                  <p>{theAnnouncement ? theAnnouncement.subtitle : ''}</p>
                </div>
                <div
                  className="buy_bt"
                  style={{display: theAnnouncement && theAnnouncement.link ? '' : 'none'}}
                >
    							<a
                    className="btn-b"
                    href={theAnnouncement ? theAnnouncement.link || '#' : '#'}
                    target="_blank"
                  >
                    <i className="fa fa-ticket" aria-hidden="true"></i>
                    {theAnnouncement ? theAnnouncement.linkName || 'Купить билет' : 'Купить билет'}
                  </a>
    						</div>
                <p dangerouslySetInnerHTML={{__html: theAnnouncement ? theAnnouncement.content : ''}}></p>
                <button className="btn-b" style={{border: 'none', padding: '0px 10px', fontSize: '0.7rem', backgroundColor: '#d9534f', margin: '20px 0 0 0'}}><i className="fa fa-bookmark-o" aria-hidden="true" /> Все Анонсы	</button>
                <ShareButtons />
              </div>
            </Modal>
          </div>
          <div className="button_place">
            <p>
              <button className="btn-a">
                <i className="fa fa-angle-down" aria-hidden="true" />
                Еще
                <i className="fa fa-angle-down" aria-hidden="true" />
              </button>
            </p>
          </div>
        </section>
        <Banner4 context={context}/>
        <Insta />
        <PartnersCarousel context={context}/>
        <Footer />
        <MenuBottom context={context}/>
        <GoUpButton />
      </div>
    );
  }
}
