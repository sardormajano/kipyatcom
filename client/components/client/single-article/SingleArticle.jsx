import React, {Component} from 'react';

import {Link} from 'react-router';

import Header from '../fuc/Header';
import Footer from '../fuc/Footer';
import MenuBottom from '../fuc/MenuBottom';
import PartnersCarousel from '../fuc/PartnersCarousel';
import GoUpButton from '../fuc/GoUpButton';
import ShopsAccordion from '../fuc/ShopsAccordion';
import Insta from '../fuc/Insta';

import TopBanner from '../fuc/TopBanner';
import Banner2 from '../fuc/Banner2';
import BannerA from '../fuc/BannerA';
import Banner3 from '../fuc/Banner3';
import BannerB from '../fuc/BannerB';
import Banner4 from '../fuc/Banner4';
import Banner5 from '../fuc/Banner5';

import moment from 'moment';

import {idToName} from '/client/lib/collectionsRelated';

export default class SingleArticle extends Component {
  renderSlides() {
    const {context} = this.props,
          {state} = context;

    if(!state.photos)
      return <div></div>;

    return state.photos.slice(0, 5).map((photo, index) => {
      return (
        <article key={index}>
          <img alt src={photo} />
        </article>
      );
    });
  }

  renderSliderPost() {
    const {context} = this.props,
          theArticle = context.state;

    if(!theArticle.photos) return [];

    return (
      <article id="slider_post">
        <input defaultChecked type="radio" name="slider_post" id="slide1" />
        {theArticle.photos.map((photo, index) => {
          return (<input defaultChecked={index ? false : true} key={index} type="radio" name="slider_post" id={`slide${index+1}`} />);
        })}
        <div id="commands">
          {theArticle.photos.map((photo, index) => {
            return (<label key={index} htmlFor={`slide${index+1}`} />);
          })}
        </div>
        <div id="active">
          {theArticle.photos.map((photo, index) => {
            return (<label key={index} htmlFor={`slide${index+1}`} />);
          })}
        </div>
        <div id="slides_post">
          <div id="container">
            <div className="inner">
              {this.renderSlides()}
            </div>
          </div>
        </div>
      </article>
    );
  }

  renderAlbums() {
    const {context} = this.props,
          {albums} = context.props;

    if(!albums.length)
      return [];

    return albums.map((album, index) => {
      date = moment.unix(album.date).format('ll').split(' ');

      return (
        <div
          className="album_post"
          key={index}
        >
          <div
            className="album_1 pha-320 pha-375 pha-544"
          >
            <a className="fotograf"> <i className="fa fa-camera" aria-hidden="true" style={{padding: '0 3px 0 0'}} /> Артур Идрисов</a>
            <a className="View"> <i className="fa fa-eye" aria-hidden="true" style={{padding: '0 3px 0 0'}} />${album.views}</a>
            <div className="post-entry-meta">
              <div className="post-entry-meta-category">
                <span className="label label-date">{date[0]} {date[1]}</span>
                <span className="label label-year">{date[2]}</span>
              </div>
              <div className="post-entry-meta-title">
                <h2>
                  <a href={`/single-album/${album._id}`}>
                    {album.title}
                  </a>
                </h2>
              </div>
              <span className="post-title">
                {album.subtitle}
              </span>
            </div>
            <a
              className="album_pic crop"
              href={`/single-album/${album._id}`}
              style={{height: '100%', width: '100%', backgroundImage: `url(${album.cover})`}}>
              <img src="/img/albums/0.gif" />
            </a>
          </div>
        </div>
      );
    });
  }

  renderPopularArticles() {
    const {context} = this.props,
          {articles} = context.props;

    return articles.slice(0, 3).map((article, index) => {
      date = moment.unix(article.date).format('ll').split(' ');

      return (
        <div
          className="post_p"
          style={{}}
          key={index}
        >
          <div className="container">
            <div className="card cart-block">
              <a href={`/single-article/${article._id}`} className="post-hov">
                <div className="card-img">
                  <div className="post-date-big">
                    <span>{date[0]}</span> <p>{date[1]}</p>
                  </div>
                  <div className="post-thumbnail-content">
                    <h3>{article.title}</h3>
                    <p>{article.subtitle}</p>
                  </div>
                  <div
                    className="image-wrapper"
                    style={{backgroundImage: `url(${article.cover})`}}
                  >
                    <img alt src='/img/article/0.jpg' />
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      );
    });
  }

  renderTags() {
    const {context} = this.props,
          {state} = context,
          {tags} = context.props;

    if(!state.tags)
      return [];

    return state.tags.map((tag, index) => {
      return (
        <Link
          key={index}
          to={`/articles?filterTag=${tag}`}
          className
        >
          {idToName(tag, tags)}
        </Link>
      );
    });
  }

  render() {
    const {context} = this.props,
          {
            albums,
            articles,
            artists,
            theJournalist,
            banners,
            shops,
            shopTypes,
          } = context.props,
          theArticle = context.state,
          date = moment.unix(theArticle.date).format('ll').split(' ');

    return (
      <div className="ALLLL">
        <TopBanner context={context}/>
        <Header context={context}/>
        <Banner2 context={context}/>
        <section className="filter-a">
          <a className="button-a" href="#"> <i className="fa fa-arrow-left" aria-hidden="true" /> </a>
          <ul className="mini-menu">
            <li><a href="#">Статьи<i className="fa fa-angle-down" aria-hidden="true" /> </a>
              <ul className="submenu">
                <li><a href="#">Фотоотчеты</a></li>
                <li><a href="#">Анонсы</a></li>
                <li><a href="#">Заведения</a></li>
                <li><a href="#">Видео</a></li>
                <li><a href="#">Контакты</a></li>
              </ul>
            </li>
          </ul>
          <div className="date-calendar">
            {/* tag element */}
            <input className="date-a" type="text" data-lang="ru" data-format="d-m-Y" data-fx-mobile="true" data-large-mode="true" />
            {/* init dateDropper */}
          </div>
          <div className="search-a">
            <ul className="nav">
              <li className="nav-search">
                <form action="#">
                  <input type="text" placeholder="Поиск по заголовку..." />
                  <input type="submit" defaultValue="Найти" />
                </form>
              </li>
            </ul>
          </div>
          <div className="select-menu">
            <ul className="mini-menu-a">
              <li><a>Все статьи<i className="fa fa-angle-down" aria-hidden="true" style={{padding: '0 0 0 5px'}} /> </a>
                <ul className="submenu-a">
                  <li><a href="#" className="submenu-active">Все статьи</a></li>
                  <li><a href="#">Новости</a></li>
                  <li><a href="#">Fashion</a></li>
                  <li><a href="#">Звезды</a></li>
                  <li><a href="#">Красота и здоровье</a></li>
                  <li><a href="#">Стиль жизни</a></li>
                  <li><a href="#">Культура</a></li>
                  <li><a href="#">Спорт</a></li>
                  <li><a href="#">Шоу Бизнес</a></li>
                  <li><a href="#">Технологии</a></li>
                  <li><a href="#">Дом, дети</a></li>
                  <li><a href="#">Авто</a></li>
                  <li><a href="#">Город</a></li>
                </ul>
              </li>
            </ul>
          </div>
          <div className="checkbox-a">
            <input id="check1" type="checkbox" name="check" defaultValue="check1" />
            <label htmlFor="check1">Ночной дозор</label>
            <input id="check2" type="checkbox" name="check" defaultValue="check2" />
            <label htmlFor="check2">Светская хроника</label>
          </div>
        </section>
        <section className="filter-b">
          <div className="poisk-a">
            <div className="search-a">
              <ul className="nav">
                <li className="nav-search">
                  <form action="#">
                    <i className="fa fa-search" aria-hidden="true" />
                    <input type="text" placeholder="Поиск по заголовку..." />
                    <div className="date-calendar date-calendar-b">
                      {/* tag element */}
                      <input className="date-a" type="text" data-lang="ru" data-format="d-m-Y" data-fx-mobile="true" data-large-mode="true" />
                      {/* init dateDropper */}
                    </div>
                    <input type="submit" defaultValue="Найти" />
                  </form>
                </li>
              </ul>
            </div>
          </div>
          <ul id="accordion" className="accordion">
            <li>
              <div className="link"><i className="fa fa-glass" aria-hidden="true" />Все статьи<i className="fa fa-chevron-down" /></div>
              <ul className="submenu-b">
                <li><a href="#" className="submenu-active">Все статьи</a></li>
                <li><a href="#">Ночной дозор</a></li>
                <li><a href="#">Светская хроника</a></li>
                <li><a href="#">Новости</a></li>
                <li><a href="#">Fashion</a></li>
                <li><a href="#">Звезды</a></li>
                <li><a href="#">Красота и здоровье</a></li>
                <li><a href="#">Стиль жизни</a></li>
                <li><a href="#">Культура</a></li>
                <li><a href="#">Спорт</a></li>
                <li><a href="#">Шоу Бизнес</a></li>
                <li><a href="#">Технологии</a></li>
                <li><a href="#">Дом, дети</a></li>
                <li><a href="#">Авто</a></li>
                <li><a href="#">Город</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <section className="mbr-section mbr-after-navbar" id="pricing-table1-6">
          <div className="content_wrapper post_full">
            <div className="post_wrapper">
              <div className="post_head">
                <div className="tag_1">
                  {this.renderTags()}
                </div>
                <h5 className>{theArticle ? theArticle.title : ''}</h5>
                <p>{theArticle ? theArticle.subtitle : ''}</p>
                <div className="post_author">
                  <div className="author_avatar">
                    <img src={theJournalist ? theJournalist.profile.cover : ''} />
                  </div>
                  <div className="post-author-name">
                    <div className="date01">
                      <time className dateTime="18-05-2017">{date[0]} {date[1]}, {date[2]}</time>
                    </div>
                    <div className="author">
                      <span className="author01">
                        Автор:</span> <span className="author02">
                        {theJournalist ? theJournalist.profile.first_name : ''} {' '}
                        {theJournalist ? theJournalist.profile.last_name : ''}
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="post_wrapper_mid">
              {this.renderSliderPost()}
            </div>
            <div className="post_wrapper">
              <div className="post_txt">
                <p dangerouslySetInnerHTML={{__html: context.state.contentStart}}>
                </p>
                <p></p>
                <div
                  style={{display: context.state.videoCode ? '' : 'none'}} 
                  className="myvideo"
                  dangerouslySetInnerHTML={{__html: context.state.videoCode}}
                >
                </div>
                <p></p>
                <p dangerouslySetInnerHTML={{__html: context.state.contentEnd}}>
                </p>
                <p></p>
                <div className="sharing">
                  <div className="ya-share2" data-services="vkontakte,facebook,gplus,twitter,whatsapp,telegram" data-counter />
                </div>
              </div>
            </div>
            <div
              className="post_wrapper_photo"
              style={{display: albums.length ? '' : 'none'}}
            >
              <div className="heading-fancy2 heading-line2">
                <h4><i className="fa fa-camera" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Фотоотчеты Мероприятия</h4>
              </div>
              <div className="post_cont">
                {this.renderAlbums()}
              </div>
            </div>
            <div className="content_wrapper">
              <div className="heading-fancy2 heading-line2">
                <h4><i className="fa fa-newspaper-o" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Популярное в категории</h4>
              </div>
              {this.renderPopularArticles()}
            </div>
          </div>
        </section>
        <Banner4 context={context} />
        <Insta />
        <PartnersCarousel context={context} />
        <Footer />
        <MenuBottom context={context}/>
        <GoUpButton />
      </div>
    );
  }
}
