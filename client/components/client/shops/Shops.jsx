import React, {Component} from 'react';

import Header from '../fuc/Header';
import Footer from '../fuc/Footer';
import MenuBottom from '../fuc/MenuBottom';
import PartnersCarousel from '../fuc/PartnersCarousel';
import GoUpButton from '../fuc/GoUpButton';
import ShopsAccordion from '../fuc/ShopsAccordion';
import Insta from '../fuc/Insta';
import TopBanner from '../fuc/TopBanner';
import Banner2 from '../fuc/Banner2';
import BannerA from '../fuc/BannerA';
import Banner3 from '../fuc/Banner3';
import BannerB from '../fuc/BannerB';
import Banner4 from '../fuc/Banner4';
import Banner5 from '../fuc/Banner5';

import {Link} from 'react-router';

import {createPositionCompareFunction} from '/client/lib/general';
import {idToName} from '/client/lib/collectionsRelated';

export default class Shops extends Component {
  dotThatShit() {
    $(this.cw).find('.dotdotdot-wrapper').dotdotdot();
  }

  componentDidMount() {
    //this.dotThatShit();
  }

  componentDidUpdate() {
    //this.dotThatShit();
  }

  renderShops(shops) {
    const {context} = this.props,
          {shopTypes} = context.props;

    return shops.map((shop, index) => {
      return (
        <div className="place" key={index}>
          <div className="place_logo">
            <a className="r-more" href="#"><img src={shop.cover} />	</a>
          </div>
          <div className="place_name">
            <Link to={`/single-shop/${shop._id}`}>
              <span>{shop.name}</span>
              <p>{shop.type2}</p>
            </Link>
            <p>
              <span className="dotdotdot-wrapper" style={{height: '2.5em'}}>
                <i className="fa fa-map-marker" aria-hidden="true" />
                {shop.address}
              </span>
              <a href={`tel:${shop.phone}`}>
                <i className="fa fa-phone" aria-hidden="true" />
                {shop.phone}</a>
            </p>
          </div>
        </div>
      );
    });
  }

  render() {
    const {context} = this.props,
          {shops} = context.props,
          numbers = new RegExp('^[0-9|#]'),
          latin = new RegExp('^[A-Za-z]'),
          cyrillic = new RegExp('^[А-Яа-я]'),
          kazakh = new RegExp('^[әғқңөұү]', 'i'),
          sortFunction = createPositionCompareFunction('position', 'name'),
          mainShops = shops.filter(sh => sh.isMain).sort(sortFunction),
          nonMainShops = shops.filter(sh => !sh.isMain);
          // numberShops = Array.prototype.slice.call(shops)
          //   .filter(shop => numbers.test(shop.name))
          //   .sort((a, b) => a.name > b.name ? 1 : -1),
          // latinShops = Array.prototype.slice.call(shops)
          //   .filter(shop => latin.test(shop.name))
          //   .sort((a, b) => a.name > b.name ? 1 : -1),
          // cyrillicShops = Array.prototype.slice.call(shops)
          //   .filter(shop => cyrillic.test(shop.name))
          //   .sort((a, b) => a.name > b.name ? 1 : -1),
          // kazakhShops = Array.prototype.slice.call(shops)
          //   .filter(shop => kazakh.test(shop.name))
          //   .sort((a, b) => a.name > b.name ? 1 : -1);

    return (
      <div className="ALLLL">
        <TopBanner context={context}/>
        <Header context={context}/>
        <Banner2 context={context}/>
        <section className="filter-a">
          <a className="button-a" href="#"> <i className="fa fa-arrow-left" aria-hidden="true" /> </a>
          <ul className="mini-menu">
            <li><a href="#">Заведения <i className="fa fa-angle-down" aria-hidden="true" /> </a>
              <ul className="submenu">
                <li><a href="#">Фотоотчеты</a></li>
                <li><a href="#">Анонсы</a></li>
                <li><a href="#">Статьи</a></li>
                <li><a href="#">Видео</a></li>
                <li><a href="#">Контакты</a></li>
              </ul>
            </li>
          </ul>
          <div className="search-a">
            <ul className="nav">
              <li className="nav-search">
                <form action="#">
                  <input type="text" placeholder="Поиск по заголовку..." style={{width: 500}} />
                  <input type="submit" defaultValue="Найти" />
                </form>
              </li>
            </ul>
          </div>
          <div className="select-menu">
            <ul className="mini-menu-a">
              <li><a>Все заведения<i className="fa fa-angle-down" aria-hidden="true" style={{padding: '0 0 0 5px'}} /> </a>
                <ul className="submenu-a">
                  <li><a href="#" className="submenu-active">Все заведения</a></li>
                  <li><a href="#">Рестораны</a></li>
                  <li><a href="#">Бары</a></li>
                  <li><a href="#">Клубы</a></li>
                  <li><a href="#">Караоке</a></li>
                  <li><a href="#">Летние террасы</a></li>
                  <li><a href="#">Кофейни</a></li>
                  <li><a href="#">Шопинг</a></li>
                  <li><a href="#">Beauty</a></li>
                  <li><a href="#">Гос.Сфера</a></li>
                  <li><a href="#">Дети</a></li>
                  <li><a href="#">Культура</a></li>
                  <li><a href="#">Спорт</a></li>
                  <li><a href="#">Концерты</a></li>
                  <li><a href="#">Авто</a></li>
                  <li><a href="#">Город</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </section>
        <section className="filter-b">
          <div className="poisk-a">
            <div className="search-a">
              <ul className="nav">
                <li className="nav-search">
                  <form action="#">
                    <i className="fa fa-search" aria-hidden="true" />
                    <input type="text" placeholder="Поиск по заголовку..." />
                    <input type="submit" defaultValue="Найти" />
                  </form>
                </li>
              </ul>
            </div>
          </div>
          <ul id="accordion" className="accordion">
            <li>
              <div className="link"><i className="fa fa-glass" aria-hidden="true" />Все заведения<i className="fa fa-chevron-down" /></div>
              <ul className="submenu-b">
                <li><a href="#" className="submenu-b-active">Все заведения</a></li>
                <li><a href="#">Рестораны</a></li>
                <li><a href="#">Бары</a></li>
                <li><a href="#">Клубы</a></li>
                <li><a href="#">Караоке</a></li>
                <li><a href="#">Летние террасы</a></li>
                <li><a href="#">Кофейни</a></li>
                <li><a href="#">Шопинг</a></li>
                <li><a href="#">Beauty</a></li>
                <li><a href="#">Гос.Сфера</a></li>
                <li><a href="#">Дети</a></li>
                <li><a href="#">Культура</a></li>
                <li><a href="#">Спорт</a></li>
                <li><a href="#">Концерты</a></li>
                <li><a href="#">Авто</a></li>
                <li><a href="#">Город</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <section ref={cw => this.cw = cw} className="mbr-section mbr-after-navbar" id="pricing-table1-6">
          <div className="mbr-section mbr-section-nopadding mbr-price-table">
            <div className="content_wrapper">
              <div
                className="heading-fancy2 heading-line2"
                style={{display: mainShops.length ? '' : 'none'}}
              >
                <h4>
                  <i className="fa fa-trophy" aria-hidden="true" style={{padding: '0 5px 0 0'}} />
                  ТОП
                </h4>
              </div>
              {this.renderShops(mainShops)}
              <div
                className="heading-fancy2 heading-line2"
                style={{display: nonMainShops.length ? '' : 'none'}}
              >
                <h4><i className="fa fa-cutlery" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> A-Z</h4>
              </div>
              {this.renderShops(nonMainShops.filter(shop => !shop.isMain))}
            </div>
          </div>
          <div className="button_place">
            <p>
              <button
                onClick={context.loadMoreHandler.bind(context)}
                className="btn-a"
              >
                <i className="fa fa-angle-down" aria-hidden="true" />
                Еще
                <i className="fa fa-angle-down" aria-hidden="true" />
              </button>
            </p>
          </div>
        </section>
        <Banner4 context={context}/>
        <Insta />
        <PartnersCarousel context={context}/>
        <Footer />
        <MenuBottom context={context}/>
        <GoUpButton />
      </div>
    );
  }
}
