import React, {Component} from 'react';

import Header from '../fuc/Header';
import Footer from '../fuc/Footer';
import PartnersCarousel from '../fuc/PartnersCarousel';
import MenuBottom from '../fuc/MenuBottom';
import GoUpButton from '../fuc/GoUpButton';
import Insta from '../fuc/Insta';

export default class NotFound extends Component {
  render() {
    const {context} = this.props;

    return (
      <div className="ALLLL">
        <Header context={context}/>
        <section className="mbr-section mbr-after-navbar" id="pricing-table1-6">
          <div className="content_wrapper post_full">
            <div className="oops-wrapper">
              <div className="oops-txt">
                <p>Страница не найдена</p>
                <span><button className="btn-e">Вернутся на главную</button></span>
              </div>
            </div>
          </div>
        </section>
        <Insta />
        <PartnersCarousel context={context}/>
        <Footer />
        <MenuBottom context={context}/>
        <GoUpButton />
      </div>
    );
  }
}
