import React, {Component} from 'react';

import Header from '../fuc/Header';
import Footer from '../fuc/Footer';
import MenuBottom from '../fuc/MenuBottom';
import PartnersCarousel from '../fuc/PartnersCarousel';
import GoUpButton from '../fuc/GoUpButton';
import ShopsAccordion from '../fuc/ShopsAccordion';
import Insta from '../fuc/Insta';
import TopBanner from '../fuc/TopBanner';
import Banner2 from '../fuc/Banner2';
import BannerA from '../fuc/BannerA';
import Banner3 from '../fuc/Banner3';
import BannerB from '../fuc/BannerB';
import Banner4 from '../fuc/Banner4';
import Banner5 from '../fuc/Banner5';
import Accordion from '../fuc/Accordion';

import moment from 'moment';

import {addScript} from '/client/lib/scriptsRelated';
import {idToName} from '/client/lib/collectionsRelated';
import {mfLinkToId, transliterate} from '/client/lib/general';

import {Link, browserHistory} from 'react-router';

import Masonry from 'react-masonry-component';

export default class Albums extends Component {
  initPhotoSwipeFromDOM(gallerySelector) {

      const self = this;

      // parse slide data (url, title, size ...) from DOM elements
      // (children of gallerySelector)
      var parseThumbnailElements = function(el) {
          var thumbElements = el.childNodes,
              numNodes = thumbElements.length,
              items = [],
              figureEl,
              linkEl,
              size,
              item;

          for(var i = 0; i < numNodes; i++) {

              figureEl = thumbElements[i]; // <figure> element

              // include only element nodes
              if(figureEl.nodeType !== 1) {
                  continue;
              }

              linkEl = figureEl.children[0]; // <a> element

              size = linkEl.getAttribute('data-size') ? linkEl.getAttribute('data-size').split('x') : ["0", "0"];

              // create slide object
              item = {
                  src: linkEl.getAttribute('href'),
                  w: parseInt(size[0], 10),
                  h: parseInt(size[1], 10)
              };



              if(figureEl.children.length > 1) {
                  // <figcaption> content
                  item.title = figureEl.children[1].innerHTML;
              }

              if(linkEl.children.length > 0) {
                  // <img> thumbnail element, retrieving thumbnail url
                  item.msrc = linkEl.children[0].getAttribute('src');
              }

              item.el = figureEl; // save link to element for getThumbBoundsFn
              items.push(item);
          }

          return items;
      };

      // find nearest parent element
      var closest = function closest(el, fn) {
          return el && ( fn(el) ? el : closest(el.parentNode, fn) );
      };

      // triggers when user clicks on thumbnail
      var onThumbnailsClick = function(e) {
          e = e || window.event;
          e.preventDefault ? e.preventDefault() : e.returnValue = false;

          var eTarget = e.target || e.srcElement;

          // find root element of slide
          var clickedListItem = closest(eTarget, function(el) {
              return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
          });

          if(!clickedListItem) {
              return;
          }

          // find index of clicked item by looping through all child nodes
          // alternatively, you may define index via data- attribute
          var clickedGallery = clickedListItem.parentNode,
              childNodes = clickedListItem.parentNode.childNodes,
              numChildNodes = childNodes.length,
              nodeIndex = 0,
              index;

          for (var i = 0; i < numChildNodes; i++) {
              if(childNodes[i].nodeType !== 1) {
                  continue;
              }

              if(childNodes[i] === clickedListItem) {
                  index = nodeIndex;
                  break;
              }
              nodeIndex++;
          }



          if(index >= 0) {
              // open PhotoSwipe if valid index found
              openPhotoSwipe( index, clickedGallery );
          }
          return false;
      };

      // parse picture index and gallery index from URL (#&pid=1&gid=2)
      var photoswipeParseHash = function() {
          var hash = window.location.hash.substring(1),
          params = {};

          if(hash.length < 5) {
              return params;
          }

          var vars = hash.split('&');
          for (var i = 0; i < vars.length; i++) {
              if(!vars[i]) {
                  continue;
              }
              var pair = vars[i].split('=');
              if(pair.length < 2) {
                  continue;
              }
              params[pair[0]] = pair[1];
          }

          if(params.gid) {
              params.gid = parseInt(params.gid, 10);
          }

          return params;
      };

      var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
          var pswpElement = document.querySelectorAll('.pswp')[0],
              gallery,
              options,
              items;

          items = parseThumbnailElements(galleryElement);

          // define options (if needed)
          options = {

              // define gallery index (for URL)
              galleryUID: galleryElement.getAttribute('data-pswp-uid'),

              getThumbBoundsFn: function(index) {
                  // See Options -> getThumbBoundsFn section of documentation for more info
                  var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                      pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                      rect = thumbnail.getBoundingClientRect();

                  return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
              }

          };

          // PhotoSwipe opened from URL
          if(fromURL) {
              if(options.galleryPIDs) {
                  // parse real index when custom PIDs are used
                  // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                  for(var j = 0; j < items.length; j++) {
                      if(items[j].pid == index) {
                          options.index = j;
                          break;
                      }
                  }
              } else {
                  // in URL indexes start from 1
                  options.index = parseInt(index, 10) - 1;
              }
          } else {
              options.index = parseInt(index, 10);
          }

          // exit if index not found
          if( isNaN(options.index) ) {
              return;
          }

          if(disableAnimation) {
              options.showAnimationDuration = 0;
          }

          // Pass data to PhotoSwipe and initialize it
          gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
          gallery.init();

          self.gallery = gallery;
      };

      // loop through all gallery elements and bind events
      var galleryElements = document.querySelectorAll( gallerySelector );

      for(var i = 0, l = galleryElements.length; i < l; i++) {
          galleryElements[i].setAttribute('data-pswp-uid', i+1);
          galleryElements[i].onclick = onThumbnailsClick;
      }

      // Parse URL and open gallery if it contains #&pid=3&gid=1
      var hashData = photoswipeParseHash();
      if(hashData.pid && hashData.gid) {
          openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
      }
  }

  dotThatShit() {
    const {context} = this.props,
          album = context.state,
          {shops} = context.props,
          currentShop = shops.filter(item => item._id === album.shopId)[0];

    if(!currentShop)
      return;

    const addressWrapper = $(this.cw).find('.address-wrapper');

    if(addressWrapper.length)
      addressWrapper.dotdotdot();
  }

  componentWillMount() {
    this.fig = [];
  }

  componentDidMount() {
    this.initPhotoSwipeFromDOM('.my-gallery');
  }

  componentDidUpdate() {
    this.dotThatShit();
  }

  renderPhotos() {
    const {context} = this.props,
          album = context.state,
          {photosNumber} = context.state,
          {artists, shops, shopTypes} = context.props,
          currentShop = shops.filter(item => item._id === album.shopId)[0],
          date = moment.unix(album.date);

    return album.photos.map((photo, index) => {
      const image = new Image();
      let imageReady = false;

      if(!album.version) {
        image.src = album.thumbnails ? album.thumbnails[mfLinkToId(photo)] : photo.replace('.jpg', '-thumbnail.jpg').replace('photos', 'thumbnails');
        image.onload = () => {
          this.fig[index].setAttribute('data-size', `${image.naturalWidth * 10}x${image.naturalHeight * 10}`);
        }
      }
      else if(album.version === 1) {
        image.src = photo.thumbnail
        image.onload = () => {
          this.fig[index].setAttribute('data-size', `${image.naturalWidth * 10}x${image.naturalHeight * 10}`);
        }

        photo = photo.photo;
      }

      return (
        <figure
          key={index}
          itemProp="associatedMedia"
          className="grid-item"
        >
          <a
            ref={fig => this.fig[index] = fig}
            href={photo}
            itemProp="contentUrl"
            style={{position: 'relative'}}
          >
            <img
              src={image.src}
              itemProp="thumbnail" alt="Image description"
            />
          </a>
          <figcaption itemProp="caption description">
            <span>{date.format('ll')}</span>
            <h2>{album.title}</h2>
            <p>{album.subtitle}</p>
          </figcaption>
        </figure>
      );
    });
  }

  render() {
    const {context} = this.props,
          album = context.state,
          {artists, shops, shopTypes} = context.props,
          currentShop = shops.filter(item => item._id === album.shopId)[0],
          date = album && album.date ? moment.unix(album.date) : '',
          theArtist = artists.filter(artist => artist._id === album.artistId)[0],
          fileName = date ? `${date.format('DD-MM-YYYY')}_${transliterate(album.title)}_${transliterate(album.subtitle)}.zip` : 'kipyatcom';

    return (
      <div ref={cw => this.cw = cw} className="ALLLL">
        <TopBanner context={context} />
        <Header context={context}/>
        <section className="zagolovok-a">
          <a
            className="button-b"
            href="#"
            onClick={e => {
              e.preventDefault();
              browserHistory.goBack();
            }}
          >
            <i className="fa fa-arrow-left" aria-hidden="true" />
          </a>
          <div className="date-name-a">
            <span>{date ? date.format('dddd') : ''}</span>
            <h2>{date ? date.format('DD') : <i className='fa fa-circle-o-notch fa-spin'/>}</h2>
            <p>{date ? date.format('MMMM') : ''}</p>
          </div>
          <div className="album_zagolovok">
            <h2>{album.title}</h2>
            <p>{album.subtitle}</p>
          </div>
          <div className="album_pl">
            <div className="place_logo">
              <Link to={`/single-shop/${currentShop ? currentShop._id: '#'}`}>
                <img src={currentShop ? currentShop.cover : ''} />
              </Link>
            </div>
            <div className="place_name">
              <Link
                className="demo-gallery__img--main"
                to={`/single-shop/${currentShop ? currentShop._id : '#'}`}>
                <span>{currentShop ? currentShop.name : ''}</span>
                <p>{currentShop ? currentShop.type2 : ''}</p>
              </Link>
              <p>
                <span
                  style={{height: '2em'}}
                  className="address-wrapper"
                >
                  <i className="fa fa-map-marker" aria-hidden="true" />{currentShop ? currentShop.address : ''}
                </span>
                <a
                  className="demo-gallery__img--main"
                  href={`tel: ${currentShop ? currentShop.phone : ''}`}
                >
                  <i className="fa fa-phone" aria-hidden="true" />{currentShop ? currentShop.phone : ''}
                </a>
              </p>
            </div>
          </div>
          <div className="album_photograph">
            <div className="photograph_avatar">
              <img src={theArtist ? theArtist.cover || '/insta-avatar.jpg' : '/insta-avatar.jpg'} />
            </div>
            <div className="photograph_name">
              <Link
                className="demo-gallery__img--main"
                to={`/albums?filterArtist=${theArtist ? theArtist._id : ''}`}
              >
                Фотограф
                <br /><span>{theArtist ? theArtist.name : '#kipyatcom'} {theArtist ? theArtist.surname : ''}</span>
              </Link>
            </div>
          </div>
          <a
            className="save-a"
            href="#"
            onClick={context.getZipFilesLink.bind(context)}
          >
            <i className="fa fa-download" aria-hidden="true" />Скачать фотоотчет
          </a>
          <Link
            className="pl-all-a"
            to={`/single-shop/${currentShop ? currentShop._id : '#'}`}
          >
            Все фотоотчеты заведения
            <i className="fa fa-angle-right" aria-hidden="true" />
          </Link>
        </section>
        <section className="zagolovok-b">
          <a
            className="button-c"
            href="#"
            onClick={e => {
              e.preventDefault();
              browserHistory.goBack();
            }}
          >
            <i className="fa fa-arrow-left" aria-hidden="true" />
          </a>
          <div className="zag-b">
            <div className="zag-b-in">
              <span>{date ? date.format('ll') : ''}</span>
              <h2>{album.title || <i className='fa fa-circle-o-notch fa-spin'/>}</h2>
              <p>{album.subtitle || ''}</p>
            </div>
          </div>
          <a
            className="button-save"
            href="#"
            onClick={context.getZipFilesLink.bind(context)}
          > <i className="fa fa-download" aria-hidden="true" /> </a>
          <Accordion>
            <li>
              <div className="link"><i className="fa fa-bars" aria-hidden="true" />Info<i className="fa fa-chevron-down" /></div>
              <div className="submenu-b">
                <div className="album_pl">
                  <div className="place_logo">
                    <Link to={`/single-shop/${currentShop ? currentShop._id : ''}`}>
                      <img src={currentShop ? currentShop.cover : ''} />
                    </Link>
                  </div>
                  <div className="place_name">
                    <Link
                      className="demo-gallery__img--main"
                      to={`/single-shop/${currentShop ? currentShop._id : ''}`}
                    >
                      <span>{idToName(album.shopId, shops)}</span>
                      <p>{currentShop ? currentShop.type2 : ''}</p>
                    </Link>
                    <p>
                      <span
                        style={{height: '2em'}}
                        className="address-wrapper"
                      >
                        <i className="fa fa-map-marker" aria-hidden="true" /> {currentShop ? currentShop.address : ''}
                      </span>
                      <a
                        className="demo-gallery__img--main phone_txt_number"
                        href={currentShop ? currentShop.phone : '#'}
                      >
                        <i className="fa fa-phone" aria-hidden="true" />
                        {currentShop ? currentShop.phone : ''}
                      </a>
                    </p>
                  </div>
                </div>
                <Link
                  className="pht-a"
                  to={`/albums?filterArtist=${theArtist ? theArtist._id : ''}`}
                >
                  <i className="fa fa-camera" aria-hidden="true" style={{top: 7, color: '#989898 !important'}} />
                  {theArtist ? theArtist.name : '#kipyatcom'} {theArtist ? theArtist.surname : ''}
                </Link>
              </div>
            </li>
          </Accordion>
        </section>
        <section className="mbr-section mbr-after-navbar" id="pricing-table1-6">
          <div className="content_wrapper">
            <a
              style={{display: 'none'}}
              className="_hidden-link"
              href=""
              download
            ></a>
            <div className="pswp" tabIndex={-1} role="dialog" aria-hidden="true">
              {/* Background of PhotoSwipe.
		         It's a separate element as animating opacity is faster than rgba(). */}
              <div className="pswp__bg" />
              {/* Slides wrapper with overflow:hidden. */}
              <div className="pswp__scroll-wrap">
                {/* Container that holds slides.
		            PhotoSwipe keeps only 3 of them in the DOM to save memory.
		            Don't modify these 3 pswp__item elements, data is added later on. */}
                <div className="pswp__container">
                  <div className="pswp__item" />
                  <div className="pswp__item" />
                  <div className="pswp__item" />
                </div>
                {/* Default (SwipeUI_Default) interface on top of sliding area. Can be changed. */}
                <div className="pswp__ui pswp__ui--hidden">
                  <div className="pswp__top-bar">
                    {/*  Controls are self-explanatory. Order can be changed. */}
                    <a
                      className="pswp__button pswp__button--download"
                      title="Скачать"
                      href="#"
                      onClick={e => {
                        e.preventDefault();
                        const hiddenLink = $('._hidden-link');
                        hiddenLink.attr('href', this.gallery.currItem.src);
                        hiddenLink[0].click();
                      }}
                    ></a>
                    <div className="kipyatcom-a"> <span>kipyat.com</span></div>
                    <button className="pswp__button pswp__button--close" title="Закрыть" />
                    <div className="pswp__preloader">
                      <div className="pswp__preloader__icn">
                        <div className="pswp__preloader__cut">
                          <div className="pswp__preloader__donut" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div className="pswp__share-tooltip" />
                  </div>
                  <div className="pswp__counter pswp__top-bar" id="counter_bot" />
                  <button className="pswp__button pswp__button--arrow--left" title="Назад">
                  </button>
                  <button className="pswp__button pswp__button--arrow--right" title="Вперед">
                  </button>
                  <div className="pswp__caption">
                    <div className="pswp__caption__center" />
                  </div>
                </div>
              </div>
            </div>
            {album.photos.length ? '' : <img style={{display: 'block', margin: 'auto'}} src="/img/albums/175x175.gif" />}
            <Masonry
              className={'my-gallery masonry grid'}
              updateOnEachImageLoad={false}
            >
              {this.renderPhotos()}
            </Masonry>
            <a
              style={{display: 'none'}}
              href="#"
              className="download-link"
              download={fileName}
            >
            </a>
          </div>
        </section>
        <Banner5 context={context} />
        <PartnersCarousel context={context}/>
        <Footer />
        <MenuBottom context={context}/>
        <GoUpButton />
      </div>
    );
  }
}
