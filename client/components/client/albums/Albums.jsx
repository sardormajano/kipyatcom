import React, {Component} from 'react';

import GoUpButton from '../fuc/GoUpButton';
import Insta from '../fuc/Insta';
import Filter from '../fuc/Filter';
import Footer from '../fuc/Footer';
import Header from '../fuc/Header';
import MenuBottom from '../fuc/MenuBottom';
import PartnersCarousel from '../fuc/PartnersCarousel';

import TopBanner from '../fuc/TopBanner';
import Banner2 from '../fuc/Banner2';
import BannerA from '../fuc/BannerA';
import Banner3 from '../fuc/Banner3';
import BannerB from '../fuc/BannerB';
import Banner4 from '../fuc/Banner4';
import Banner5 from '../fuc/Banner5';

import {Link} from 'react-router';

import moment from 'moment';
import {idToName} from '/client/lib/collectionsRelated';

import {createPositionCompareFunction} from '/client/lib/general.js';

export default class Albums extends Component {

  renderAlbums(albums) {
    const {context} = this.props,
          {artists} = context.props;

    return albums.map((album, index) => {
      const date = moment.unix(album.date).format('ll').split(' '),
            dayMonth = `${date[0]} ${date[1]}`,
            year = `${date[2]}`;

      return (
        <div
          className="album_0"
          key={index}
        >
          <div className="album_1 pha-320 pha-375 pha-544">
            <a className="fotograf">
              <i className="fa fa-camera" aria-hidden="true" style={{padding: '0 3px 0 0'}} />
              {idToName(album.artistId, artists)}
            </a>
            <a
              className="View"
              style={{display: album.hideViews ? 'none' : ''}}
            >
              <i className="fa fa-eye" aria-hidden="true" style={{padding: '0 3px 0 0'}} />
              {parseInt(album.views) * parseInt(album.viewMultiplier)}
            </a>
            <div className="post-entry-meta">
              <div className="post-entry-meta-category">
                <span className="label label-date">{dayMonth}</span>
                <span className="label label-year">{year}</span>
              </div>
              <div className="post-entry-meta-title">
                <h2><Link to={`/single-album/${album._id}`}>{album.title}</Link></h2>
              </div>
              <span className="post-title">{album.subtitle}</span>
            </div>
            <Link
              className="album_pic crop"
              to={`/single-album/${album._id}`}
              style={{height: '100%', width: '100%', backgroundColor: '#ccc', backgroundImage: `url(${album.cover})`}}
            >
              <img src='/img/albums/0.gif' />
            </Link>
          </div>
        </div>
      );
    });
  }

  render() {
    const {context} = this.props,
          {albums, artists} = context.props,
          {filterWord, filterTag, filterDate, filterArtist} = context.state,
          compareFunction = createPositionCompareFunction('topPosition', 'date'),
          topAlbums = albums
                      .filter(album => album.isTop)
                      .sort(compareFunction),
          filteredAlbums = albums.filter((album => {
            if(album.isTop)
              return false;

            //filtering by word
            if(!(new RegExp(filterWord, 'i')).test(album.title)
              && !(new RegExp(filterWord, 'i')).test(album.subtitle))
              return false;

            //filtering by tags
            if(filterTag.length) {
              if(!album.tags.includes(filterTag))
                return false;
            }

            //filtering by date
            if(filterDate.length) {
              const fDateArray = filterDate.split('-');

              const fDate = moment([fDateArray[2], parseInt(fDateArray[1]) - 1, fDateArray[0]]),
                    aDate = moment.unix(album.date);

              if(fDate.get('year') !== aDate.get('year')
                  || fDate.get('month') !== aDate.get('month')
                   || fDate.get('date') !== aDate.get('date'))
                return false;
            }

            //filter by artists
            if(filterArtist.length) {
              if(album.artistId !== filterArtist)
                return false;
            }

            return true;
          })).slice(0, context.state.albumsNumber);

    return (
      <div className="ALLLL">
        <TopBanner context={context} />
        <Header context={context} />
        <Banner2 context={context} />
        <Filter context={context} />
        <section className="mbr-section mbr-after-navbar" id="pricing-table1-6">
          <div className="mbr-section mbr-section-nopadding mbr-price-table">
            <div className="pha">
              <div
                style={{display: !filteredAlbums.length && context.state.albumsReady ? '' : 'none'}}
                className="heading-fancy2 heading-line2"
              >
        				<h4>
                  <i className="fa fa-frown-o" aria-hidden="true" style={{padding: '0 5px 0 0'}}></i>
                  Ничего не найдено
                </h4>
        			</div>
              <div
                style={{display: (filterWord.length || filterTag.length || filterDate.length || filterArtist.length) && filteredAlbums.length ? 'none' : ''}}
                className="head-2 heading-fancy2 heading-line2"
              >
        				<h4><i className="fa fa-thumb-tack" aria-hidden="true" style={{padding: '0 5px 0 0'}}></i> Популярные фотоотчеты </h4>
        			</div>
              {(filterWord.length || filterTag.length || filterDate.length || filterArtist.length) && filteredAlbums.length ? '' : this.renderAlbums(topAlbums)}
              <div className="heading-fancy2 heading-line2">
        				<h4>
                  <i className="fa fa-newspaper-o" aria-hidden="true" style={{padding: '0 5px 0 0'}}></i>
                  {filterWord.length || filterTag.length || filterDate.length || filterArtist.length ? `Найдено ${context.state.foundNumber}` : 'Все фотоотчеты' }
                </h4>
        			</div>
              {this.renderAlbums(filteredAlbums)}
            </div>
          </div>
          <div className="button_place">
            <p>
              <button
                className="btn-a"
                onClick={context.loadMoreHandler.bind(context)}
                dangerouslySetInnerHTML={{__html: context.state.loadMoreLabel}}
              />
            </p>
          </div>
        </section>
        <Banner5 context={context}/>
        <Insta />
        <PartnersCarousel context={context}/>
        <Footer />
        <MenuBottom context={context}/>
        <GoUpButton />
      </div>
    );
  }
}
