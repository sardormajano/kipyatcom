import React, {Component} from 'react';

import Header from '../fuc/Header';
import HomeSlider from '../fuc/HomeSlider';
import Footer from '../fuc/Footer';
import PartnersCarousel from '../fuc/PartnersCarousel';
import MenuBottom from '../fuc/MenuBottom';
import GoUpButton from '../fuc/GoUpButton';
import Insta from '../fuc/Insta';
import HomeAlbums from '../fuc/HomeAlbums';
import TopBanner from '../fuc/TopBanner';
import Banner2 from '../fuc/Banner2';
import BannerA from '../fuc/BannerA';
import Banner3 from '../fuc/Banner3';
import BannerB from '../fuc/BannerB';
import Banner4 from '../fuc/Banner4';
import Banner5 from '../fuc/Banner5';
import ModalBanner from '../fuc/ModalBanner';
import HomeAnnouncements from '../fuc/HomeAnnouncements';
import HomeArticle from '../fuc/HomeArticle';
import HomeVideo from '../fuc/HomeVideo';

export default class Home extends Component {
  componentDidMount() {
  }

  render() {
    const {context} = this.props;

    return (
      <div className="ALLLL">
        <ModalBanner context={context}/>
        <TopBanner context={context}/>
        <Header context={context}/>
        <Banner2 context={context}/>
        <section className="mbr-section mbr-after-navbar" id="pricing-table1-6">
          <div className="mbr-section mbr-section-nopadding mbr-price-table">
            <div className="row">
              <div className="col-xs-12-1  col-sm-6 col-md-6 col-xl-3 col-xxl-3" id="imgimg">
                <HomeSlider context={context}/>
                <BannerA context={context}/>
              </div>
              <HomeAlbums context={context} />
            </div>
          </div>
        </section>
        <Banner3 context={context}/>
        <section className="mbr-cards mbr-section mbr-section-nopadding mbr-after-navbar" id="features1-h" style={{backgroundColor: 'rgb(255, 255, 255)'}}>
          <div className="mbr-cards-row row striped">
            <HomeAnnouncements context={context}/>
            <div className="col-xs-1-1  col-sm-1-1 col-md-1-1 col-lg-1-1 col-xl-1-1" style={{paddingTop: 10, paddingBottom: 10}}>
              <div className="heading-fancy heading-line">
                <h4 style={{color: '#e4e4e4 !important', fontWeight: 'normal !important', background: '#fff'}}> Реклама</h4>
              </div>
              <BannerB context={context}/>
            </div>
          </div>
        </section>
        <Banner4 context={context}/>
        <div className="content-line">
          <HomeArticle context={context}/>
          <HomeVideo context={context}/>
        </div>
        <Banner5 context={context}/>
        <Insta />
        {/* Elastislide Carousel */}
        <PartnersCarousel context={context}/>
        {/* End Elastislide Carousel */}
        <Footer />
        <MenuBottom context={context}/>
        <GoUpButton />
      </div>
    );
  }
}
