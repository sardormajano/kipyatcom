import React, {Component} from 'react';

import GoUpButton from '../fuc/GoUpButton';
import Insta from '../fuc/Insta';
import Filter from '../fuc/Filter';
import Footer from '../fuc/Footer';
import Header from '../fuc/Header';
import MenuBottom from '../fuc/MenuBottom';
import PartnersCarousel from '../fuc/PartnersCarousel';

import TopBanner from '../fuc/TopBanner';
import Banner2 from '../fuc/Banner2';
import BannerA from '../fuc/BannerA';
import Banner3 from '../fuc/Banner3';
import BannerB from '../fuc/BannerB';
import Banner4 from '../fuc/Banner4';
import Banner5 from '../fuc/Banner5';

import {idToName} from '/client/lib/collectionsRelated';

export default class Contacts extends Component {
  renderContacts(contacts) {
    const {context} = this.props,
          {cities} = context.props;

    return contacts.map(contact => {
      return (
        <div key={contact._id} className="contact_head">
          <a href={`http://${contact.domain}`}> {contact.domain}</a>
          <h5 className>{idToName(contact.cityId, cities)}</h5>
          <a href={`tel:${contact.phone}`} className="phone_txt_number_contacts" style={{WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)'}}>
            <i className="fa fa-phone" aria-hidden="true" style={{padding: '0 5px 0 0', color: '#afafaf'}} />{contact.phone}</a>
          <p><i className="fa fa-envelope" aria-hidden="true" style={{padding: '0 5px 0 0', color: '#afafaf'}} />{contact.email}</p>
          <a href="https://www.instagram.com/kipyatcom/" target="_blank">
            <i className="fa fa-instagram" aria-hidden="true" style={{padding: '0 5px 0 0', color: '#afafaf'}} />
            <span>{contact.insta}</span>
          </a>
        </div>
      );
    });
  }

  renderRegionContacts(contacts) {
    const {context} = this.props,
          {cities} = context.props;

    return contacts.map(contact => {
      return (
        <div key={contact._id} className="region_cont">
          <a href={`http://${contact.domain}`}>{contact.domain}</a>
          <h5 className>{idToName(contact.cityId, cities)}</h5>
          <a href={`tel:${contact.phone}`} className="phone_txt_number_contacts" style={{WebkitTapHighlightColor: 'rgba(0, 0, 0, 0)'}}>
            <i className="fa fa-phone" aria-hidden="true" style={{padding: '0 5px 0 0', color: '#afafaf'}} />{contact.phone}</a>
          <p><i className="fa fa-envelope" aria-hidden="true" style={{padding: '0 5px 0 0', color: '#afafaf'}} />{contact.email}</p>
          <a href={`https://www.instagram.com/${contact.insta.slice(1)}/`} target="_black">
            <i className="fa fa-instagram" aria-hidden="true" style={{padding: '0 5px 0 0', color: '#afafaf'}} />
            {contact.insta}
          </a>
        </div>
      );
    });
  }

  render() {
    const {context} = this.props,
          {contacts} = context.props,
          astanaContact = contacts.filter(contact => contact.cityId === '0'),
          otherContacts = contacts.filter(contact => contact.cityId !== '0');

    return (
      <div className="ALLLL">
        <TopBanner context={context}/>
        <Header context={context}/>
        <section className="mbr-section mbr-after-navbar" id="pricing-table1-6">
          <div className="heading-fancy2 heading-line2">
            <h4><i className="fa fa-phone" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Контакты</h4>
          </div>
          <div className="content_wrapper post_full">
            <div className="post_wrapper">
              {this.renderContacts(astanaContact)}
              <div className="contact_head">
                <div className="heading-fancy2 heading-line2">
                  <h4><i className="fa fa-phone" aria-hidden="true" style={{padding: '0 5px 0 0'}} />Регионы</h4>
                </div>
                {this.renderRegionContacts(otherContacts)}
              </div>
              <div className="sharing">
                <div className="ya-share2" data-services="vkontakte,facebook,gplus,twitter,whatsapp,telegram" data-counter />
              </div>
            </div>
          </div>
        </section>
        <Insta context={context}/>
        <PartnersCarousel context={context}/>
        <Footer context={context}/>
        <MenuBottom context={context}/>
        <GoUpButton context={context}/>
      </div>
    );
  }
}
