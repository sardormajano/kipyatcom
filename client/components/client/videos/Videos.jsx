import React, {Component} from 'react';

import Header from '../fuc/Header';
import Footer from '../fuc/Footer';
import MenuBottom from '../fuc/MenuBottom';
import PartnersCarousel from '../fuc/PartnersCarousel';
import GoUpButton from '../fuc/GoUpButton';
import ShopsAccordion from '../fuc/ShopsAccordion';
import Insta from '../fuc/Insta';
import TopBanner from '../fuc/TopBanner';
import Banner2 from '../fuc/Banner2';
import BannerA from '../fuc/BannerA';
import Banner3 from '../fuc/Banner3';
import BannerB from '../fuc/BannerB';
import Banner4 from '../fuc/Banner4';
import Banner5 from '../fuc/Banner5';
import Filter from '../fuc/Filter';
import Modal from '../fuc/Modal';

import {Link} from 'react-router';

import moment from 'moment';

import {idToName} from '/client/lib/collectionsRelated';
import {createPositionCompareFunction} from '/client/lib/general.js';

export default class Videos extends Component {

  renderVideos(videos) {
    const {context} = this.props;

    return videos
      .map((video, index) => {
        const date = moment.unix(video.date).format('ll').split(' '),
              dayMonth = `${date[0]} ${date[1]}`;

        return (
          <div className="post_p" key={index}>
            <div className="container">
              <div className="card cart-block">
                <a
                  href="#"
                  className="post-hov video-trigger"
                  data-state-name='theVideo'
                  data-state-value={video._id}
                >
                  <div className="card-img">
                    <div className="post-date-big">
                      <span>{date[0]}</span> <p>{date[1]}</p>
                    </div>
                    <div className="post-play-icon">
                      <i className="fa fa-play" aria-hidden="true" />
                    </div>
                    <div className="post-thumbnail-content">
                      <h3>{video.title}</h3>
                      <p>{video.subtitle}</p>
                    </div>
                    <div
                      className="image-wrapper"
                      style={{backgroundSize: '100% 100%', backgroundImage: `url(${video.cover})`}}
                    >
                      <img
                        alt
                        src='/img/video/0.gif'
                      />
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        );
      });
  }

  renderTags(ts) {
    if(!ts)
      return <div />;

    const {context} = this.props,
          {tags} = context.props;

    return ts.map((tag, index) => {
      return (
        <a href="#" className>{idToName(tag, tags)}</a>
      );
    });
  }

  render() {
    const {context} = this.props,
          {theVideo} = context.state,
          {videos} = context.props,
          compareFunction = createPositionCompareFunction('topPosition', 'date'),
          topVideos = videos
                        .filter(video => video.isTop)
                        .sort(compareFunction),
          latestVideos = videos.filter(video => !video.isTop),
          currentVideo = videos.filter(v => v._id === theVideo)[0],
          date = currentVideo ? moment.unix(currentVideo.date).format('ll').split(' ') : '';

    return (
      <div className="ALLLL">
       <TopBanner context={context}/>
       <Header context={context}/>
       <Banner2 context={context}/>
       <Filter context={context}/>
       <section
         className="mbr-section mbr-after-navbar"
         id="pricing-table1-6"
         ref={cw => this.cw = cw}
       >
         <div className="mbr-section mbr-section-nopadding mbr-price-table">
           <div className="content_wrapper_dark">
             <div className="head-dark-1">
               <h4><i className="fa fa-camera" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Видео</h4>
             </div>
             <div className="head-dark-2">
               <h4><i className="fa fa-thumb-tack" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Популярные Видео </h4>
             </div>
             {this.renderVideos(topVideos)}
             <div className="head-dark">
               <h4><i className="fa fa-newspaper-o" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Последние Видео</h4>
             </div>
             {this.renderVideos(latestVideos)}
           </div>
         </div>
         <div className="button_place">
           <p>
             <button
               onClick={context.loadMoreHandler.bind(context)}
               className="btn-a"
             >
               <i className="fa fa-angle-down" aria-hidden="true" />
               {context.state.loadMoreLabel}
               <i className="fa fa-angle-down" aria-hidden="true" />
             </button>
           </p>
         </div>
       </section>
       <Modal
         context={context}
         triggerSelector='.video-trigger'
       >
         <a href="#" className="close"> </a>
         <header id="header_anons_video">
           <h1><a href="#"><img src="img/logo_kipyatcom.svg" className="logo_big" /></a></h1>
           <h6><i className="fa fa-video-camera" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Видео </h6>
         </header>
         <div className="modal_info">
           <div className="post_head">
             <div className="tag_1">
               {currentVideo ? this.renderTags(currentVideo.tags) : []}
             </div>
             <h5 className>{currentVideo ? currentVideo.title : ''}</h5>
             <p>{currentVideo ? currentVideo.subtitle : ''}</p>
             <div className="post_author">
               <div className="post-author-name">
                 <div className="date01">
                   <time className dateTime="18-05-2017">{date[0]} {date[1]}, {date[2]}</time>
                 </div>
               </div>
             </div>
           </div>
           <div className="video">

             <p>
               <span dangerouslySetInnerHTML={{__html: currentVideo ? currentVideo.videoCode : ''}}></span>
               <a href="#">
                 {currentVideo ? currentVideo.title : ''} / {currentVideo ? currentVideo.subtitle : ''} / {date[0]} {date[1]}, {date[2]}
               </a>
               from
               <a href="https://vimeo.com/kipyatcom">kipyat.com</a>
               on
               <a href="https://vimeo.com">Vimeo</a>.
             </p>
           </div>
           <Link to="/videos">
             <button
               id="all-video-white-text"
               className="btn-b"
               style={{
                 border: 'none',
                 padding: '0px 10px',
                 fontSize: '0.7rem',
                 backgroundColor: '#d9534f',
                 margin: '20px 0 0 0'
               }}>
               <i style={{color: 'white'}} className="fa fa-bookmark-o" aria-hidden="true" />
               <span style={{color: 'white'}}>Все Видео</span>
             </button>
           </Link>
           <div className="sharing">
             <div className="ya-share2" data-services="vkontakte,facebook,gplus,twitter,whatsapp,telegram" data-counter />
           </div>
         </div>
       </Modal>
       <Banner4 context={context}/>
       <Insta />
       <PartnersCarousel context={context}/>
       <Footer context={context}/>
       <MenuBottom context={context}/>
       <GoUpButton />
     </div>
    );
  }
}
