import React, {Component} from 'react';

import Header from '../fuc/Header';
import HomeSlider from '../fuc/HomeSlider';
import Footer from '../fuc/Footer';
import PartnersCarousel from '../fuc/PartnersCarousel';
import MenuBottom from '../fuc/MenuBottom';
import GoUpButton from '../fuc/GoUpButton';
import Insta from '../fuc/Insta';
import HomeAlbums from '../fuc/HomeAlbums';
import TopBanner from '../fuc/TopBanner';
import Banner2 from '../fuc/Banner2';
import BannerA from '../fuc/BannerA';
import Banner3 from '../fuc/Banner3';
import BannerB from '../fuc/BannerB';
import Banner4 from '../fuc/Banner4';
import Banner5 from '../fuc/Banner5';
import HomeAnnouncements from '../fuc/HomeAnnouncements';
import HomeArticle from '../fuc/HomeArticle';
import HomeVideo from '../fuc/HomeVideo';

import moment from 'moment';
import {idToName} from '/client/lib/collectionsRelated';
import {Link} from 'react-router';

export default class SingleShop extends Component {
  componentDidMount() {
    var glide = $(this.cw).find('.slider-sl').glide().data('api_glide');

    $(window).on('keyup', function (key) {
      if (key.keyCode === 13) {
        glide.jump(3, console.log('Wooo!'));
      };
    });

    $(this.cw).find('.slider-sl-arrow').on('click', function() {
      console.log(glide.current());
    });

    const $mapCode = $(this.props.context.state.mapCode);

    $(this.cw).find('.map-wrapper').append($mapCode);
  }

  renderSlidePhotos() {
    const {context} = this.props,
          {state} = context;

    if(!state.photos.length) {
      return [
        <li className="slide-sl" key={0}>
          <img src='/img/places/no-img-place.jpg' width="100%" height="100%" />
        </li>
      ]
    }

    return state.photos.map((photo, index) => {
      return (
        <li className="slide-sl" key={index}>
          <img src={photo} width="100%" height="100%" />
        </li>
      );
    });
  }

  renderDescription() {
    const {context} = this.props,
          {state} = context;

    return state.description.split('\n').map((item, index) => {
      return (
        <p key={index}>{item}</p>
      );
    });
  }

  renderAlbums() {
    const {context} = this.props,
          {state, props} = context,
          {albums, artists} = props;

    return albums
            .filter(album => album.shopId === state._id)
            .map((album, index) => {
              const date = moment.unix(album.date).format('ll').split(' '),
                    dayMonth = `${date[0]} ${date[1]}`,
                    year = `${date[2]}`;

              return (
                <div
                  key={index}
                  className="album_0"
                >
                  <div className="album_1 pha-320 pha-375 pha-544">
                    <Link
                      className="fotograf"
                      to={`/single-album/${album._id}`}
                    >
                      <i className="fa fa-camera" aria-hidden="true" style={{padding: '0 3px 0 0'}} />
                      {idToName(album.artistId, artists)}
                    </Link>
                    <a className="View"> <i className="fa fa-eye" aria-hidden="true" style={{padding: '0 3px 0 0'}} /> 12528</a>
                    <div className="post-entry-meta">
                      <div className="post-entry-meta-category">
                        <span className="label label-date">{dayMonth}</span>
                        <span className="label label-year">{year}</span>
                      </div>
                      <div className="post-entry-meta-title">
                        <h2>
                          <Link to={`/single-album/${album._id}`}>{album.title.toUpperCase()}</Link>
                        </h2>
                      </div>
                      <span className="post-title">{album.subtitle.toUpperCase()}</span>
                    </div>
                    <Link
                      className="album_pic crop"
                      to={`/single-album/${album._id}`}
                      style={{height: '100%', width: '100%', backgroundImage: `url(${album.cover})`}}
                    >
                      <img src='/img/albums/0.gif' />
                    </Link>
                  </div>
                </div>
              );
            });

  }

  render() {
    const {context} = this.props,
          {state} = context,
          {albums} = context.props,
          filteredAlbums = albums.filter(album => album.shopId === state._id);

    return (
      <div className="ALLLL">
        <TopBanner context={context} />
        <Header context={context} />
        <Banner2 context={context} />
        <section className="filter-a">
          <a className="button-a" href="#"> <i className="fa fa-arrow-left" aria-hidden="true" /> </a>
          <ul className="mini-menu">
            <li><a href="#">Заведения <i className="fa fa-angle-down" aria-hidden="true" /> </a>
              <ul className="submenu">
                <li><a href="#">Фотоотчеты</a></li>
                <li><a href="#">Анонсы</a></li>
                <li><a href="#">Статьи</a></li>
                <li><a href="#">Видео</a></li>
                <li><a href="#">Контакты</a></li>
              </ul>
            </li>
          </ul>
          <div className="search-a">
            <ul className="nav">
              <li className="nav-search">
                <form action="#">
                  <input type="text" placeholder="Поиск по заголовку..." style={{width: 500}} />
                  <input type="submit" defaultValue="Найти" />
                </form>
              </li>
            </ul>
          </div>
          <div className="select-menu">
            <ul className="mini-menu-a">
              <li><a>Рестораны<i className="fa fa-angle-down" aria-hidden="true" style={{padding: '0 0 0 5px'}} /> </a>
                <ul className="submenu-a">
                  <li><a href="#" className="submenu-active">Все заведения</a></li>
                  <li><a href="#">Рестораны</a></li>
                  <li><a href="#">Бары</a></li>
                  <li><a href="#">Клубы</a></li>
                  <li><a href="#">Караоке</a></li>
                  <li><a href="#">Летние террасы</a></li>
                  <li><a href="#">Кофейни</a></li>
                  <li><a href="#">Шопинг</a></li>
                  <li><a href="#">Beauty</a></li>
                  <li><a href="#">Гос.Сфера</a></li>
                  <li><a href="#">Дети</a></li>
                  <li><a href="#">Культура</a></li>
                  <li><a href="#">Спорт</a></li>
                  <li><a href="#">Концерты</a></li>
                  <li><a href="#">Авто</a></li>
                  <li><a href="#">Город</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </section>
        <section className="filter-b back_all">
          <a className="button-d" href="#"> <i className="fa fa-arrow-left" aria-hidden="true" /> </a>
          <div className="poisk-a">
            <div className="search-a">
              <ul className="nav">
                <li className="nav-search">
                  <form action="#">
                    <i className="fa fa-search" aria-hidden="true" />
                    <input type="text" placeholder="Поиск по заголовку..." />
                    <input type="submit" defaultValue="Найти" />
                  </form>
                </li>
              </ul>
            </div>
          </div>
          <ul id="accordion" className="accordion">
            <li>
              <div className="link"><i className="fa fa-glass" aria-hidden="true" />Все заведения<i className="fa fa-chevron-down" /></div>
              <ul className="submenu-b">
                <li><a href="#" className="submenu-b-active">Все заведения</a></li>
                <li><a href="#">Рестораны</a></li>
                <li><a href="#">Бары</a></li>
                <li><a href="#">Клубы</a></li>
                <li><a href="#">Караоке</a></li>
                <li><a href="#">Летние террасы</a></li>
                <li><a href="#">Кофейни</a></li>
                <li><a href="#">Шопинг</a></li>
                <li><a href="#">Beauty</a></li>
                <li><a href="#">Гос.Сфера</a></li>
                <li><a href="#">Дети</a></li>
                <li><a href="#">Культура</a></li>
                <li><a href="#">Спорт</a></li>
                <li><a href="#">Концерты</a></li>
                <li><a href="#">Авто</a></li>
                <li><a href="#">Город</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <section
          ref={cw => this.cw = cw}
          className="mbr-section mbr-after-navbar"
          id="pricing-table1-6"
        >
          <div className="mbr-section mbr-section-nopadding mbr-price-table">
            <div className="content_wrapper_2">
              <div className="place_big">
                <div className="place_big_logo">
                  <a className="r-more" href="#"><img src={state.cover} />	</a>
                </div>
                <div className="place_big_name">
                  <a href="#" className="name_name">
                    <span>{state.name}</span>
                    <p>{state.type2.toUpperCase()}</p>
                  </a>
                  <p className="calling">
                    <a href={`tel:${state.phone}`}><i className="fa fa-phone" aria-hidden="true" />
                      {state.phone}
                    </a>
                  </p>
                  <p style={{display: state.address ? '' : "none"}}>
                    <i className="fa fa-map-marker" aria-hidden="true" />
                    {state.address}
                  </p>
                  <p style={{display: state.workHours ? '' : "none"}}>
                    <i className="fa fa-clock-o" aria-hidden="true" />
                    {state.workHours}
                  </p>
                  <p style={{display: state.kitchen ? '' : "none"}}>
                    <i className="fa fa-cutlery" aria-hidden="true" />
                    {state.kitchen}
                  </p>
                  <p style={{display: state.bill ? '' : "none"}}>
                    <i className="fa fa-money" aria-hidden="true" />
                    {state.bill}
                  </p>
                  <p style={{display: state.website ? '' : "none"}}>
                    <i className="fa fa-globe" aria-hidden="true" />
                    {state.website}
                  </p>
                </div>
              </div>
              <div
                className="place_info"
                style={{display: state.menu || state.description ? '' : 'none'}}
              >
                <div className="place_00">
                  <div
                    style={{display: state.menu ? '' : 'none'}}
                    className="place_info_pic"
                  >
                    <a href={state.menu} target="_blank"> <img src={state.cover} /> </a>
                    <a href={state.menu} target="_blank">
                      Открыть / Скачать Меню
                      <i className="fa fa-external-link" aria-hidden="true" />
                    </a>
                  </div>
                  <div
                    style={{display: state.description ? '' : 'none'}}
                    className="place_info_text"
                  >
                    {this.renderDescription()}
                  </div>
                </div>
              </div>
              <div className="place place_full">
                <div className="place_logo">
                  <a className="r-more" href="#"><img src={state.cover} />	</a>
                </div>
                <div className="place_name">
                  <a href="#">
                    <span>{state.name}</span>
                    <p>{state.type2}</p>
                  </a>
                  <p>
                    <span>
                      <i className="fa fa-map-marker" aria-hidden="true" />
                      {state.address}
                    </span>
                    <a href={`tel:${state.phone}`}>
                      <i className="fa fa-phone" aria-hidden="true" />
                      {state.phone}
                    </a>
                  </p>
                </div>
                <div className="place_full_info">
                  <p>
                    <i className="fa fa-clock-o" aria-hidden="true" />
                    {state.workHours}
                  </p>
                  <p>
                    <i className="fa fa-cutlery" aria-hidden="true" />
                    {state.kitchen}
                  </p>
                  <p>
                    <i className="fa fa-money" aria-hidden="true" />
                    {state.bill}
                  </p>
                  <p>
                    <i className="fa fa-globe" aria-hidden="true" />
                    {state.website}
                  </p>
                </div>
              </div>
              <div className="tabs_info1">
                <input className="tabs_info" id="tab1" type="radio" name="tabs" defaultChecked />
                <label className="tabs_info" htmlFor="tab1" title="Интерьр">Итерьер</label>
                <input className="tabs_info" id="tab2" type="radio" name="tabs" />
                <label className="tabs_info" htmlFor="tab2" title="Информация">Информация</label>
                <input className="tabs_info" id="tab3" type="radio" name="tabs" />
                <label className="tabs_info" htmlFor="tab3" title="Карта">Карта</label>
                <input className="tabs_info" id="tab4" type="radio" name="tabs" />
                <label className="tabs_info" htmlFor="tab4" title="Карта">Карта</label>
                <section id="content-tab1">
                  <div className="slider-sl slider-place">
                    <ul className="slides slides-place">
                      {this.renderSlidePhotos()}
                    </ul>
                  </div>
                </section>
                <section id="content-tab2">
                  {this.renderDescription()}
                </section>
                <section id="content-tab3" className="map-wrapper">
                </section>
                <section id="content-tab4">
                  <div className="place_info_pic">
                    <a href={state.menu} target="_blank">
                      <img src="/img/pdf.png" />
                    </a>
                    <a href={state.menu}>
                      Открыть / Скачать Меню
                      <i className="fa fa-external-link" aria-hidden="true" />
                    </a>
                  </div>
                </section>
              </div>
            </div>
            <div className="post_wrapper_dark"><div className="post_min">
                <div className="place_info_left">
                  <div className="head-dark">
                    <h4><i className="fa fa-picture-o" aria-hidden="true" style={{padding: '0 5px 0 0'}} /> Итерьер</h4>
                  </div>
                  <div className="slider-sl slider-place">
                    <ul className="slides slides-place">
                      {this.renderSlidePhotos()}
                    </ul>
                  </div>
                </div>
                <div className="place_info_right">
                  <div className="head-dark">
                    <h4>
                      <i className="fa fa-map-marker" aria-hidden="true" style={{padding: '0 5px 0 0'}} />
                      Карта
                    </h4>
                  </div>
                  <div className="map-wrapper"></div>
                </div>
              </div>
            </div>
            <div
              className="content_wrapper"
              style={{minHeight: '0'}}
            >
              <div className="heading-fancy2 heading-line2">
                <h4>
                  <i className="fa fa-camera-retro" aria-hidden="true" style={{padding: '0 5px 0 0'}} />
                  {filteredAlbums.length ? 'Все фотоотчеты заведения' : 'Нет фоотчетов'}
                </h4>
              </div>
              {this.renderAlbums()}
            </div>
          </div>
          <div
            style={{display: albums.length ? '' : 'none'}}
            className="button_place"
          >
            <p>
              <button className="btn-a">	<i className="fa fa-angle-down" aria-hidden="true" />	Еще	<i className="fa fa-angle-down" aria-hidden="true" />	</button>
            </p>
          </div>
        </section>
        <Banner4 context={context}/>
        <Insta />
        <PartnersCarousel context={context} />
        <Footer context={context} />
        <MenuBottom context={context} />
        <GoUpButton context={context}/>
      </div>
    );
  }
}
