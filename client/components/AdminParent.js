import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import {createContainer} from 'meteor/react-meteor-data';

import SidebarContainer from '../containers/admin/SidebarContainer';

class AdminParent extends Component {

  render() {
    return (
      <div id="wrapper" className="toggled">
        <SidebarContainer pathname={this.props.location.pathname}/>
        <div id="page-content-wrapper">
          <div className="container-fluid">
            {this.props.children}
          </div>
        </div>
      </div>
    )
  }
};

export default createContainer(() => {
    if (!Meteor.user() && !Meteor.loggingIn()) {
        browserHistory.push('/login')
    }

    return {}
}, AdminParent);
