import React, {Component} from 'react';

export default class DateTimePicker extends Component {
  componentDidMount() {
    const {context, stateName} = this.props;

    const $dtp = $(this.dtp);

    $dtp.datetimepicker({
      defaultDate: new Date(),
      locale: 'ru'
    });

    $dtp.on('dp.hide', e => {
      context.setState({[stateName]: e.date.unix()})
    });
  }

  componentDidUpdate() {
    const {context, stateName} = this.props;
    const $dtp = $(this.dtp);

    $dtp.data("DateTimePicker").date(moment.unix(context.state[stateName]));
  }

  render() {
    return (
      <div className="form-group">
        <label>{this.props.label}</label>
        <div className="input-group date" id="datetimepicker2">
          <input
            ref={dtp => this.dtp = dtp}
            type="text"
            className="form-control"
            placeholder="Выберите дату и время"
          />
          <span className="input-group-addon">
            <span className="glyphicon glyphicon-calendar" />
          </span>
        </div>
      </div>
    );
  }
}
