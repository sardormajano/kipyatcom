import React, {Component} from 'react';

export default class Select2 extends Component {
  prepareSelect2() {
    const {context, placeholder, stateName} = this.props;

    this.select2Ready = true;

    $s2 = $(this.s2);

    $s2.select2({
      theme: "bootstrap",
      placeholder: placeholder,
    }).on('select2:close', evt => {
      const value = $(this.s2).val();
      context.setState({
        [stateName]: value
      });
    });
  }

  componentDidMount() {
    const {subscription, subscriptionArgument} = this.props;

    if(!subscription) {
      this.prepareSelect2();
    }
    else {
      Meteor.subscribe(subscription, subscriptionArgument, {
        onReady: this.prepareSelect2.bind(this)
      });
    }
  }

  componentDidUpdate() {
    const {context, stateName} = this.props,
          {state} = context;

    $(this.s2).val(context.state[stateName]).trigger('change');
  }

  renderOptions() {
    return this.props.options.map((item, index) => {
      return (
        <option
          key={index}
          value={item._id}>
            {item.name} {item.surname || ''}
        </option>
      );
    });
  }

  render() {
    const {context, stateName, single, placeholder} = this.props,
          {state} = context;

    return (
      <select
        ref={s2 => this.s2 = s2}
        className={`form-control select2${single ? '' : '-multiple'}`}
        multiple={single ? false : 'multiple'}
        defaultValue={context[stateName] || single ? '-1' : []}
      >
        <option disabled value="-1">{placeholder}</option>
        {this.renderOptions()}
      </select>
    );
  }
}
