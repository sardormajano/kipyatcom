import React, {Component} from 'react';

export default class FormGroup extends Component {
  changeHandler(e) {
    const {context, stateName} = this.props;

    context.setState({[stateName]: e.currentTarget.value});
  }

  render() {
    const errorClass = this.props.error ? 'has-error' : '';

    return (
      <div className={`form-group ${errorClass}`}>
        <label htmlFor={this.props.stateName}>{this.props.label}</label>
        <input
          autoComplete="on"
          name={this.props.stateName}
          onChange={this.changeHandler.bind(this)}
          type={this.props.type || 'text'}
          className="form-control"
          placeholder={`введите ${this.props.label.toLowerCase()}`}
          value={this.props.context.state[this.props.stateName]}
        />
      </div>
    );
  }
}
