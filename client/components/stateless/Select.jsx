import React, {Component} from 'react';
import {idToName} from '../../lib/collectionsRelated';

export default class Select extends Component {
  selectHandler(_id) {
    this.props.context.setState({
      [this.props.stateName]: _id
    });
  }

  renderOptions() {
    return this.props.options.map((item) => {
      return (
        <li key={item._id}>
          <a
            onClick={(e) => {
              e.preventDefault();
              this.selectHandler.call(this, item._id)
            }} href="#">
            {item.name} {item.surname || ''}
          </a>
        </li>
      );
    });
  }

  render() {
    const {context, placeholder, stateName, options} = this.props,
          value = context.state[stateName] ? idToName(context.state[stateName], options) : placeholder;

    return (
      <div className="dropdown">
        <button
          style={{textAlign: 'left'}}
          className="btn btn-default dropdown-toggle col-sm-12"
          type="button"
          id={this.props.stateName}
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="true"
        >
          {`${value} `}
          <span className="caret" />
        </button>
        <ul className="dropdown-menu col-sm-12" aria-labelledby={this.props.stateName}>
          {this.renderOptions()}
        </ul>
      </div>
    );
  }
}
