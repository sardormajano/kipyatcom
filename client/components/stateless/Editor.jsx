import React, {Component} from 'react';

import {setEditorValue, getEditValue} from '/client/lib/general';

export default class Editor extends Component {
  componentDidMount() {
    const {height, defaultValue, id} = this.props;

    $.getScript('/ckeditor/ckeditor.js', () => {
      console.log('replacing');
      this.editor = CKEDITOR.replace(this.ta, {
        height: height || 500
      });

      this.editor.setData(defaultValue);
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.defaultValue !== nextProps.defaultValue;
  }

  componentDidUpdate() {
    console.log('updating');
    if(this.editor)
      this.editor.setData(this.props.defaultValue);
  }

  render() {
    const {context} = this.props;

    return (
      <textarea
        ref={ta => this.ta = ta}
        id={this.props.id}
      />
    );
  }
}
