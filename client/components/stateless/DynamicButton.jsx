import React, {Component} from 'react';

import {Link} from 'react-router';

export default class DynamicButton extends Component {
  render() {
    const {pathnames, pathname} = this.props,
          className = pathnames.includes(pathname) ? 'active' : '';

    return (
      <Link
        to={this.props.to}
        className={className}
      >
        {this.props.children}
      </Link>
    );
  }
}
