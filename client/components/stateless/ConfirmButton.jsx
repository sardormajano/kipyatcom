import React, {Component} from 'react';

export default class ConfirmButton extends Component {
  componentDidMount() {
    const {context} = this.props;
  }

  render() {
    const {
      context,
      itemId,
      functionName
    } = this.props;

    return (
      <button
        ref={cb => this.cb = cb}
        className={`btn btn-danger ${this.props.className}`}
        onClick={e => {
          if(!functionName)
            context.deleteItem.call(context, itemId);

          else {
            context[functionName].call(context);
          }
        }}
      >
        {this.props.children}
      </button>
    );
  }
}
