import React, {Component} from 'react';

export default class TextArea extends Component {
  changeHandler(e) {
    const {context, stateName} = this.props;

    context.setState({[stateName]: e.currentTarget.value});
  }

  render() {
    return (
      <div className="form-group">
        <label htmlFor={this.props.stateName}>{this.props.label}</label>
        <textarea
          name={this.props.stateName}
          onChange={this.changeHandler.bind(this)}
          className="form-control"
          rows="5"
          placeholder={`введите ${this.props.label.toLowerCase()}`}
          value={this.props.context.state[this.props.stateName]}
        />
      </div>
    );
  }
}
