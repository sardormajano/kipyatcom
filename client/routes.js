import React from 'react';

import App from './components/App';

/****************************/
/**CLIENT PART BEGIN**/

import HomeContainer from './containers/client/home/HomeContainer';
import CAlbumsContainer from './containers/client/albums/AlbumsContainer';
import SingleAlbumContainer from './containers/client/single-album/SingleAlbumContainer';
import CShopsContainer from './containers/client/shops/ShopsContainer';
import SingleShopContainer from './containers/client/single-shop/SingleShopContainer';
import CVideosContainer from './containers/client/videos/VideosContainer';
import CAnnouncementsContainer from './containers/client/announcements/AnnouncementsContainer';
import CArticlesContainer from './containers/client/articles/ArticlesContainer';
import SingleArticleContainer from './containers/client/single-article/SingleArticleContainer';
import CContactsContainer from './containers/client/contacts/ContactsContainer';

/****************************/
/**CLIENT PART END**/

/****************************/
/**ADMIN PART BEGIN**/

import AdminParent from './components/AdminParent';

import LoginContainer from './containers/admin/LoginContainer';

//CITIES
import CitiesContainer from './containers/admin/cities/CitiesContainer';

//USERS
import UsersContainer from './containers/admin/users/UsersContainer';
import NewUserContainer from './containers/admin/users/NewUserContainer';
import EditUserContainer from './containers/admin/users/EditUserContainer';

//ARTISTS
import ArtistsContainer from './containers/admin/artists/ArtistsContainer';
import NewArtistContainer from './containers/admin/artists/NewArtistContainer';
import EditArtistContainer from './containers/admin/artists/EditArtistContainer';

//TAGS
import TagsContainer from './containers/admin/tags/TagsContainer';

//ANNOUNCEMENTS
import AnnouncementsContainer from './containers/admin/announcements/AnnouncementsContainer';
import NewAnnouncementContainer from './containers/admin/announcements/NewAnnouncementContainer';
import EditAnnouncementContainer from './containers/admin/announcements/EditAnnouncementContainer';

//ALBUMS
import AlbumsContainer from './containers/admin/albums/AlbumsContainer';
import NewAlbumContainer from './containers/admin/albums/NewAlbumContainer';
import EditAlbumContainer from './containers/admin/albums/EditAlbumContainer';

//SHOPS
import ShopsContainer from './containers/admin/shops/ShopsContainer';
import NewShopContainer from './containers/admin/shops/NewShopContainer';
import EditShopContainer from './containers/admin/shops/EditShopContainer';

//ARTICLES
import ArticlesContainer from './containers/admin/articles/ArticlesContainer';
import NewArticleContainer from './containers/admin/articles/NewArticleContainer';
import EditArticleContainer from './containers/admin/articles/EditArticleContainer';

//VIDEOS
import VideosContainer from './containers/admin/videos/VideosContainer';
import NewVideoContainer from './containers/admin/videos/NewVideoContainer';
import EditVideoContainer from './containers/admin/videos/EditVideoContainer';

//CONTACTS
import ContactsContainer from './containers/admin/contacts/ContactsContainer';

//SLIDER
import SlidesContainer from './containers/admin/slides/SlidesContainer';
import NewSlideContainer from './containers/admin/slides/NewSlideContainer';
import EditSlideContainer from './containers/admin/slides/EditSlideContainer';

//BANNERS
import BannersContainer from './containers/admin/banners/BannersContainer';
import EditBannerContainer from './containers/admin/banners/EditBannerContainer';
import EditModalBannerContainer from './containers/admin/banners/EditModalBannerContainer';

/****************************/
/**ADMIN PART END**/

/**/

import NotFoundContainer from './containers/client/not-found/NotFoundContainer';

/**/

import {Router, Route, IndexRoute, browserHistory} from 'react-router';

export const renderRoutes = () => (
  <Router onUpdate={() => window.scrollTo(0, 0)} history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={HomeContainer}/>
      <Route path="albums" component={CAlbumsContainer} />
      <Route path="announcements" component={CAnnouncementsContainer} />
      <Route path="articles" component={CArticlesContainer} />
      <Route path="contacts" component={CContactsContainer} />;
      <Route path="single-album/:_id" component={SingleAlbumContainer} />
      <Route path="shops" component={CShopsContainer} />
      <Route path="single-shop/:_id" component={SingleShopContainer} />
      <Route path="videos" component={CVideosContainer} />
      <Route path="single-article/:_id" component={SingleArticleContainer} />

      <Route path="login" component={LoginContainer} />
      <Route path="admin" component={AdminParent}>

        {/*USERS*/}
        <Route path="users" component={UsersContainer} />
        <Route path="new-user" component={NewUserContainer} />
        <Route path="edit-user/:_id" component={EditUserContainer} />

        {/*ARTISTS*/}
        <Route path="artists" component={ArtistsContainer} />
        <Route path="new-artist" component={NewArtistContainer} />
        <Route path="edit-artist/:_id" component={EditArtistContainer} />

        {/*TAGS*/}
        <Route path="tags" component={TagsContainer} />

        {/*CITIES*/}
        <Route path="cities" component={CitiesContainer} />

        {/*ANNOUNCEMENTS*/}
        <Route path="announcements" component={AnnouncementsContainer} />
        <Route path="new-announcement" component={NewAnnouncementContainer} />
        <Route path="edit-announcement/:_id" component={EditAnnouncementContainer} />

        {/*SHOPS*/}
        <Route path="shops" component={ShopsContainer} />
        <Route path="new-shop" component={NewShopContainer} />
        <Route path="edit-shop/:_id" component={EditShopContainer} />

        {/*ALBUMS*/}
        <Route path="albums" component={AlbumsContainer} />
        <Route path="new-album" component={NewAlbumContainer} />
        <Route path="edit-album/:_id" component={EditAlbumContainer} />

        {/*ALBUMS*/}
        <Route path="articles" component={ArticlesContainer} />
        <Route path="new-article" component={NewArticleContainer} />
        <Route path="edit-article/:_id" component={EditArticleContainer} />

        {/*VIDEOS*/}
        <Route path="videos" component={VideosContainer} />
        <Route path="new-video" component={NewVideoContainer} />
        <Route path="edit-video/:_id" component={EditVideoContainer} />

        {/*CONTACTS*/}
        <Route path="contacts" component={ContactsContainer} />

        {/*BANNERS*/}
        <Route path="banners" component={BannersContainer} />
        <Route path="edit-banner/:_id" component={EditBannerContainer} />
        <Route path="edit-modal-banner/:_id" component={EditModalBannerContainer} />

        {/*SLIDER*/}
        <Route path="slides" component={SlidesContainer} />
        <Route path="new-slide" component={NewSlideContainer} />
        <Route path="edit-slide/:_id" component={EditSlideContainer} />
      </Route>
      <Route path="*" component={NotFoundContainer} />
    </Route>
  </Router>
);
