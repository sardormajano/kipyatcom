export const PATHNAMES = {
  albums: '1',
  'single-album': '1',
  announcements: '2',
  shops: '3',
  'single-shop': '3',
  articles: '4',
  'single-article': '4',
  videos: '5',
  contacts: '6'
}
