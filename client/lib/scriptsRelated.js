export const qs = document.querySelector.bind(document),
             qsa = document.querySelectorAll.bind(document);

export const addScript = (attrs) => {
    if (qs(`[src='${attrs.src}']`))
        return;

    const script = document.createElement('script');

    script.setAttribute('type', 'text/javascript');

    for (key in attrs) {
        script.setAttribute(key, attrs[key]);
    };

    document.body.appendChild(script);
}

export const removeScript = (script) => {
    script.parentNode.removeChild(script);
}
