export const MOMENT_RU = {
    months: {
        format: "января_февраля_марта_апреля_мая_июня_июля_августа_сентября_октября_ноября_декабря".split("_"),
        standalone: "январь_февраль_март_апрель_май_июнь_июль_август_сентябрь_октябрь_ноябрь_декабрь".split("_")
    },
    monthsShort: {
        // по CLDR именно "июл." и "июн.", но какой смысл менять букву на точку ?
        format: "янв._февр._мар._апр._мая_июня_июля_авг._сент._окт._нояб._дек.".split("_"),
        standalone: "янв._февр._март_апр._май_июнь_июль_авг._сент._окт._нояб._дек.".split("_")
    },
    weekdays: {
        standalone: "воскресенье_понедельник_вторник_среда_четверг_пятница_суббота".split("_"),
        format: "воскресенье_понедельник_вторник_среду_четверг_пятницу_субботу".split("_"),
        isFormat: /\[ ?[Вв] ?(?:прошлую|следующую|эту)? ?\] ?dddd/
    },
    weekdaysShort: "вс_пн_вт_ср_чт_пт_сб".split("_"),
    weekdaysMin: "вс_пн_вт_ср_чт_пт_сб".split("_"),
    // полные названия с падежами, по три буквы, для некоторых, по 4 буквы, сокращения с точкой и без точки
    monthsRegex: /^(январ[ья]|янв\.?|феврал[ья]|февр?\.?|марта?|мар\.?|апрел[ья]|апр\.?|ма[йя]|июн[ья]|июн\.?|июл[ья]|июл\.?|августа?|авг\.?|сентябр[ья]|сент?\.?|октябр[ья]|окт\.?|ноябр[ья]|нояб?\.?|декабр[ья]|дек\.?)/i,
    // копия предыдущего
    monthsShortRegex: /^(январ[ья]|янв\.?|феврал[ья]|февр?\.?|марта?|мар\.?|апрел[ья]|апр\.?|ма[йя]|июн[ья]|июн\.?|июл[ья]|июл\.?|августа?|авг\.?|сентябр[ья]|сент?\.?|октябр[ья]|окт\.?|ноябр[ья]|нояб?\.?|декабр[ья]|дек\.?)/i,
    // полные названия с падежами
    monthsStrictRegex: /^(январ[яь]|феврал[яь]|марта?|апрел[яь]|ма[яй]|июн[яь]|июл[яь]|августа?|сентябр[яь]|октябр[яь]|ноябр[яь]|декабр[яь])/i,
    // Выражение, которое соотвествует только сокращённым формам
    monthsShortStrictRegex: /^(янв\.|февр?\.|мар[т.]|апр\.|ма[яй]|июн[ья.]|июл[ья.]|авг\.|сент?\.|окт\.|нояб?\.|дек\.)/i,
    longDateFormat: {
        LT: "HH:mm",
        LTS: "HH:mm:ss",
        L: "DD.MM.YYYY",
        LL: "D MMMM YYYY г.",
        LLL: "D MMMM YYYY г., HH:mm",
        LLLL: "dddd, D MMMM YYYY г., HH:mm"
    },
    calendar: {
        sameDay: "[Сегодня в] LT",
        nextDay: "[Завтра в] LT",
        lastDay: "[Вчера в] LT",
        nextWeek: function(a) {
            if (a.week() === this.week()) return 2 === this.day() ? "[Во] dddd [в] LT" : "[В] dddd [в] LT";
            switch (this.day()) {
                case 0:
                    return "[В следующее] dddd [в] LT";
                case 1:
                case 2:
                case 4:
                    return "[В следующий] dddd [в] LT";
                case 3:
                case 5:
                case 6:
                    return "[В следующую] dddd [в] LT"
            }
        },
        lastWeek: function(a) {
            if (a.week() === this.week()) return 2 === this.day() ? "[Во] dddd [в] LT" : "[В] dddd [в] LT";
            switch (this.day()) {
                case 0:
                    return "[В прошлое] dddd [в] LT";
                case 1:
                case 2:
                case 4:
                    return "[В прошлый] dddd [в] LT";
                case 3:
                case 5:
                case 6:
                    return "[В прошлую] dddd [в] LT"
            }
        },
        sameElse: "L"
    },
    meridiemParse: /ночи|утра|дня|вечера/i,
    isPM: function(a) {
        return /^(дня|вечера)$/.test(a)
    },
    meridiem: function(a, b, c) {
        return a < 4 ? "ночи" : a < 12 ? "утра" : a < 17 ? "дня" : "вечера"
    },
    ordinalParse: /\d{1,2}-(й|го|я)/,
    ordinal: function(a, b) {
        switch (b) {
            case "M":
            case "d":
            case "DDD":
                return a + "-й";
            case "D":
                return a + "-го";
            case "w":
            case "W":
                return a + "-я";
            default:
                return a
        }
    },
    week: {
        dow: 1, // Monday is the first day of the week.
        doy: 7
    }
};
