export const multipleSubscribe = strArray => {
  strArray.forEach(item => {
    Meteor.subscribe(item);
  });
}
