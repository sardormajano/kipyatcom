import {Meteor} from 'meteor/meteor';
import {render} from 'react-dom';
import {renderRoutes} from './routes.js';
import moment from 'moment';
import {MOMENT_RU} from '/client/lib/locales';

Meteor.startup(function() {
  moment.locale('ru', MOMENT_RU);

  const root = document.createElement('div');
  root.setAttribute('id', 'root');
  document.body.appendChild(root);

  render(renderRoutes(), root);
});
