import React, {Component} from 'react';

import {createContainer} from 'meteor/react-meteor-data';

import Contacts from '/client/components/client/contacts/Contacts';

import {BannersCollection} from '/api/banners';
import {CitiesCollection} from '/api/cities';
import {ContactsCollection} from '/api/contacts';
import {ShopsCollection} from '/api/shops';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class ContactsContainer extends Component {
  componentWillMount() {
    multipleSubscribe([
      'banners',
      'shops.home',
      'contacts',
      'cities'
    ]);
  }

  render() {
    return <Contacts context={this}/>;
  }
}

export default createContainer(() => {
  return {
    banners: BannersCollection.find().fetch(),
    contacts: ContactsCollection.find().fetch(),
    cities: CitiesCollection.find().fetch(),
    shops: ShopsCollection.find().fetch({
        isMain: true,
    })
  };
}, ContactsContainer);
