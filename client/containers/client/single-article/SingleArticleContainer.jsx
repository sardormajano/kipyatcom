import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';

import {AlbumsCollection} from '/api/albums';
import {ArticlesCollection} from '/api/articles';
import {ArtistsCollection} from '/api/artists';
import {BannersCollection} from '/api/banners';
import {ShopTypesCollection} from '/api/shopTypes';
import {ShopsCollection} from '/api/shops';
import {TagsCollection} from '/api/tags';
import {UsersCollection} from '/api/users';

import SingleArticle from '../../../components/client/single-article/SingleArticle';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class SingleArticleContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  componentWillMount() {
    Meteor.subscribe('article', this.props.params._id, {
      onReady: (err) => {
        if(err) {
          console.log(err.reason);
        }
        else {
          const articleData = ArticlesCollection.findOne({_id: this.props.params._id})

          if(!articleData) browserHistory.push('*');

          Meteor.subscribe('albums.including', articleData.albumIds);
          Meteor.subscribe('journalist', articleData.authorId);
          this.setState(articleData);
        }
      }
    });

    multipleSubscribe([
      'artists',
      'banners',
      'shops.home',
      'top.articles',
    ]);

    Meteor.subscribe('tags', 3);
  }

  render() {
    return (
      <SingleArticle context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    articles: ArticlesCollection.find().fetch(),
    albums: AlbumsCollection.find().fetch(),
    artists: ArtistsCollection.find().fetch(),
    banners: BannersCollection.find().fetch(),
    shops: ShopsCollection.find().fetch(),
    shopTypes: ShopTypesCollection.find().fetch(),
    tags: TagsCollection.find().fetch(),
    theJournalist: UsersCollection.findOne({
      roles: ['journalist']
    })
  };
}, SingleArticleContainer);
