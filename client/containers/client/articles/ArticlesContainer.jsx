import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import {ArticlesCollection} from '/api/articles';
import {BannersCollection} from '/api/banners';
import {ShopsCollection} from '/api/shops';
import {TagsCollection} from '/api/tags';

import Articles from '../../../components/client/articles/Articles';

import {multipleSubscribe} from '/client/lib/meteorRelated';
import moment from 'moment';

const angleDown = '<i class="fa fa-angle-down" aria-hidden="true"></i>';
      moreLabel = `${angleDown} Еще ${angleDown}`,
      loadingLabel = `<i class='fa fa-circle-o-notch fa-spin'></i>`;

class ArticlesContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      articlesNumber: 8,
      incrementBy: 8,

      filterDate: '',
      filterWord: '',
      filterTag: '',
      filterArtist: '',

      foundNumber: 0,

      loadMoreLabel: moreLabel,
      articlesReady: false,
    }
  }

  componentDidMount() {
    const {query} = this.props.location;

    if(!Object.keys(query).length)
      return ;

    this.setState(query);
  }

  componentWillMount() {
    Meteor.subscribe('articles.all', this.state.articlesNumber, () => {
      this.setState({articlesReady: true});
    });

    multipleSubscribe([
      'articles.top',
      'banners',
      'shops.home',
    ]);

    Meteor.subscribe('tags', 3);
  }

  search() {
    const {articlesNumber} = this.state,
          filterData = {
            filterDate: this.state.filterDate,
            filterWord: this.state.filterWord,
            filterTag: this.state.filterTag,
          };

    const fDateArray = filterData.filterDate.split('-'),
          theDate = moment([fDateArray[2], parseInt(fDateArray[1]) - 1, fDateArray[0]]);

    filterData.filterDate = theDate.unix();

    if(filterData.filterDate) {
      filterData.nextDay = moment([theDate.year(),  theDate.month(), theDate.date() + 1]).unix(),
      filterData.prevDay = moment([theDate.year(),  theDate.month(), theDate.date() - 1]).unix();
    }

    Meteor.call('article.filtered.count', filterData, (err, res) => {
      if(err)
        console.log(err.reason);
      else {
        this.setState({
          foundNumber: res
        });
      }
    });

    Meteor.subscribe('articles.all', 8, filterData, {
      onReady: () => {
        this.setState({
          articlesNumber: 8,
          articlesReady: true
        });
      }
    });

    this.setState({
      articlesReady: false
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.state.filterWord !== prevState.filterWord
    || this.state.filterDate !== prevState.filterDate
    || this.state.filterTag !== prevState.filterTag) {
      this.search();
    }
  }

  loadMoreHandler() {
    const {articlesNumber, incrementBy} = this.state,
          newArticlesNumber = articlesNumber + incrementBy,
          filterData = {
            filterDate: this.state.filterDate,
            filterWord: this.state.filterWord,
            filterTag: this.state.filterTag,
            filterArtist: this.state.filterArtist,
          };

    const fDateArray = filterData.filterDate.split('-'),
          theDate = moment([fDateArray[2], parseInt(fDateArray[1]) - 1, fDateArray[0]]);

    if(filterData.filterDate) {
      filterData.nextDay = moment([theDate.year(),  theDate.month(), theDate.date() + 1]).unix(),
      filterData.prevDay = moment([theDate.year(),  theDate.month(), theDate.date() - 1]).unix();
    }

    Meteor.subscribe('articles.all', newArticlesNumber, filterData, {
      onReady: () => {
        this.setState({
          loadMoreLabel: moreLabel,
          articlesReady: true
        });
      }
    });

    this.setState({
      loadMoreLabel: loadingLabel,
      articlesReady: false
    });

    Meteor.call('article.filtered.count', filterData, (err, res) => {
      if(err)
        console.log(err.reason);
      else {
        this.setState({
          articlesNumber: newArticlesNumber,
          foundNumber: res
        });
      }
    })
  }

  render() {
    return (
      <Articles context={this} />
    );
  }
}

export default createContainer(() => {
  return {
    articles: ArticlesCollection.find({
      }, {
        sort: {createdAt: -1},
      }).fetch(),
    banners: BannersCollection.find().fetch(),
    shops: ShopsCollection.find().fetch({
        deleted: {$ne: true},
        isMain: true,
    }),
    tags: TagsCollection.find().fetch()
  };
}, ArticlesContainer);
