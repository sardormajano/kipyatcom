import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import {browserHistory} from 'react-router';

import {AlbumsCollection} from '/api/albums';
import {ArtistsCollection} from '/api/artists';
import {BannersCollection} from '/api/banners';
import {ShopTypesCollection} from '/api/shopTypes';
import {ShopsCollection} from '/api/shops';

import SingleAlbum from '../../../components/client/single-album/SingleAlbum';

import {multipleSubscribe} from '/client/lib/meteorRelated';
import {mfIdToLink} from '/client/lib/general';

class SingleAlbumContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: '',
      photos: [],
      shopId: '',
      title: '',
      subtitle: '',
      date: '',
      artistId: '',
      views: '',
      tags: [],
      deleted: false,

      ready: false
    };
  }

  componentWillMount() {
    Meteor.subscribe('album', this.props.params._id, {
      onReady: (err) => {
        if(err) {
          console.log(err.reason);
        }
        else {
          const theAlbum = AlbumsCollection.findOne({_id: this.props.params._id});

          if(!theAlbum) browserHistory.push('*');

          this.setState({
            ready: true,
            ...theAlbum
          });
        }
      }
    });

    multipleSubscribe(['artists', 'banners', 'shops', 'shopTypes']);
  }

  componentDidMount() {
    Meteor.call('album.increment.view', this.props.params._id);
  }

  loadMoreHandler() {
    this.setState({albumsNumber: this.state.albumsNumber + this.state.incrementBy});
  }

  getZipFilesLink(e) {
    e.preventDefault();

    Meteor.call('zip.album', this.props.params._id, (err, result) => {
      if(err) {
        console.log(err.reason);
      }
      else {
        const downloadLink = document.querySelector('.download-link');

        downloadLink.href = `/photodocs/DocsCollection/${result}/original/${result}`;

        if (window.confirm("Вы уверены, что хотите скачать фотоотчет?")) {
          downloadLink.click();
        }
      }
    })
  }

  render() {
    return (
      <SingleAlbum context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    albums: AlbumsCollection.find().fetch(),
    artists: ArtistsCollection.find().fetch(),
    banners: BannersCollection.find().fetch(),
    shops: ShopsCollection.find().fetch(),
    shopTypes: ShopTypesCollection.find().fetch()
  };
}, SingleAlbumContainer);
