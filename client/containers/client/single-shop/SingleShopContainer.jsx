import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';

import SingleShop from '../../../components/client/single-shop/SingleShop';

import {AlbumsCollection} from '/api/albums';
import {ArtistsCollection} from '/api/artists';
import {BannersCollection} from '/api/banners';
import {ShopsCollection} from '/api/shops';
import {ShopTypesCollection} from '/api/shopTypes';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class SingleShopContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: '',
      cover2: '',
      photos: [],
      photosToDelete: [],

      menu: '',
      name: '',
      type2: '',
      address: '',
      phone: '',
      kitchen: '',
      bill: '',
      workHours: '',
      website: '',
      description: '',
      mapCode: '',
      tags: [],
      isMain: false,
      position: '',
    };
  }

  componentWillMount() {
    Meteor.subscribe('shop', this.props.params._id, {
      onReady: (err) => {
        if(err) {
          console.log(err.reason);
        }
        else {
          const theShop = ShopsCollection.findOne({_id: this.props.params._id});

          if(!theShop) browserHistory.push('*')

          this.setState(theShop);
        }
      }
    });

    Meteor.subscribe('albums.of.shop', this.props.params._id);

    multipleSubscribe(['banners', 'tags', 'artists']);
  }

  render() {
    return (
      <SingleShop context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    albums: AlbumsCollection.find({},
      {
        sort: {date: -1}
      }).fetch(),
    artists: ArtistsCollection.find().fetch(),
    banners: BannersCollection.find().fetch(),
    shops: ShopsCollection.find().fetch(),
    shopTypes: ShopTypesCollection.find().fetch()
  };
}, SingleShopContainer);
