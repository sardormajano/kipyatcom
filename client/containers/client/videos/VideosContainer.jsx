import React, {Component} from 'react';

import {createContainer} from 'meteor/react-meteor-data';

import {VideosCollection} from '/api/videos';
import {BannersCollection} from '/api/banners';
import {ShopsCollection} from '/api/shops';
import {TagsCollection} from '/api/tags';

import Videos from '../../../components/client/videos/Videos';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class VideosContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      videosNumber: 6,
      incrementBy: 6,

      filterDate: '',
      filterWord: '',
      filterTag: '',

      loadMoreLabel: 'Еще',
      showModal: false,
    };
  }

  componentWillMount() {
    Meteor.subscribe('videos.all', this.state.videosNumber);
    multipleSubscribe([
      'videos.top',
      'banners',
      'shops.home',
    ]);
    Meteor.subscribe('tags', 4);
  }

  loadMoreHandler() {
    const {videosNumber, incrementBy} = this.state,
          newVideosNumber = videosNumber + incrementBy;

    Meteor.subscribe('videos.all', newVideosNumber, {
      onReady: () => {
        this.setState({loadMoreLabel: 'Еще'});
      }
    });

    this.setState({
      loadMoreLabel: 'Загрузка ...',
      videosNumber: newVideosNumber
    });
  }

  render() {
    return (
      <Videos context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    banners: BannersCollection.find().fetch(),
    videos: VideosCollection.find({
    }, {
      sort: {date: -1},
    }).fetch(),
    shops: ShopsCollection.find().fetch({
      isMain: true,
    }),
    tags: TagsCollection.find().fetch(),
  };
}, VideosContainer);
