import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Announcements from '../../../components/client/announcements/Announcements';

import {AnnouncementsCollection} from '/api/announcements';
import {BannersCollection} from '/api/banners';
import {ShopsCollection} from '/api/shops';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class AnnouncementsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentAnnouncement: ''
    }
  }

  componentWillMount() {
    multipleSubscribe([
      'announcements',
      'banners',
      'shops.home',
    ]);
  }

  render() {
    return (
      <Announcements context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    announcements: AnnouncementsCollection.find({
      }, {
        sort: {createdAt: -1},
      }).fetch(),
    banners: BannersCollection.find().fetch(),
    shops: ShopsCollection.find().fetch({
        deleted: {$ne: true},
        isMain: true,
    }),
  };
}, AnnouncementsContainer);
