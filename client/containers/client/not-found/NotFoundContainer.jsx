import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import NotFound from '../../../components/client/not-found/NotFound';

import {ShopsCollection} from '/api/shops';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class NotFoundContainer extends Component {
  componentWillMount() {
    multipleSubscribe([
      'shops.home',
    ]);
  }

  render() {
    return (
      <NotFound context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    shops: ShopsCollection.find().fetch({
        deleted: {$ne: true},
        isMain: true,
    }),
  };
}, NotFoundContainer);
