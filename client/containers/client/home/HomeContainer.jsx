import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Home from '../../../components/client/home/Home';

import {AlbumsCollection} from '/api/albums';
import {AnnouncementsCollection} from '/api/announcements';
import {ArticlesCollection} from '/api/articles';
import {ArtistsCollection} from '/api/artists';
import {BannersCollection} from '/api/banners';
import {VideosCollection} from '/api/videos';
import {ShopsCollection} from '/api/shops';
import {SlidesCollection} from '/api/slides';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class HomeContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentAnnouncement: '',
      currentVideo: ''
    }
  }

  componentWillMount() {
    multipleSubscribe([
      'artists',
      'albums.main',
      'albums.home',
      'announcements.home',
      'articles.home',
      'banners',
      'shops.home',
      'slides.home',
      'videos.home',
    ]);
  }

  render() {
    return (
      <Home context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    albums: AlbumsCollection.find({
      }, {
        sort: {date: -1},
      }).fetch(),
    announcements: AnnouncementsCollection.find({
        isMain: true
      }, {
        sort: {date: -1},
        limit: 2
      }).fetch(),
    articles: ArticlesCollection.find().fetch({
      isMain: true
    }, {
      sort: {date: -1},
      limit: 1
    }),
    artists: ArtistsCollection.find().fetch(),
    banners: BannersCollection.find().fetch(),
    shops: ShopsCollection.find().fetch({
        deleted: {$ne: true},
        isMain: true,
    }),
    slides: SlidesCollection.find().fetch(),
    videos: VideosCollection.find().fetch()
  };
}, HomeContainer);
