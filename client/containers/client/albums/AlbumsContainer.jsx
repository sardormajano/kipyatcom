import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import {AlbumsCollection} from '/api/albums';
import {ArtistsCollection} from '/api/artists';
import {BannersCollection} from '/api/banners';
import {ShopsCollection} from '/api/shops';
import {TagsCollection} from '/api/tags';

import Albums from '../../../components/client/albums/Albums';

import {multipleSubscribe} from '/client/lib/meteorRelated';
import moment from 'moment';

const angleDown = '<i class="fa fa-angle-down" aria-hidden="true"></i>';
      moreLabel = `${angleDown} Еще ${angleDown}`,
      loadingLabel = `<i class='fa fa-circle-o-notch fa-spin'></i>`;

class AlbumsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      albumsNumber: 8,
      incrementBy: 8,

      filterDate: '',
      filterWord: '',
      filterTag: '',
      filterArtist: '',

      foundNumber: 0,

      loadMoreLabel: moreLabel,
      albumsReady: false,
    };
  }

  componentDidMount() {
    const {query} = this.props.location;

    if(!Object.keys(query).length)
      return ;

    this.setState(query);
  }

  componentWillMount() {
    Meteor.subscribe('albums.all', this.state.albumsNumber, () => {
      this.setState({albumsReady: true});
    });

    multipleSubscribe([
      'albums.top',
      'artists',
      'banners',
      'shops.home',
    ]);
    Meteor.subscribe('tags', 0);
  }

  search() {
    const {albumsNumber} = this.state,
          filterData = {
            filterDate: this.state.filterDate,
            filterWord: this.state.filterWord,
            filterTag: this.state.filterTag,
            filterArtist: this.state.filterArtist,
          };

    const fDateArray = filterData.filterDate.split('-'),
          theDate = moment([fDateArray[2], parseInt(fDateArray[1]) - 1, fDateArray[0]]);

    filterData.filterDate = theDate.unix();

    if(filterData.filterDate) {
      filterData.nextDay = moment([theDate.year(),  theDate.month(), theDate.date() + 1]).unix(),
      filterData.prevDay = moment([theDate.year(),  theDate.month(), theDate.date() - 1]).unix();
    }

    Meteor.call('album.filtered.count', filterData, (err, res) => {
      if(err)
        console.log(err.reason);
      else {
        this.setState({
          foundNumber: res
        });
      }
    });

    Meteor.subscribe('albums.all', 8, filterData, {
      onReady: () => {
        this.setState({
          albumsNumber: 8,
          albumsReady: true
        });
      }
    });

    this.setState({
      albumsReady: false
    });
  }

  loadMoreHandler() {
    const {albumsNumber, incrementBy} = this.state,
          newAlbumsNumber = albumsNumber + incrementBy,
          filterData = {
            filterDate: this.state.filterDate,
            filterWord: this.state.filterWord,
            filterTag: this.state.filterTag,
            filterArtist: this.state.filterArtist,
          };

    const fDateArray = filterData.filterDate.split('-'),
          theDate = moment([fDateArray[2], parseInt(fDateArray[1]) - 1, fDateArray[0]]);

    if(filterData.filterDate) {
      filterData.nextDay = moment([theDate.year(),  theDate.month(), theDate.date() + 1]).unix(),
      filterData.prevDay = moment([theDate.year(),  theDate.month(), theDate.date() - 1]).unix();
    }

    Meteor.subscribe('albums.all', newAlbumsNumber, filterData, {
      onReady: () => {
        this.setState({
          loadMoreLabel: moreLabel,
          albumsReady: true
        });
      }
    });

    this.setState({
      loadMoreLabel: loadingLabel,
      albumsReady: false
    });

    Meteor.call('album.filtered.count', filterData, (err, res) => {
      if(err)
        console.log(err.reason);
      else {
        this.setState({
          albumsNumber: newAlbumsNumber,
          foundNumber: res
        });
      }
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.state.filterWord !== prevState.filterWord
    || this.state.filterDate !== prevState.filterDate
    || this.state.filterTag !== prevState.filterTag
    || this.state.filterArtist !== prevState.filterArtist) {
      this.search();
    }
  }

  render() {
    return (
      <Albums context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    albums: AlbumsCollection.find({
      }, {
        sort: {date: -1},
      }).fetch(),
    artists: ArtistsCollection.find().fetch(),
    banners: BannersCollection.find().fetch(),
    shops: ShopsCollection.find().fetch({
        isMain: true,
    }),
    tags: TagsCollection.find().fetch()
  };
}, AlbumsContainer);
