import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Shops from '../../../components/client/shops/Shops';

import {AlbumsCollection} from '/api/albums';
import {BannersCollection} from '/api/banners';
import {ShopsCollection} from '/api/shops';
import {ShopTypesCollection} from '/api/shopTypes';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class ShopsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      shopsNumber: 10,
      inc: 10
    }
  }

  loadMoreHandler() {
    const {shopsNumber, inc} = this.state;

    Meteor.subscribe('shops.all', shopsNumber);
    this.setState({shopsNumber: shopsNumber + inc});
  }

  componentWillMount() {
    const {shopsNumber, inc} = this.state;

    Meteor.subscribe('shops.all', shopsNumber);

    multipleSubscribe([
      'albums.home',
      'banners',
      'shopTypes',
      'shops.top'
    ]);

    this.setState({shopsNumber: shopsNumber + inc});
  }

  render() {
    return (
      <Shops context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    albums: AlbumsCollection.find({
      }, {
        sort: {date: -1},
        limit: 6
      }).fetch(),
    banners: BannersCollection.find().fetch(),
    shops: ShopsCollection.find({}, {
      sort: {name: 1}
    }).fetch(),
    shopTypes: ShopTypesCollection.find().fetch()
  };
}, ShopsContainer);
