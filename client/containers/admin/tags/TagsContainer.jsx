import React, {Component } from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Tags from '../../../components/admin/tags/Tags';
import {TagsCollection} from '/api/tags';

class TagsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentTag: '',
      currentCategory: 0
    }
  }

  componentWillMount() {
    Meteor.subscribe('tags');
  }

  createTag() {
    const caf = document.querySelector('#create-tag-form'),
          {elements} = caf,
          name = elements['tagNameModal'].value,
          category = this.state.currentCategory;

    Meteor.call('tag.create', {name, category}, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Тэг успешно добавлен!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });

          elements['tagNameModal'].value = '';
      }
    });
  }

  preUpdateTag(e) {
    const _id = e.currentTarget.getAttribute('data-id'),
          {name} = TagsCollection.findOne({_id}),
          {elements} = document.querySelector('#edit-tag-form');

    elements['name'].value = name;

    this.setState({currentTag: _id});
  }

  updateTag(e) {
    e.preventDefault();

    const _id = this.state.currentTag,
          {elements} = document.querySelector('#edit-tag-form'),
          name = elements.name.value;

    Meteor.call('tag.update', _id, {name}, (err) => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Тэг успешно изменен!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });

          elements.name.value = '';
      }
    });
  }

  deleteTag(e) {
    e.preventDefault();

    Meteor.call('tag.delete', e.currentTarget.getAttribute('data-id'), err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Тэг успешно удален!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  render() {
      return (
          <Tags context={this} />
      );
  }
}

export default createContainer( ()=> {
    return {
      tags: TagsCollection.find().fetch()
    };
  },
  TagsContainer);
