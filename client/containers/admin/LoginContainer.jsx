import React, {Component} from 'react';
import {createContainer} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';

import Login from '../../components/admin/Login';

class LoginContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
        }
    }

    loginHandler(e) {
        e.preventDefault();
        const {email, password} = this.state;

        Meteor.loginWithPassword(
            email, password
        , (err) => {
            if(err) {
                Bert.alert({
                    message: err.reason,
                    type: 'danger',
                    style: 'growl-top-right',
                    icon: 'fa-exclamation'
                });
            }
            else {
                Bert.alert({
                    message: 'Добро пожаловать, '+Meteor.user().profile.first_name+'!',
                    type: 'success',
                    style: 'growl-top-right',
                    icon: 'fa-user'
                });
            }
        });
    }

    render() {
        return <Login context={this}/>
    }
}

export default createContainer(() => {
    if (Meteor.user() && !Meteor.loggingIn()) {
        browserHistory.push('/admin/albums');
    }

    return {

    };
}, LoginContainer)
