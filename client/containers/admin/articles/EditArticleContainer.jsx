import React, {Component} from 'react';
import EditArticle from '../../../components/admin/articles/EditArticle';
import {createContainer} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';

import {AlbumsCollection} from '/api/albums';
import {ArticlesCollection} from '/api/articles';
import {ShopsCollection} from '/api/shops';
import {TagsCollection} from '/api/tags';
import {UsersCollection} from '/api/users';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {multipleSubscribe} from '/client/lib/meteorRelated';
import moment from 'moment';

import {getEditorValue, setEditorValue} from '/client/lib/general';

class EditArticleContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: '',
      photo1: '',
      photo2: '',
      photo3: '',
      photo4: '',
      photo5: '',
      albumIds: [],

      shopId: '',
      title: '',
      subtitle: '',
      date: moment().unix(),
      authorId: '',
      customAuthor: '',
      noAuthor: false,
      tags: [],
      videoCode: '',
      isMain: false,
      mainPosition: '',
      isTop: false,
      topPosition: '',

      articleReady: false
    };
  }

  componentWillMount() {
    multipleSubscribe([
      'albums.for.article',
      'article.to.compare.positions',
      'journalists',
      'shop.names'
    ]);

    Meteor.subscribe('tags', 3);

    Meteor.subscribe('article', this.props.params._id, {
      onReady: (err) => {
        if(err) {
          console.log(err.reason);
        }
        else {
          const data = ArticlesCollection.findOne({_id: this.props.params._id});

          data.photos.forEach((photo, index) => data[`photo${index+1}`] = photo);
          delete data.photos;
          setEditorValue('content-start', data.contentStart);
          setEditorValue('content-end', data.contentEnd);

          data.articleReady = true;

          this.setState(data);
        }
      }
    });
  }

  changeCover(currentTarget, stateName, cropperId) {
    const files = currentTarget.files,
          reader = new FileReader();

    reader.onloadend = () => {
      this.setState({[stateName]: reader.result});
      $(`#${cropperId}`).modal({show: true});
    }

    reader.readAsDataURL(files[0]);
  }

  updateArticleHandler(e) {
    e.preventDefault();

    const data = this.state,
          {
            binaryPhoto1, binaryPhoto2,
            binaryPhoto3, binaryPhoto4,
            binaryPhoto5,
            photo1, photo2, photo3,
            photo4, photo5
          } = data;

          data.binaryPhotos = [binaryPhoto1, binaryPhoto2, binaryPhoto3, binaryPhoto4, binaryPhoto5];
          data.photos = [photo1, photo2, photo3, photo4, photo5];
          data.contentStart = getEditorValue('content-start'),
          data.contentEnd = getEditorValue('content-end');

    delete data.binaryPhoto1; delete data.photo1;
    delete data.binaryPhoto2; delete data.photo2;
    delete data.binaryPhoto3; delete data.photo3;
    delete data.binaryPhoto4; delete data.photo4;
    delete data.binaryPhoto5; delete data.photo5;

    data.binaryPhotos.forEach((bPhoto, index) => bPhoto && data.photos.splice(index, 1));
    data.binaryCover && delete data.cover;

    Meteor.call('article.update', this.props.params._id, data, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Статья изменена успешно',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        browserHistory.push('/admin/articles');
      }
    });
  }

  toggleAlbum(_id) {
    const {albumIds} = this.state;
    let newAlbumIds;

    if(albumIds.includes(_id)) {
      newAlbumIds = albumIds.filter(item => item !== _id);
    }
    else {
      newAlbumIds = [...albumIds, _id];
    }

    this.setState({albumIds: newAlbumIds});
  }

  render() {        
    return (
      <EditArticle context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    shops: ShopsCollection.find().fetch(),
    journalists: UsersCollection.find({
      roles: ['journalist']
    }).fetch(),
    tags: TagsCollection.find({category: 3}).fetch(),
    albums: AlbumsCollection.find().fetch(),
    shops: ShopsCollection.find().fetch(),
    articles: ArticlesCollection.find().fetch()
  };
}, EditArticleContainer);
