import React, {Component } from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Articles from '../../../components/admin/articles/Articles';
import {ArticlesCollection} from '/api/articles';
import {ShopsCollection} from '/api/shops';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class ArticlesContainer extends Component {
  componentWillMount() {
    multipleSubscribe(['files.all', 'articles'])
  }

  constructor(props) {
    super(props);

    this.state = {
      currentArticles: undefined
    }
  }

  componentWillMount() {
    multipleSubscribe(['files.all', 'articles', 'shops']);
  }

  toggleVisibility(_id) {
    Meteor.call('article.toggle.visibility', _id, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
        console.log('success');
      }
    });
  }

  deleteItem(_id) {
    Meteor.call('article.delete', _id, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Статья успешно удалена!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });

          this.setState({currentAlbum: ''});
      }
    });
  }

  render() {
    return (
      <Articles context={this} />
    );
  }
}

export default createContainer( ()=> {
  return {
    articles: ArticlesCollection.find().fetch(),
    shops: ShopsCollection.find().fetch(),
  };
},
ArticlesContainer);
