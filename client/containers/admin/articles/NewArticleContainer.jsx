import React, {Component} from 'react';
import NewArticle from '../../../components/admin/articles/NewArticle';
import {createContainer} from 'meteor/react-meteor-data';

import {AlbumsCollection} from '/api/albums';
import {ArticlesCollection} from '/api/articles';
import {ShopsCollection} from '/api/shops';
import {TagsCollection} from '/api/tags';
import {UsersCollection} from '/api/users';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {multipleSubscribe} from '/client/lib/meteorRelated';
import moment from 'moment';

import {setEditorValue, getEditorValue} from '/client/lib/general';

const defaultState = {
  cover: '',
  albumIds: [],
  photo1: '',
  photo2: '',
  photo3: '',
  photo4: '',
  photo5: '',

  shopId: '',
  title: '',
  subtitle: '',
  date: moment().unix(),
  authorId: '',
  customAuthor: '',
  noAuthor: false,
  tags: [],
  videoCode: '',
  isMain: false,
  mainPosition: '',
  isTop: false,
  topPosition: ''
};

class NewArticleContainer extends Component {
  constructor(props) {
    super(props);

    this.state = defaultState;
  }

  componentWillMount() {
    multipleSubscribe([
      'albums.for.article',
      'article.to.compare.positions',
      'journalists',
      'shop.names'
    ]);

    Meteor.subscribe('tags', 3);
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files,
          reader = new FileReader();

    reader.onloadend = () => {
      this.setState({cover: reader.result});
      $('#logo-cropper').modal({show: true});
    }

    reader.readAsDataURL(files[0]);
  }

  changeCover(currentTarget, stateName, cropperId) {
    const files = currentTarget.files,
          reader = new FileReader();

    reader.onloadend = () => {
      this.setState({[stateName]: reader.result});
      $(`#${cropperId}`).modal({show: true});
    }

    reader.readAsDataURL(files[0]);
  }

  createArticleHandler(e) {
    e.preventDefault();

    const data = {...this.state},
          {binaryPhoto1, binaryPhoto2, binaryPhoto3, binaryPhoto4, binaryPhoto5} = data;

    data.binaryPhotos = [binaryPhoto1, binaryPhoto2, binaryPhoto3, binaryPhoto4, binaryPhoto5];
    data.contentStart = getEditorValue('content-start'),
    data.contentEnd = getEditorValue('content-end');

    delete data.binaryPhoto1; delete data.photo1;
    delete data.binaryPhoto2; delete data.photo2;
    delete data.binaryPhoto3; delete data.photo3;
    delete data.binaryPhoto4; delete data.photo4;
    delete data.binaryPhoto5; delete data.photo5;
    delete data.cover;

    Meteor.call('article.create', data, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Статья добавлена успешно',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        this.setState(defaultState);

        setEditorValue('content-start', 'введите начало контента');
        setEditorValue('content-start', 'введите конец контента');
      }
    });
  }

  toggleAlbum(_id) {
    const {albumIds} = this.state;
    let newAlbumIds;

    if(albumIds.includes(_id)) {
      newAlbumIds = albumIds.filter(item => item !== _id);
    }
    else {
      newAlbumIds = [...albumIds, _id];
    }

    this.setState({albumIds: newAlbumIds});
  }

  render() {
    return (
      <NewArticle context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    albums: AlbumsCollection.find().fetch(),
    articles: ArticlesCollection.find().fetch(),
    journalists: UsersCollection.find({
      roles: ['journalist']
    }).fetch(),
    tags: TagsCollection.find({category: 3}).fetch(),
    shops: ShopsCollection.find().fetch()
  };
}, NewArticleContainer);
