import React, {Component } from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Slides from '../../../components/admin/slides/Slides';
import {SlidesCollection} from '/api/slides';
import {ShopsCollection} from '/api/shops';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class SlidesContainer extends Component {
  componentWillMount() {
    multipleSubscribe(['files.all', 'slides'])
  }

  constructor(props) {
    super(props);

    this.state = {
      currentSlides: undefined
    }
  }

  componentWillMount() {
    multipleSubscribe(['files.all', 'slides', 'shops']);
  }

  deleteItem(id) {
    Meteor.call('slide.delete', id, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Фотоальбом успешно удален!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  render() {
    return (
      <Slides context={this} />
    );
  }
}

export default createContainer( ()=> {
  return {
    slides: SlidesCollection.find().fetch(),
    shops: ShopsCollection.find().fetch(),
  };
},
SlidesContainer);
