import React, {Component} from 'react';
import EditSlide from '../../../components/admin/slides/EditSlide';
import {createContainer} from 'meteor/react-meteor-data';

import {browserHistory} from 'react-router';

import {SlidesCollection} from '/api/slides';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {multipleSubscribe} from '/client/lib/meteorRelated';
import moment from 'moment';

class EditSlideContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: '',
      link: ''
    };
  }

  componentWillMount() {
    Meteor.subscribe('slide', this.props.params._id, {
      onReady: (err) => {
        if(err) {
          console.log(err.reason);
        }
        else {
          this.setState(SlidesCollection.findOne({_id: this.props.params._id}));
        }
      }
    });
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover: link});
      $('#logo-cropper').modal({show: true});
    });
  }

  editSlideHandler(e) {
    e.preventDefault();

    Meteor.call('slide.update', this.props.params._id, this.state, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Слайд успешно изменен',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        browserHistory.push('/admin/slides');
      }
    });
  }

  render() {
    return (
      <EditSlide context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
  };
}, EditSlideContainer);
