import React, {Component} from 'react';
import NewSlide from '../../../components/admin/slides/NewSlide';
import {createContainer} from 'meteor/react-meteor-data';

import {ShopsCollection} from '/api/shops';
import {ArtistsCollection} from '/api/artists';
import {TagsCollection} from '/api/tags';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {multipleSubscribe} from '/client/lib/meteorRelated';
import moment from 'moment';

class NewSlideContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: '',
      link: '',
    };
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover: link});
      $('#logo-cropper').modal({show: true});
    });
  }
  
  createSlideHandler(e) {
    e.preventDefault();

    Meteor.call('slide.create', this.state, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Слайд добавлен успешно',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        this.setState({
          cover: '',
          link: '',
        });
      }
    });
  }

  render() {
    return (
      <NewSlide context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
  };
}, NewSlideContainer);
