import React, {Component} from 'react';
import NewVideo from '../../../components/admin/videos/NewVideo';
import {createContainer} from 'meteor/react-meteor-data';

import {VideosCollection} from '/api/videos';
import {UsersCollection} from '/api/users';
import {TagsCollection} from '/api/tags';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {multipleSubscribe} from '/client/lib/meteorRelated';
import moment from 'moment';

class NewVideoContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: '',
      title: '',
      subtitle: '',
      date: moment().unix(),
      authorId: '',
      customAuthor: '',
      noAuthor: false,
      tags: [],
      isMain: false,
      mainPosition: '',
      isTop: false,
      topPosition: '',
      videoCode: '',
      extraLink: ''
    };
  }

  componentWillMount() {
    multipleSubscribe([
      'files.all',
      'authors',
      'videos.to.compare.positions'
    ]);

    Meteor.subscribe('tags', 4);
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files,
          reader = new FileReader();

    reader.onloadend = () => {
      this.setState({cover: reader.result});
      $('#logo-cropper').modal({show: true});
    }

    reader.readAsDataURL(files[0]);
  }

  createVideoHandler(e) {
    e.preventDefault();

    Meteor.call('video.create', this.state, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Видео добавлено успешно',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        this.setState({
          cover: '',
          title: '',
          subtitle: '',
          date: moment().unix(),
          authorId: '',
          customAuthor: '',
          noAuthor: false,
          tags: [],
          isMain: false,
          videoCode: '',
          extraLink: ''
        });
      }
    });
  }

  toggleAlbum(_id) {
    const {albumIds} = this.state;
    let newAlbumIds;

    if(albumIds.includes(_id)) {
      newAlbumIds = albumIds.filter(item => item !== _id);
    }
    else {
      newAlbumIds = [...albumIds, _id];
    }

    this.setState({albumIds: newAlbumIds});
  }

  render() {
    return (
      <NewVideo context={this}/>
    );
  }
}

export default createContainer(() => {
  let userFilter = Meteor.loggingIn() ? {} : {_id: {$ne: Meteor.user()._id}};

  return {
    authors: UsersCollection.find(userFilter).fetch(),
    tags: TagsCollection.find({category: 4}).fetch(),
    videos: VideosCollection.find().fetch()
  };
}, NewVideoContainer);
