import React, {Component} from 'react';
import EditVideo from '../../../components/admin/videos/EditVideo';
import {createContainer} from 'meteor/react-meteor-data';

import {browserHistory} from 'react-router';

import {VideosCollection} from '/api/videos';
import {UsersCollection} from '/api/users';
import {TagsCollection} from '/api/tags';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {multipleSubscribe} from '/client/lib/meteorRelated';
import moment from 'moment';

class EditVideoContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: '',
      title: '',
      subtitle: '',
      date: moment().unix(),
      authorId: '',
      customAuthor: '',
      noAuthor: false,
      tags: [],
      isMain: false,
      mainPosition: '',
      isTop: false,
      topPosition: '',
      videoCode: '',
      extraLink: ''
    };
  }

  componentWillMount() {
    multipleSubscribe([
      'authors',
      'videos.to.compare.positions'
    ]);

    Meteor.subscribe('tags', 4);

    Meteor.subscribe('video', this.props.params._id, {
      onReady: (err) => {
        if(err) {
          console.log(err.reason);
        }
        else {
          this.setState(VideosCollection.findOne({_id: this.props.params._id}));
        }
      }
    });
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover: link});
      $('#logo-cropper').modal({show: true});
    });
  }

  updateVideoHandler(e) {
    e.preventDefault();

    Meteor.call('video.update', this.props.params._id, this.state, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Видео изменено успешно',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        browserHistory.push('/admin/videos');
      }
    });
  }

  render() {
    return (
      <EditVideo context={this}/>
    );
  }
}

export default createContainer(() => {
  let userFilter = Meteor.loggingIn() ? {} : {_id: {$ne: Meteor.user()._id}};

  return {
    authors: UsersCollection.find(userFilter).fetch(),
    tags: TagsCollection.find({category: 4}).fetch(),
    videos: VideosCollection.find().fetch()
  };
}, EditVideoContainer);
