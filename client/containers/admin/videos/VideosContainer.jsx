import React, {Component } from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Videos from '../../../components/admin/videos/Videos';
import {VideosCollection} from '/api/videos';
import {ShopsCollection} from '/api/shops';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class VideosContainer extends Component {
  componentWillMount() {
    multipleSubscribe(['files.all', 'videos'])
  }

  constructor(props) {
    super(props);

    this.state = {
      currentVideos: undefined
    }
  }

  componentWillMount() {
    multipleSubscribe(['videos', 'shops']);
  }

  deleteVideo(id) {
    Meteor.call('video.delete', id, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Видео успешно удалено!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  render() {
    return (
      <Videos context={this} />
    );
  }
}

export default createContainer( ()=> {
  return {
    videos: VideosCollection.find().fetch(),
    shops: ShopsCollection.find().fetch(),
  };
},
VideosContainer);
