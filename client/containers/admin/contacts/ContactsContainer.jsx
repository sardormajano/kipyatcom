import React, {Component } from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Contacts from '../../../components/admin/contacts/Contacts';
import {ContactsCollection} from '/api/contacts';
import {CitiesCollection} from '/api/cities';

import {multipleSubscribe} from '/client/lib/meteorRelated';

const defaultState = {
  currentContact: '',
  cityId: '',
  domain: '',
  phone: '',
  email: '',
  insta: '',
};

class ContactsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = defaultState;
  }

  componentWillMount() {
    multipleSubscribe(['contacts', 'cities']);
  }

  createContact() {
    const data = {...this.state};

    delete data.currentContact;

    Meteor.call('contact.create', data, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Данные успешно сохранены!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });

          this.setState(defaultState);
      }
    });
  }

  preUpdateContact(e) {
    const _id = e.currentTarget.getAttribute('data-id'),
          theCollection = ContactsCollection.findOne({_id});

    this.setState(theCollection);
  }

  updateContact(e) {
    const _id = this.state._id,
          data = {...this.state};

    delete data.currentContact;

    Meteor.call('contact.update', _id, data, (err) => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Данные успешно обновлены!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });

          delete this.state._id;

          this.setState({
            currentContact: '',
            email: '',
            phone: '',
            cityId: '',
          });
      }
    });
  }

  deleteContact(e) {
    e.preventDefault();

    Meteor.call('contact.delete', e.currentTarget.getAttribute('data-id'), err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Город успешно удален!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  render() {
    return (
        <Contacts context={this} />
    );
  }
}

export default createContainer( ()=> {
    return {
      contacts: ContactsCollection.find().fetch(),
      cities: CitiesCollection.find().fetch()
    };
  },
  ContactsContainer);
