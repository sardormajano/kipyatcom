import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import EditShop from '../../../components/admin/shops/EditShop';
import {createContainer} from 'meteor/react-meteor-data';

import {TagsCollection} from '/api/tags';
import {ShopTypesCollection} from '/api/shopTypes';
import {ShopsCollection} from '/api/shops';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class EditShopContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: undefined,
      cover2: '',
      photos: [],
      photosToDelete: [],

      menu: '',
      name: '',
      type2: '',
      address: '',
      phone: '',
      kitchen: '',
      bill: '',
      workHours: '',
      website: '',
      description: '',
      mapCode: '',
      tags: [],
      isMain: false,
      position: '',
    };
  }

  addMenu(e) {
    e.preventDefault();
    this.setState({menu: '...загрузка'});

    const {files} = e.currentTarget;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({menu: link});
    });
  }

  addPhoto(e) {
    e.preventDefault();

    const {files} = e.currentTarget;

    Array.prototype.forEach.call(files, item => {
      mfUploadFile(item, DocsCollection, link => {
        this.setState({photos: [...this.state.photos, link]});
      });
    });
  }

  togglePhoto(photo) {
    const {photosToDelete} = this.state;

    if(photosToDelete.includes(photo)) {
      this.setState({
        photosToDelete: photosToDelete.filter(item => item !== photo)
      });
    } else {
      this.setState({
        photosToDelete: [...photosToDelete, photo]
      });
    }
  }

  removePhotos(e) {

    const {photosToDelete} = this.state;

    this.setState({
      photos: this.state.photos.filter(item => !photosToDelete.includes(item)),
      photosToDelete: []
    });
  }

  toggleAllPhotos(e) {
    e.preventDefault();

    const {photos, photosToDelete} = this.state,
          allSelected = photosToDelete.length === photos.length;

    this.setState({
      photosToDelete: allSelected ? [] : [...photos]
    });
  }

  componentWillMount() {
    multipleSubscribe([
      'files.all',
      'shopTypes',
      'tags',
      'shops'
    ]);

    Meteor.subscribe('shop', this.props.params._id, {
      onReady: (err) => {
        if(err) {
          console.log(err.reason);
        }
        else {
          this.setState(ShopsCollection.findOne({_id: this.props.params._id}));
        }
      }
    });
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files,
          reader = new FileReader();

    reader.onloadend = () => {
      this.setState({cover: reader.result});
      $('#logo-cropper').modal({show: true});
    }

    reader.readAsDataURL(files[0]);
  }

  changeCover2Handler(e) {
    e.preventDefault();

    const files = e.currentTarget.files,
          reader = new FileReader();

    reader.onloadend = () => {
      this.setState({cover2: reader.result});
      $('#logo-cropper2').modal({show: true});
    }

    reader.readAsDataURL(files[0]);
  }

  editShopHandler(e) {
    e.preventDefault();

    Meteor.call('shop.update', this.props.params._id, this.state, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Заведение успешно изменено',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        browserHistory.push('/admin/shops');
      }
    });
  }

  render() {
    if(!this.state._id) {
      return <div>...загрузка</div>;
    }

    return (
      <EditShop context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    files: '',
    shopTypes: ShopTypesCollection.find().fetch(),
    tags: TagsCollection.find({category: 2}).fetch(),
    shops: ShopsCollection.find().fetch()
  };
}, EditShopContainer);
