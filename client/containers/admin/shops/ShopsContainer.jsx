import React, {Component } from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Shops from '../../../components/admin/shops/Shops';
import {ShopsCollection} from '../../../../api/shops';
import {ShopTypesCollection} from '../../../../api/shopTypes';

class ShopsContainer extends Component {
  componentWillMount() {
    Meteor.subscribe('files.all');
    Meteor.subscribe('shopTypes');

    Meteor.subscribe(`shops.by.page`, this.state.page);

    Meteor.call('shops.page.count', (err, result) => {
      if(err) {
        console.log(`error ${err.reason}`);
      }
      else {
        this.setState({pagesCount: result});
      }
    });
  }

  toggleVisibility(_id) {
    Meteor.call('shop.toggle.visibility', _id, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
        console.log('success');
      }
    });
  }

  constructor(props) {
    super(props);

    this.state = {
      currentShops: undefined,
      searchWord: '',
      page: 1,
      pagesCount: 0
    }
  }

  deleteItem(id) {
    Meteor.call('shop.delete', id, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Заведение успешно удалено!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  pageChangeHandler(page) {
    Meteor.subscribe(`shops.by.page`, page);
  }

  render() {
      return (
          <Shops context={this} />
      );
  }
}

export default createContainer( ()=> {
  return {
    shops: ShopsCollection.find({},
      {
        sort: {name: -1}
      }).fetch(),
    shopTypes: ShopTypesCollection.find().fetch()
  };
},
ShopsContainer);
