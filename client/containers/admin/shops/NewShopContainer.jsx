import React, {Component} from 'react';
import NewShop from '../../../components/admin/shops/NewShop';
import {createContainer} from 'meteor/react-meteor-data';

import {ShopsCollection} from '/api/shops';
import {TagsCollection} from '/api/tags';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class NewShopContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: '',
      cover2: '',
      photos: [],
      photosToDelete: [],

      menu: '',
      name: '',
      type2: '',
      address: '',
      phone: '',
      kitchen: '',
      bill: '',
      workHours: '',
      website: '',
      description: '',
      mapCode: '',
      tags: [],
      isMain: false,
      position: '',
    };
  }

  addMenu(e) {
    e.preventDefault();
    this.setState({menu: '...загрузка'});

    const {files} = e.currentTarget;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({menu: link});
    });
  }

  addPhoto(e) {
    e.preventDefault();

    const {files} = e.currentTarget;

    Array.prototype.forEach.call(files, item => {
      mfUploadFile(item, DocsCollection, link => {
        this.setState({photos: [...this.state.photos, link]});
      });
    });
  }

  togglePhoto(photo) {
    const {photosToDelete} = this.state;

    if(photosToDelete.includes(photo)) {
      this.setState({
        photosToDelete: photosToDelete.filter(item => item !== photo)
      });
    } else {
      this.setState({
        photosToDelete: [...photosToDelete, photo]
      });
    }
  }

  removePhotos(e) {

    const {photosToDelete} = this.state;

    this.setState({
      photos: this.state.photos.filter(item => !photosToDelete.includes(item)),
      photosToDelete: []
    });
  }

  toggleAllPhotos(e) {
    e.preventDefault();

    const {photos, photosToDelete} = this.state,
          allSelected = photosToDelete.length === photos.length;

    this.setState({
      photosToDelete: allSelected ? [] : [...photos]
    });
  }

  componentWillMount() {
    Meteor.subscribe('files.all', {
      onReady: () => {
        this.setState({filesReady: true});
      }
    });
    multipleSubscribe([
      'files.all',
      'shops',
      'tags'
    ]);
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover: link});
      $('#logo-cropper').modal({show: true});
    });
  }

  changeCover2Handler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover2: link});
      $('#logo-cropper2').modal({show: true});
    });
  }

  createShopHandler(e) {
    e.preventDefault();

    Meteor.call('shop.create', this.state, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Заведение добавлено успешно',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        this.setState({
          cover: undefined,
          photos: [],

          type: undefined,
          name: '',
          type2: '',
          address: '',
          phone: '',
          kitchen: '',
          bill: '',
          workHours: '',
          website: '',
          description: '',
          mapCode: '',
          tags: [],
          isMain: false,
          errorMessage: ''
        });
      }
    });
  }

  render() {
    return (
      <NewShop context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    shops: ShopsCollection.find().fetch(),
    tags: TagsCollection.find().fetch()
  };
}, NewShopContainer);
