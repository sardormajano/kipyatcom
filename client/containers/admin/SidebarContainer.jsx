import React, {Component } from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Sidebar from '../../components/admin/Sidebar';
import {CitiesCollection} from '../../../api/cities';

class SidebarContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentCity: undefined
    };
  }

  changeCity(cityId) {
    this.setState({currentCity: cityId});
    Meteor.call('user.changeCity', Meteor.userId(), cityId);
    window.location = '/admin';
  }

  componentWillMount() {
    Meteor.subscribe('cities');
  }

  setDefaultCity() {
    if(Meteor.loggingIn())
    {
      setTimeout(this.setDefaultCity.bind(this), 500);
    }
    else {
      this.setState({currentCity: Meteor.user().profile.city});
    }
  }

  componentDidMount() {
    this.setDefaultCity();
  }

  render() {
      const {pathname} = this.props;

      return (
          <Sidebar
            cities={this.props.cities}
            context={this}
            pathname={pathname}
          />
      );
  }
}

export default createContainer( ()=> {
    return {
      cities: CitiesCollection.find({}).fetch()
    };
},
SidebarContainer);
