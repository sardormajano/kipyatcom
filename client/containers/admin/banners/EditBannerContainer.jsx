import React, {Component} from 'react';
import EditBanner from '../../../components/admin/banners/EditBanner';
import {createContainer} from 'meteor/react-meteor-data';

import {BannersCollection} from '/api/banners';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {browserHistory} from 'react-router';

class EditBannerContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      photos: [],
      photosToDelete: [],
      off: false,
      isTop: false,
      links: [],
    };
  }

  componentWillMount() {
    Meteor.subscribe('banner', this.props.params._id, {
      onReady: (err) => {
        if(err) {
          console.log(err.reason);
        }
        else {
          this.setState(BannersCollection.findOne({_id: this.props.params._id}));
        }
      }
    });
  }

  addPhoto(e) {
    e.preventDefault();

    const {files} = e.currentTarget;

    Array.prototype.forEach.call(files, item => {
      mfUploadFile(item, DocsCollection, link => {
        this.setState({photos: [...this.state.photos, link]});
      });
    });
  }

  togglePhoto(photo) {
    const {photosToDelete} = this.state;

    if(photosToDelete.includes(photo)) {
      this.setState({
        photosToDelete: photosToDelete.filter(item => item !== photo)
      });
    } else {
      this.setState({
        photosToDelete: [...photosToDelete, photo]
      });
    }
  }

  removePhotos(e) {

    const {photosToDelete} = this.state,
          newLinks = this.state.links,
          newPhotos = this.state.photos.filter((item, index) => {
            if(photosToDelete.includes(item)) {
              newLinks.splice(index, 1);
              return false;
            }

            return true;
          });

    this.setState({
      photos: newPhotos,
      links: newLinks,
      photosToDelete: []
    });
  }

  toggleAllPhotos(e) {
    e.preventDefault();

    const {photos, photosToDelete} = this.state,
          allSelected = photosToDelete.length === photos.length;

    this.setState({
      photosToDelete: allSelected ? [] : [...photos]
    });
  }

  editBannerHandler(e) {
    e.preventDefault();

    Meteor.call('banner.update', this.props.params._id, this.state, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Баннер успешно изменен',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        browserHistory.push('/admin/banners');
      }
    });
  }

  render() {
    return (
      <EditBanner context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    banners: BannersCollection.find().fetch()
  };
}, EditBannerContainer);
