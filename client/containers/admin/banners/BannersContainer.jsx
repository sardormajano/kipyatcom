import React, {Component } from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Banners from '../../../components/admin/banners/Banners';
import {BannersCollection} from '/api/banners';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class BannersContainer extends Component {
  componentWillMount() {
    multipleSubscribe(['files.all', 'banners'])
  }

  constructor(props) {
    super(props);

    this.state = {
      currentCategory: 0
    }
  }

  componentWillMount() {
    multipleSubscribe(['files.all', 'banners', 'shops']);
  }

  deleteBanner(id) {
    Meteor.call('banner.delete', id, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Баннер успешно удален!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  render() {
    return (
      <Banners context={this} />
    );
  }
}

export default createContainer( ()=> {
  return {
    banners: BannersCollection.find().fetch()
  };
},
BannersContainer);
