import React, {Component } from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Albums from '../../../components/admin/albums/Albums';
import {AlbumsCollection} from '/api/albums';
import {ShopsCollection} from '/api/shops';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class AlbumsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentAlbums: undefined,
      currentAlbum: '',
      searchWord: '',
      page: 1,
      pagesCount: 0,

      latestHandler: ''
    }
  }

  componentWillMount() {
    multipleSubscribe(['shops']);

   this.setState({
     latestHandler: Meteor.subscribe(`albums.by.page`, this.state.page)
   });

    Meteor.call('albums.page.count', (err, result) => {
      if(err) {
        console.log(`error ${err.reason}`);
      }
      else {
        this.setState({pagesCount: result});
      }
    });
  }

  toggleVisibility(_id) {
    Meteor.call('album.toggle.visibility', _id, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
        console.log('success');
      }
    });
  }

  deleteItem(_id) {
    Meteor.call('album.delete', _id, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Фотоальбом успешно удален!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  pageChangeHandler(page) {
    this.state.latestHandler.stop();

    Meteor.subscribe(`albums.by.page`, 'stop', {
      onStop: () => {
        this.setState({
          latestHandler: Meteor.subscribe(`albums.by.page`, this.state.page)
        });
      }
    });
  }

  render() {
    return (
      <Albums context={this} />
    );
  }
}

export default createContainer( ()=> {
  return {
    albums: AlbumsCollection.find({
      deleted: {$ne: true}
    }, {
      sort: {date: -1},
    }).fetch(),
    shops: ShopsCollection.find().fetch(),
  };
},
AlbumsContainer);
