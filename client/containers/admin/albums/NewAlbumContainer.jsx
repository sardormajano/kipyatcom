import React, {Component} from 'react';
import NewAlbum from '../../../components/admin/albums/NewAlbum';
import {createContainer} from 'meteor/react-meteor-data';

import {AlbumsCollection} from '/api/albums';
import {ArtistsCollection} from '/api/artists';
import {ShopsCollection} from '/api/shops';
import {TagsCollection} from '/api/tags';

import {multipleSubscribe} from '/client/lib/meteorRelated';
import {mfUploadFile, mfLinkToId, filesToBinaryStrings} from '/client/lib/general';
import moment from 'moment';

class NewAlbumContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: '',
      photos: [],
      thumbnails: {},
      photosToDelete: [],

      shopId: '',
      title: '',
      subtitle: '',
      date: moment().unix(),
      artistId: '',
      views: '',
      tags: '',
      isMain: false,
      mainPosition: '',
      isTop: false,
      topPosition: '',
      views: 0,
      viewMultiplier: 3,
      hideViews: false,

      buttonLabel: 'Создать альбом',
      uploadedPhotosNumber: 0,
      version: 1
    };
  }

  addPhoto(e) {
    e.preventDefault();

    const {files} = e.currentTarget;

    this.setState({
      photos: files,
      uploadedPhotosNumber: files.length
    });
  }

  componentWillMount() {
    Meteor.subscribe('tags', 0);
    multipleSubscribe([
      'artists',
      'shop.names',
      'albums.to.compare.positions'
    ]);
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files,
          reader = new FileReader();

    reader.onloadend = () => {
      this.setState({cover: reader.result});
      $('#logo-cropper').modal({show: true});
    }

    reader.readAsDataURL(files[0]);
  }

  validateFields() {
    let errorMessages = [];

    if(!this.state.cover) {
      errorMessages.push('Вы не выбрали обложку');
    }

    if(!this.state.shopId) {
      errorMessages.push('Вы не выбрали заведение');
    }

    if(!this.state.title) {
      errorMessages.push('Вы не ввели заголовок');
    }

    if(!this.state.tags) {
      errorMessages.push('Вы не выбрали тэги');
    }

    return errorMessages;
  }

  resetTheForm() {
    $('#create-album-button').html(`Создать альбом`);

    document.querySelector('form#new-album').elements['photos-input'].value = '';

    this.setState({
      cover: '',
      photos: [],
      thumbnails: {},
      previews: {},
      photosToDelete: [],

      shopId: '',
      title: '',
      subtitle: '',
      date: moment().unix(),
      artistId: '',
      views: '',
      tags: '',
      isMain: false,
      mainPosition: '',
      isTop: false,
      topPosition: '',
      views: 0,
      viewMultiplier: 3,
      hideViews: false,

      buttonLabel: 'Добавить фото',
      uploadedPhotosNumber: 0
    });
  }

  createAlbumHandler(e) {
    e.preventDefault();

    const errorMessages = this.validateFields();

    $('#create-album-button').html(`Началась загрузка ...`);

    if(errorMessages.length) {
      Bert.alert({
          message: errorMessages.join('<br/>'),
          type: 'danger',
          style: 'growl-top-right',
          icon: 'fa-user'
      });

      return ;
    }

    const data = this.state,
          context = this;

    delete data.previews;
    delete data.photosToDelete;
    delete data.buttonLabel;
    delete data.uploadedPhotosNumber;

    filesToBinaryStrings(data.photos, binaryStrings => {
      data.photos = [];

      $('#create-album-button').html(`Осталось ${binaryStrings.length} фото`);

      function *uploadAlbum() {
        const id = yield albumCreate(data);

        while(binaryStrings.length) {
          if(binaryStrings.length > 10)
            data.photos = binaryStrings.splice(0, 10);
          else
            data.photos = binaryStrings.splice(0, binaryStrings.length);

          $('#create-album-button').html(`Осталось ${binaryStrings.length} фото`);

          yield albumAddPhotos(id, data.photos);
        }

        Bert.alert({
            message: 'Альбом добавлен успешно',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });
        context.resetTheForm();
      }

      let uploadIter = uploadAlbum();

      uploadIter.next();

      function albumCreate(data) {
        Meteor.call('album.create', data, (err, result) => {
          if(err) {
            Bert.alert({
                message: err.reason,
                type: 'danger',
                style: 'growl-top-right',
                icon: 'fa-exclamation'
            });
          } else {
            uploadIter.next(result);
          }
        });
      }

      function albumAddPhotos(id, photos) {
        Meteor.call('album.add.photos', id, photos, (err, result) => {
          if(err) {
            Bert.alert({
                message: err.reason,
                type: 'danger',
                style: 'growl-top-right',
                icon: 'fa-exclamation'
            });
          } else {
            uploadIter.next();
          }
        });
      }
    });
  }

  render() {
    return (
      <NewAlbum context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    albums: AlbumsCollection.find().fetch(),
    artists: ArtistsCollection.find().fetch(),
    shops: ShopsCollection.find().fetch(),
    tags: TagsCollection.find({category: 0}).fetch()
  };
}, NewAlbumContainer);
