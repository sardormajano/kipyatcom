import React, {Component} from 'react';
import EditAlbum from '../../../components/admin/albums/EditAlbum';
import {createContainer} from 'meteor/react-meteor-data';

import {browserHistory} from 'react-router';

import {AlbumsCollection} from '/api/albums';
import {ArtistsCollection} from '/api/artists';
import {ShopsCollection} from '/api/shops';
import {TagsCollection} from '/api/tags';

import {DocsCollection} from '/api/docs';
import {mfUploadFile, mfUploadFileAndThumbnail, mfLinkToId} from '/client/lib/general';

import {multipleSubscribe} from '/client/lib/meteorRelated';
import moment from 'moment';

class EditAlbumContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: undefined,
      photos: [],
      photosToDelete: [],

      shopId: '',
      title: '',
      subtitle: '',
      date: moment().unix(),
      artistId: undefined,
      views: '',
      tags: [],
      isMain: false,
      mainPosition: '',
      isTop: false,
      topPosition: '',
      ready: false,
      views: 0,
      viewMultiplier: 3,
      hideViews: false,

      buttonLabel: 'Добавить фото',
      version: 1
    };
  }

  validateFields() {
    let errorMessages = [];

    if(!this.state.cover) {
      errorMessages.push('Вы не выбрали обложку');
    }

    if(!this.state.shopId) {
      errorMessages.push('Вы не выбрали заведение');
    }

    if(!this.state.title) {
      errorMessages.push('Вы не ввели заголовок');
    }

    if(!this.state.tags) {
      errorMessages.push('Вы не выбрали тэги');
    }

    return errorMessages;
  }

  addPhoto(e) {
    e.preventDefault();

    const {files} = e.currentTarget;

    if(!files.length)
      return ;

    let   countDown = files.length;

    Array.prototype.forEach.call(files, item => {
      mfUploadFileAndThumbnail(item, DocsCollection, (imageLink) => {
        const photos = [...this.state.photos, {photo: imageLink}];
        let buttonLabel;

        countDown --;

        if(countDown)
          buttonLabel = `Осталось ${countDown} фото.`;
        else
          buttonLabel = 'Добавить фото';

        this.setState({
          photos, buttonLabel
        });
      });
    });

    this.setState({buttonLabel: `Осталось ${countDown} фото.`});
  }

  togglePhoto(photo) {
    const {photosToDelete} = this.state;

    if(photosToDelete.includes(photo)) {
      this.setState({
        photosToDelete: photosToDelete.filter(item => item !== photo)
      });
    } else {
      this.setState({
        photosToDelete: [...photosToDelete, photo]
      });
    }
  }

  removePhotos(e) {
    const {photosToDelete} = this.state,
          {thumbnails} = this.state,
          newPhotos = this.state.photos.filter((item, index) => {
            if(photosToDelete.includes(item)) {
              if(!this.state.version)
                delete thumbnails[mfLinkToId(item)];

              return true;
            }

            return false;
          });

    this.setState({
      photos: this.state.photos.filter(item => !photosToDelete.includes(item)),
      photosToDelete: [],
      thumbnails
    });
  }

  toggleAllPhotos(e) {
    e.preventDefault();

    const {photos, photosToDelete} = this.state,
          allSelected = photosToDelete.length === photos.length;

    this.setState({
      photosToDelete: allSelected ? [] : [...photos]
    });
  }

  componentWillMount() {
    Meteor.subscribe('album', this.props.params._id, {
      onReady: (err) => {
        if(err) {
          console.log(err.reason);
        }
        else {
          this.setState({
            ready: true,
            ...AlbumsCollection.findOne({_id: this.props.params._id})
          });
        }
      }
    });

    Meteor.subscribe('tags', 0);

    multipleSubscribe(['shop.names', 'artists', 'albums.to.compare.positions']);
  }

  changeCoverHandler(e) {
    const files = e.currentTarget.files,
          reader = new FileReader();

    reader.onloadend = () => {
      this.setState({cover: reader.result});
      $('#logo-cropper').modal({show: true});
    }

    reader.readAsDataURL(files[0]);
  }

  editAlbumHandler(e) {
    e.preventDefault();

    const errorMessages = this.validateFields();

    if(errorMessages.length) {
      Bert.alert({
          message: errorMessages.join('<br/>'),
          type: 'danger',
          style: 'growl-top-right',
          icon: 'fa-user'
      });

      return ;
    }

    Meteor.call('album.update', this.props.params._id, this.state, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Альбом успешно изменен',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        browserHistory.push('/admin/albums');
      }
    });
  }

  render() {
    return (
      <EditAlbum context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    albums: AlbumsCollection.find().fetch(),
    artists: ArtistsCollection.find().fetch(),
    shops: ShopsCollection.find().fetch(),
    tags: TagsCollection.find({category: 0}).fetch()
  };
}, EditAlbumContainer);
