import React, {Component} from 'react';
import NewUser from '../../../components/admin/users/NewUser';
import {createContainer} from 'meteor/react-meteor-data';

import {UsersCollection} from '/api/users';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {multipleSubscribe} from '/client/lib/meteorRelated';

const defaultState = {
  cover: '',
  cover2: '',

  name: '',
  surname: '',
  email: '',
  role: '',
  password: '',
  passwordConfirmation: ''
};

class NewUserContainer extends Component {
  constructor(props) {
    super(props);

    this.state = defaultState;
  }

  componentWillMount() {
    multipleSubscribe([
      'users',
    ]);
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover: link});
      $('#logo-cropper').modal({show: true});
    });
  }

  changeCover2Handler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover2: link});
    });
  }

  createUserHandler(e) {
    e.preventDefault();

    if(this.state.password !== this.state.passwordConfirmation) {
      Bert.alert({
          message: 'Пароли не совпадают',
          type: 'danger',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
      });

      return ;
    }

    Meteor.call('user.create', this.state, this.state.role, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Пользователь добавлен успешно',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        this.setState(defaultState);
      }
    });
  }

  render() {
    return (
      <NewUser context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    users: UsersCollection.find().fetch(),
  };
}, NewUserContainer);
