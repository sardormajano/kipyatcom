import React, {Component} from 'react';
import EditUser from '../../../components/admin/users/EditUser';
import {createContainer} from 'meteor/react-meteor-data';

import {UsersCollection} from '/api/users';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {multipleSubscribe} from '/client/lib/meteorRelated';

import {browserHistory} from 'react-router';

const defaultState = {
  cover: '',
  cover2: '',

  name: '',
  surname: '',
  email: '',
  role: '',
  password: 'no-password',
  passwordConfirmation: 'no-password'
};

class EditUserContainer extends Component {
  constructor(props) {
    super(props);

    this.state = defaultState;
  }

  componentWillMount() {
    Meteor.subscribe('user', this.props.params._id, {
      onReady: (err) => {
        if(err) {
          console.log(err.reason);
        }
        else {
          const theUser = UsersCollection.findOne({_id: this.props.params._id});
          this.setState({
            name: theUser.profile.first_name,
            surname: theUser.profile.last_name,
            email: theUser.emails[0].address,
            role: theUser.roles[0],
            cover: theUser.profile.cover,
            cover2: theUser.profile.cover2,
          });
        }
      }
    });
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover: link});
      $('#logo-cropper').modal({show: true});
    });
  }

  changeCover2Handler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover2: link});
    });
  }

  updateUserHandler(e) {
    e.preventDefault();

    if(this.state.password !== this.state.passwordConfirmation) {
      Bert.alert({
          message: 'Пароли не совпадают',
          type: 'danger',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
      });

      return ;
    }

    Meteor.call('user.update', this.props.params._id, this.state, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Пользователь успешно изменен',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        browserHistory.push('/admin/users');
      }
    });
  }

  render() {
    return (
      <EditUser context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    users: UsersCollection.find().fetch(),
  };
}, EditUserContainer);
