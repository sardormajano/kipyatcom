import React, {Component } from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Users from '../../../components/admin/users/Users';
import {UsersCollection} from '/api/users';

class UsersContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: undefined,
      usersReady: false,

      email: '',
      password: '',
      confirm: '',
      first_name: '',
      last_name: '',
      role: undefined,
    }
  }

  componentWillMount() {
    Meteor.subscribe('users', {
      onReady: () => {
        this.setState({usersReady: true});
      }
    });

    Meteor.subscribe('roles');
  }

  createUser() {
    if(this.state.password !== this.state.confirm) {
      Bert.alert({
          message: 'Пароли не совпадают',
          type: 'danger',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
      });

      return ;
    }

    Meteor.call('user.create', {
      username: this.state.email,
      email: this.state.email,
      password: this.state.password,
      profile: {
        first_name: this.state.first_name,
        last_name: this.state.last_name
      }
    }, this.state.role, err => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Пользователь успешно добавлен!',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        this.setState({
          email: '',
          password: '',
          confirm: '',
          first_name: '',
          last_name: '',
          role: undefined,
        });
      }
    });
  }

  preUpdateUser(e) {
    const _id = e.currentTarget.getAttribute('data-id'),
          theUser = UsersCollection.findOne({_id});

    this.setState({
      currentUser: _id,
      email: theUser.emails[0].address,
      password: '',
      confirm: '',
      first_name: theUser.profile.first_name,
      last_name: theUser.profile.last_name,
      role: theUser.roles[0],
    });
  }

  updateUser(e) {
    e.preventDefault();

    if(this.state.password !== this.state.confirm) {
      Bert.alert({
          message: 'Пароли не совпадают',
          type: 'danger',
          style: 'growl-top-right',
          icon: 'fa-exclamation'
      });

      return ;
    }

    const _id = this.state.currentUser;

    Meteor.call('user.update', _id, {
      email: this.state.email,
      password: this.state.password,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      role: this.state.role
    }, (err) => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Данные пользователя успешно изменены!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  deleteUser(e) {
    e.preventDefault();

    Meteor.call('user.delete', e.currentTarget.getAttribute('data-id'), err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Пользователь успешно удален!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  render() {
    if(!this.state.usersReady)
      return <div>...загружаю пользователей</div>;

    return (
        <Users context={this} />
    );
  }
}

export default createContainer( ()=> {
  return {
    users: UsersCollection.find().fetch(),
    roles: Roles.getAllRoles().fetch()
  };
}, UsersContainer);
