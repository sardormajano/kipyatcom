import React, {Component } from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Announcements from '../../../components/admin/announcements/Announcements';
import {AnnouncementsCollection} from '/api/announcements';
import {ShopsCollection} from '/api/shops';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class AnnouncementsContainer extends Component {
  componentWillMount() {
    multipleSubscribe(['files.all', 'announcements'])
  }

  constructor(props) {
    super(props);

    this.state = {
      currentAnnouncements: undefined
    }
  }

  componentWillMount() {
    multipleSubscribe(['files.all', 'announcements', 'shops']);
  }

  deleteAnnouncement(id) {
    Meteor.call('announcement.delete', id, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Анонс успешно удален!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  render() {
    return (
      <Announcements context={this} />
    );
  }
}

export default createContainer( ()=> {
  return {
    announcements: AnnouncementsCollection.find().fetch(),
    shops: ShopsCollection.find().fetch()
  };
},
AnnouncementsContainer);
