import React, {Component} from 'react';
import NewAnnouncement from '../../../components/admin/announcements/NewAnnouncement';
import {createContainer} from 'meteor/react-meteor-data';

import {AnnouncementsCollection} from '/api/announcements';
import {ArtistsCollection} from '/api/artists';
import {ShopsCollection} from '/api/shops';
import {TagsCollection} from '/api/tags';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {multipleSubscribe} from '/client/lib/meteorRelated';
import moment from 'moment';

import {setEditorValue, getEditorValue} from '/client/lib/general';

class NewAnnouncementContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: '',

      shopId: undefined,
      title: '',
      subtitle: '',
      date: moment().unix(),
      linkName: 'купить билет',
      link: '',
      hasContent: false,
      mainPhoto: '',
      author: '',
      content: '',
      tags: [],
      isMain: false,
      mainPosition: '',
      isTop: false,
      topPosition: '',
    };
  }

  addPhoto(e) {
    e.preventDefault();

    const {files} = e.currentTarget;

    Array.prototype.forEach.call(files, item => {
      mfUploadFile(item, DocsCollection, link => {
        this.setState({photos: [...this.state.photos, link]});
      });
    });
  }

  componentWillMount() {
    multipleSubscribe([
      'announcements',
      'artists',
      'files.all',
      'shops',
      'tags'
    ]);
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover: link});
      $('#logo-cropper').modal({show: true});
    });
  }

  addMainPhoto(e) {
    e.preventDefault();

    const file = e.currentTarget.files[0];

    if(!file) return;

    mfUploadFile(file, DocsCollection, link => {
      this.setState({mainPhoto: link});
    });
  }

  createAnnouncementHandler(e) {
    e.preventDefault();

    const data = {...this.state};

    data.content = getEditorValue('content');

    Meteor.call('announcement.create', data, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Альбом добавлен успешно',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        this.setState({
          cover: '',
          shopId: undefined,
          title: '',
          subtitle: '',
          date: moment().unix(),
          link: '',
          hasContent: false,
          mainPhoto: '',
          author: '',
          content: '',
          tags: [],
          isMain: false,
          mainPosition: '',
          isTop: false,
          topPosition: '',
        });

        setEditorValue('content', 'введите контент');
      }
    });
  }

  render() {
    return (
      <NewAnnouncement context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    announcements: AnnouncementsCollection.find().fetch(),
    artists: ArtistsCollection.find().fetch(),
    shops: ShopsCollection.find().fetch(),
    tags: TagsCollection.find({category: 1}).fetch()
  };
}, NewAnnouncementContainer);
