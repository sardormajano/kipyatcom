import React, {Component} from 'react';
import EditAnnouncement from '../../../components/admin/announcements/EditAnnouncement';
import {createContainer} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';

import {AnnouncementsCollection} from '/api/announcements';
import {ShopsCollection} from '/api/shops';
import {ArtistsCollection} from '/api/artists';
import {TagsCollection} from '/api/tags';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {multipleSubscribe} from '/client/lib/meteorRelated';
import moment from 'moment';

import {getEditorValue} from '/client/lib/general';

class EditAnnouncementContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: undefined,

      shopId: undefined,
      title: '',
      subtitle: '',
      date: moment().unix(),
      linkName: 'купить билет',
      link: '',
      hasContent: false,
      mainPhoto: undefined,
      author: '',
      tags: [],
      isMain: false,
      mainPosition: '',
      isTop: false,
      topPosition: '',
    };
  }

  addPhoto(e) {
    e.preventDefault();

    const {files} = e.currentTarget;

    Array.prototype.forEach.call(files, item => {
      mfUploadFile(item, DocsCollection, link => {
        this.setState({photos: [...this.state.photos, link]});
      });
    });
  }

  componentWillMount() {
    multipleSubscribe([
      'announcements',
      'artists',
      'files.all',
      'shops',
      'tags',
    ]);

    Meteor.subscribe('announcement', this.props.params._id, {
      onReady: (err) => {
        if(err) {
          console.log(err.reason);
        }
        else {
          this.setState(AnnouncementsCollection.findOne({_id: this.props.params._id}));
        }
      }
    });
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover: link});
      $('#logo-cropper').modal({show: true});
    });
  }

  addMainPhoto(e) {
    e.preventDefault();

    const file = e.currentTarget.files[0];

    if(!file) return;

    mfUploadFile(file, DocsCollection, link => {
      this.setState({mainPhoto: link});
    });
  }

  editAnnouncementHandler(e) {
    e.preventDefault();

    const data = {...this.state};

    data.content = getEditorValue('content');

    Meteor.call('announcement.update', this.props.params._id, data, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Альбом успешно изменен',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        browserHistory.push('/admin/announcements');
      }
    });
  }

  render() {
    return (
      <EditAnnouncement context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    announcements: AnnouncementsCollection.find().fetch(),
    artists: ArtistsCollection.find().fetch(),
    shops: ShopsCollection.find().fetch(),
    tags: TagsCollection.find({category: 1}).fetch()
  };
}, EditAnnouncementContainer);
