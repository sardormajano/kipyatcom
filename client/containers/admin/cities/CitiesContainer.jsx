import React, {Component } from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Cities from '../../../components/admin/cities/Cities';
import {CitiesCollection} from '/api/cities';

class CitiesContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentCity: undefined
    }
  }

  componentWillMount() {
    Meteor.subscribe('cities');
  }

  createCity() {
    const caf = document.querySelector('#create-city-form'),
          {elements} = caf,
          name = elements['cityNameModal'].value,
          domainPrefix = elements['domainPrefixModal'].value;

    Meteor.call('city.create', {name, domainPrefix}, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Данные успешно сохранены!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  preUpdateCity(e) {
    const _id = e.currentTarget.getAttribute('data-id'),
          {name} = CitiesCollection.findOne({_id}),
          {elements} = document.querySelector('#edit-city-form');

    elements['name'].value = name;
    this.setState({currentCity: _id});
  }

  updateCity(e) {
    e.preventDefault();

    const _id = this.state.currentCity,
          {elements} = document.querySelector('#edit-city-form'),
          name = elements.name.value,
          domainPrefix = elements.domainPrefix.value;

    Meteor.call('city.update', _id, {name, domainPrefix}, (err) => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Данные успешно обновлены!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  deleteCity(e) {
    e.preventDefault();

    Meteor.call('city.delete', e.currentTarget.getAttribute('data-id'), err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Город успешно удален!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  render() {
      return (
          <Cities context={this} />
      );
  }
}

export default createContainer( ()=> {
    return {
      cities: CitiesCollection.find().fetch()
    };
  },
  CitiesContainer);
