import React, {Component} from 'react';
import EditArtist from '../../../components/admin/artists/EditArtist';
import {createContainer} from 'meteor/react-meteor-data';

import {ArtistsCollection} from '/api/artists';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {browserHistory} from 'react-router';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class EditArtistContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: '',
      cover2: '',

      name: '',
      surname: '',
      ready: false
    };
  }

  componentWillMount() {
    Meteor.subscribe('artist', this.props.params._id, {
      onReady: (err) => {
        if(err) {
          console.log(err.reason);
        }
        else {
          this.setState({
            ready: true,
            ...ArtistsCollection.findOne({_id: this.props.params._id})
          });
        }
      }
    });
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover: link});
      $('#logo-cropper').modal({show: true});
    });
  }

  changeCover2Handler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover2: link});
    });
  }

  editArtistHandler(e) {
    e.preventDefault();

    Meteor.call('artist.update', this.props.params._id, this.state, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Фотограф успешно изменен',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        browserHistory.push('/admin/artists');
      }
    });
  }

  render() {
    if(!this.state.ready) {
      return <div></div>;
    }
    return (
      <EditArtist context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
    artists: ArtistsCollection.find().fetch(),
  };
}, EditArtistContainer);
