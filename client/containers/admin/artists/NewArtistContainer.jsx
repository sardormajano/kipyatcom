import React, {Component} from 'react';
import NewArtist from '../../../components/admin/artists/NewArtist';
import {createContainer} from 'meteor/react-meteor-data';

import {DocsCollection} from '/api/docs';
import {mfUploadFile} from '/client/lib/general';

import {multipleSubscribe} from '/client/lib/meteorRelated';

class NewArtistContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: '',
      cover2: '',

      name: '',
      surname: ''
    };
  }

  changeCoverHandler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover: link});
      $('#logo-cropper').modal({show: true});
    });
  }

  changeCover2Handler(e) {
    e.preventDefault();

    const files = e.currentTarget.files;

    if(!files.length) return;

    mfUploadFile(files[0], DocsCollection, link => {
      this.setState({cover2: link});
    });
  }

  createArtistHandler(e) {
    e.preventDefault();

    Meteor.call('artist.create', this.state, (err) => {
      if(err) {
        Bert.alert({
            message: err.reason,
            type: 'danger',
            style: 'growl-top-right',
            icon: 'fa-exclamation'
        });
      }
      else {
        Bert.alert({
            message: 'Фотограф добавлен успешно',
            type: 'success',
            style: 'growl-top-right',
            icon: 'fa-user'
        });

        this.setState({
          cover: '',
          cover2: '',

          name: '',
          surname: ''
        });
      }
    });
  }

  render() {
    return (
      <NewArtist context={this}/>
    );
  }
}

export default createContainer(() => {
  return {
  };
}, NewArtistContainer);
