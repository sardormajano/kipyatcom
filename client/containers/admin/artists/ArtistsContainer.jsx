import React, {Component } from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import Artists from '../../../components/admin/artists/Artists';
import {ArtistsCollection} from '/api/artists';

class ArtistsContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentArtist: undefined
    }
  }

  componentWillMount() {
    Meteor.subscribe('artists');
  }

  createArtist() {
    const caf = document.querySelector('#create-artist-form'),
          {elements} = caf,
          name = elements['artistNameModal'].value,
          surname = elements['artistSurnameModal'].value;

    Meteor.call('artist.create', {name, surname}, err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Данные успешно сохранены!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  preUpdateArtist(e) {
    const _id = e.currentTarget.getAttribute('data-id'),
          {name, surname} = ArtistsCollection.findOne({_id}),
          {elements} = document.querySelector('#edit-artist-form');

    elements['name'].value = name;
    elements['surname'].value = surname;

    this.setState({currentArtist: _id});
  }

  updateArtist(e) {
    e.preventDefault();

    const _id = this.state.currentArtist,
          {elements} = document.querySelector('#edit-artist-form'),
          name = elements.name.value,
          surname = elements.surname.value;

    Meteor.call('artist.update', _id, {name, surname}, (err) => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Данные успешно обновлены!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  deleteArtist(e) {
    e.preventDefault();

    Meteor.call('artist.delete', e.currentTarget.getAttribute('data-id'), err => {
      if(err) {
          Bert.alert({
              message: err.reason,
              type: 'danger',
              style: 'growl-top-right',
              icon: 'fa-exclamation'
          });
      }
      else {
          Bert.alert({
              message: 'Пользователь успешно удален!',
              type: 'success',
              style: 'growl-top-right',
              icon: 'fa-user'
          });
      }
    });
  }

  render() {
      return (
          <Artists context={this} />
      );
  }
}

export default createContainer( ()=> {
    return {
      artists: ArtistsCollection.find().fetch()
    };
  },
  ArtistsContainer);
