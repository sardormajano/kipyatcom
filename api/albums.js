import { Mongo } from 'meteor/mongo';
import convert from 'cyrillic-to-latin';
import sharp from 'meteor/c9s:sharp';
import fs from 'fs';
import {DocsCollection} from '/api/docs';
import {CitiesCollection} from '/api/cities';

export const AlbumsCollection = new Mongo.Collection('albums');

const mfIdTolink = _id => {
  return `/photodocs/DocsCollection/${_id}/original/${_id}.jpg`;
}

const mfLinkToId = link => {
  return link.split('/')[3];
}

if (Meteor.isServer) {

  const createThumbnail = (fileName, ref, callback) => {
    DocsCollection.write(new Buffer('hello', 'binary'), {
      fileName: fileName.replace('.jpg', '-thumbnail.jpg'),
      type: 'image/jpeg'
    }, (error, fileRef) => {
      sharp(`${ref.path}`)
        .resize(200)
        .toFile(`${fileRef.path}`);

      callback(fileRef._id);
    });
  }

  Meteor.publish('albums', function(){
    return AlbumsCollection.find({deleted: {$ne: true}});
  });

  Meteor.publish('albums.including', function(albumIds){
    return AlbumsCollection.find({
      deleted: {$ne: true}, hidden: {$ne: true},
      _id: {$in: albumIds}
    }, {
      fields: {
        date: 1,
        title: 1,
        subtitle: 1,
        artistId: 1,
        views: 1,
        cover: 1
      }
    });
  });

  Meteor.publish('albums.main', function(cityId){
    return AlbumsCollection.find({
        deleted: {$ne: true}, hidden: {$ne: true},
        isMain: true
      },
      {
        fields: {
          photos: 0,
          thumbnails: 0,
          previews: 0
        },
        sort: {date: -1},
        limit: 6
      }
    );
  });

  Meteor.publish('albums.for.article', function() {
    return AlbumsCollection.find({
      deleted: {$ne: true}, hidden: {$ne: true}
    },{
      fields: {
        cover: 1,
        title: 1,
        subtitle: 1,
        shopId: 1,
        date: 1,
      },
      sort: {date: -1},
    });
  });

  Meteor.publish('albums.top', function(cityId){
    return AlbumsCollection.find({
        deleted: {$ne: true}, hidden: {$ne: true},
        isTop: true
      },
      {
        fields: {
          photos: 0,
          thumbnails: 0,
          previews: 0
        },
        sort: {date: -1},
        limit: 6
      }
    );
  });

  Meteor.publish('albums.home', function(cityId){
    return AlbumsCollection.find({
        deleted: {$ne: true}, hidden: {$ne: true}
      },
      {
        fields: {
          photos: 0,
          thumbnails: 0,
          previews: 0
        },
        sort: {date: -1},
        limit: 6
      }
    );
  });

  Meteor.publish('album', function(_id){
    return AlbumsCollection.find({
      deleted: {$ne: true}, _id
    }, {
      fields: {
        previews: 0,
        photosToDelete: 0
      }
    });
  });

  Meteor.publish('albums.all', (number, filterData, cityId) => {
    if(!filterData) {
      return AlbumsCollection.find({
        deleted: {$ne: true}, hidden: {$ne: true},
        isTop: {$ne: true}
      }, {
        fields: {
          photos: 0,
          thumbnails: 0,
          previews: 0
        },
        sort: {date: -1},
        limit: number
      });
    }
    else {
      const filter = {};

      filter.deleted = {$ne: true};
      filter.isTop = {$ne: true};

      if(filterData.filterDate) {
        filter.date = {$gte: filterData.prevDay, $lt: filterData.nextDay}
      }

      if(filterData.filterWord) {
        filter['$or'] =
          [
            { title: {$regex: filterData.filterWord, $options: 'i' } },
            { subtitle: {$regex: filterData.filterWord, $options: 'i' } }
          ]
      }

      if(filterData.filterTag) {
        filter.tags = filterData.filterTag;
      }

      if(filterData.filterArtist) {
        filter.artistId = filterData.filterArtist;
      }

      return AlbumsCollection.find(filter, {
        fields: {
          photos: 0,
          thumbnails: 0,
          previews: 0
        },
        sort: {date: -1},
        limit: number
      });
    }
  });

  Meteor.publish('albums.of.shop', (_id) => {
    return AlbumsCollection.find({
      deleted: {$ne: true}, hidden: {$ne: true},
      shopId: _id
    },
    {
      fields: {
        photos: 0,
        thumbnails: 0,
        previews: 0
      },
      sort: {date: -1}
    });
  });

  Meteor.publish('albums.by.page', function(page){
    if(page === 'stop') {
      this.stop();
      return ;
    }

    const pageSize = 16,
          theUser = Meteor.users.findOne(this.userId),
          theCity = CitiesCollection.findOne(theUser.profile.city);

    if(theCity.domainPrefix === 'astana') {
      return AlbumsCollection.find({
          deleted: {$ne: true},
          $or: [
            {cityId: theUser.profile.city},
            {cityId: {$exists: false}}
          ],
        },
        {
          fields: {
            title: 1,
            subtitle: 1,
            shopId: 1,
            cover: 1,
            date: 1,
            hidden: 1,
          },
          sort: {date: -1},
          skip: pageSize * (page - 1),
          limit: pageSize
        }
      );
    }

    return AlbumsCollection.find({
        deleted: {$ne: true},
        cityId: theUser.profile.city,
      },
      {
        fields: {
          title: 1,
          subtitle: 1,
          shopId: 1,
          cover: 1,
          date: 1,
        },
        sort: {date: -1},
        skip: pageSize * (page - 1),
        limit: pageSize
      }
    );
  });

  Meteor.publish('albums.to.compare.positions', function() {
    return AlbumsCollection.find({deleted: {$ne: true}, hidden: {$ne: true}}, {
      fields: {
        mainPosition: 1,
        title: 1,
        topPosition: 1,
      },
    });
  });

  Meteor.methods({
    'album.delete'(_id) {
      AlbumsCollection.update({_id}, { $set: {deleted: true}});
    },
    'album.toggle.visibility'(_id) {
      const theAlbum = AlbumsCollection.findOne(_id);

      AlbumsCollection.update({_id}, { $set: {hidden: !theAlbum.hidden}});
    },
    'album.update'(_id, data) {
      const suffix = parseInt(new Date().getTime() / 1000);

      delete data.previews;

      data.titleLatin = convert(`${data.title}_${suffix}`.replace(' ', '_'));

      const fileBuffer = new Buffer(data.binaryCover, 'binary');

      DocsCollection.write(fileBuffer, {
        fileName: `cover-${_id}`,
        type: 'image/jpeg'
      }, (err, fileRef) => {

        data._id = _id;
        data.cover = mfIdTolink(fileRef._id);

        delete data.binaryCover;
        delete data.previews;

        let countDown = data.photos.length;
        const newPhotos = [];

        Array.prototype.forEach.call(data.photos, (photo, index) => {
          if(!photo.photo)
          {
            newPhotos.push(photo);
            countDown--;
            if(!countDown) {
              data.photos = newPhotos;

              AlbumsCollection.update({_id},
                {
                  $set: data,
                }
              );
            }
            return ;
          }

          const photoName = `${data.titleLatin}_${index}.jpg`,
                fileRef = {path: `/photodocs/${mfLinkToId(photo.photo)}.jpg`};

          createThumbnail(photoName, fileRef, thumbnailId => {
              newPhotos.push({
                photo: photo.photo,
                thumbnail: mfIdTolink(thumbnailId)
              });

              countDown--;
              if(!countDown) {
                data.photos = newPhotos;

                AlbumsCollection.update({_id},
                  {
                    $set: data,
                  }
                );
              }
            });
        });
      });
    },
    'album.increment.view'(_id) {
      const theViews = parseInt(AlbumsCollection.findOne({_id}).views) + 1;
      AlbumsCollection.update({_id}, { $set: {views: theViews}});
    },
    'albums.page.count'() {
      const pageSize = 16,
            albumsCount = AlbumsCollection.find({deleted: {$ne: true}}).count(),
            remainder = albumsCount % pageSize;

      let pagesCount = parseInt(albumsCount / pageSize);

      if(remainder)
        pagesCount ++;

      return pagesCount;
    },
    'album.create'(data) {
      const suffix = parseInt(new Date().getTime() / 1000),
            theUser = Meteor.user();

      data.titleLatin = convert(`${data.title}_${suffix}`.replace(' ', '_'));

      const fileBuffer = new Buffer(data.binaryCover, 'binary'),
            _id = Random.id();

      DocsCollection.write(fileBuffer, {
        fileName: `cover-${_id}`,
        type: 'image/jpeg'
      }, (err, fileRef) => {

        data._id = _id;
        data.cover = mfIdTolink(fileRef._id);
        data.cityId = theUser.profile.city;

        delete data.binaryCover;
        delete data.previews;

        AlbumsCollection.insert(data);
      });

      return _id;
    },
    'album.add.photos'(_id, photos) {
      const suffix = parseInt(new Date().getTime() / 1000),
            data = AlbumsCollection.findOne({_id}, {fields: {title: 1, photos: 1}});

      data.titleLatin = convert(`${data.title}_${suffix}`.replace(' ', '_'));

      let countDown = photos.length;
      const newPhotos = data.photos;

      Array.prototype.forEach.call(photos, (file, index) => {
        const path = `/photodocs`,
              fileName = `${data.titleLatin}_${index}.jpg`,
              fileBuffer = new Buffer(file, 'binary');

        DocsCollection.write(fileBuffer, {
          fileName: fileName,
          type: 'image/jpeg'
        }, (err, fileRef) => {
          createThumbnail(fileName, fileRef, thumbnailId => {
            newPhotos.push({
              photo: mfIdTolink(fileRef._id),
              thumbnail: mfIdTolink(thumbnailId)
            });

            countDown--;
            if(!countDown) {
              AlbumsCollection.update({_id},
                {
                  $set: { photos: newPhotos },
                }
              );
            }
          });
        });
      });
    },
    'album.filtered.count'(filterData) {
      const filter = {};

      filter.deleted = {$ne: true};
      filter.isTop = {$ne: true};

      if(filterData.filterDate) {
        filter.date = {$gte: filterData.prevDay, $lt: filterData.nextDay}
      }

      if(filterData.filterWord) {
        filter['$or'] =
          [
            { title: {$regex: filterData.filterWord, $options: 'i' } },
            { subtitle: {$regex: filterData.filterWord, $options: 'i' } }
          ]
      }

      if(filterData.filterTag) {
        filter.tags = filterData.filterTag;
      }

      if(filterData.filterArtist) {
        filter.artistId = filterData.filterArtist;
      }

      return AlbumsCollection.find(filter, {
        fields: {
          _id: 1,
        }
      }).count();
    },
  });
}
