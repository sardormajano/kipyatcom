import { Mongo } from 'meteor/mongo';

export const CitiesCollection = new Mongo.Collection('cities');

if (Meteor.isServer) {
  Meteor.publish('cities', function(){
    return CitiesCollection.find({deleted: {$ne: true}});
  });

  Meteor.methods({
    'city.create'(data) {
      CitiesCollection.insert(data);
    },
    'city.delete'(_id) {
      CitiesCollection.update({_id}, { $set: {deleted: true}});
    },
    'city.update'(_id, data) {
      CitiesCollection.update({_id}, { $set: data});
    }
  });
}
