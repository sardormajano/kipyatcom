import { Mongo } from 'meteor/mongo';
import {DocsCollection} from '/api/docs';

const mfIdTolink = _id => {
  return `/photodocs/DocsCollection/${_id}/original/${_id}.jpg`;
}

export const UsersCollection = Meteor.users;

if (Meteor.isServer) {
  Meteor.publish('users', function(){
    if(!Roles.userIsInRole(this.userId, 'master')) return UsersCollection.find({hello: true});

    return UsersCollection.find({deleted: {$ne: true}});
  });

  Meteor.publish('user', function(_id){
    if(!Roles.userIsInRole(this.userId, 'master')) return UsersCollection.find({hello: true});

    return UsersCollection.find({
      deleted: {$ne: true},
      _id
    });
  });

  Meteor.publish('authors', function(){
    return UsersCollection.find({
      deleted: {$ne: true},
      'roles': ['contentManager']
    });
  });

  Meteor.publish('journalists', function(){
    return UsersCollection.find({
      deleted: {$ne: true},
      'roles': ['journalist'],
    }, {
      fields: {
        roles: 1,
        'profile.cover': 1,
        'profile.first_name': 1,
        'profile.last_name': 1,
      }
    });
  });

  Meteor.publish('journalist', function(_id){
    return UsersCollection.find({
      _id
    }, {
      fields: {
        roles: 1,
        'profile.cover': 1,
        'profile.first_name': 1,
        'profile.last_name': 1,
      }
    });
  });

  Meteor.publish('roles', function() {
    if(!Roles.userIsInRole(this.userId, 'master')) return Roles.find({hello: true});

    return Roles.getAllRoles();
  });

  Meteor.methods({
    'user.changeCity'(_id, cityId) {
      if(!Meteor.user().roles.length) return;

      UsersCollection.update({_id}, { $set: {
          'profile.city': cityId
        }
      });
    },
    'user.create'(data, role) {
      const theUser = Meteor.user();

      if(!Roles.userIsInRole(theUser._id, 'master')) return 'fuck off!';

      const fileBuffer = new Buffer(data.binaryCover, 'binary'),
            _id = Random.id();

      DocsCollection.write(fileBuffer, {
        fileName: `cover-${_id}`,
        type: 'image/jpeg'
      }, (err, fileRef) => {

        data.cover = mfIdTolink(fileRef._id);
        data.profile = {
          createdAt: Date.parse(new Date()),
          createdBy: theUser._id,
          first_name: data.name,
          last_name: data.surname,
          company: 'kipyatcom',
          cover: data.cover,
          cover2: data.cover2
        };

        delete data.binaryCover;
        delete data.cover;
        delete data.cover2;
        delete data.name;
        delete data.surname;
        delete data.passwordConfirmation;

        let tuser = Accounts.createUser(data);
        Roles.addUsersToRoles(tuser, data.role);
      });
    },
    'user.delete'(_id) {
      if(!Roles.userIsInRole(Meteor.userId(), 'master')) return;

      UsersCollection.update({_id}, { $set: {deleted: true}});
    },
    'user.update'(_id, data) {
      const theUser = Meteor.user();

      if(!Roles.userIsInRole(theUser._id, 'master')) return;

      if(!data.binaryCover) {
        data.emails = [{address: data.email}]
        data.profile = {
          createdAt: Date.parse(new Date()),
          createdBy: theUser._id,
          first_name: data.name,
          last_name: data.surname,
          company: 'kipyatcom',
          cover: data.cover,
          cover2: data.cover2
        };

        delete data._id;
        delete data.name;
        delete data.surname;
        delete data.passwordConfirmation;

        console.log(data);

        UsersCollection.update({_id}, { $set: data });

        Roles.setUserRoles(_id, data.role);
        data.password !== 'no-password' && Accounts.setPassword(_id, data.password);

        return ;
      }

      const fileBuffer = new Buffer(data.binaryCover, 'binary');

      DocsCollection.write(fileBuffer, {
        fileName: `cover-${_id}`,
        type: 'image/jpeg'
      }, (err, fileRef) => {

        data.cover = mfIdTolink(fileRef._id);
        data.emails = [{address: data.email}];
        data.profile = {
          createdAt: Date.parse(new Date()),
          createdBy: theUser._id,
          first_name: data.name,
          last_name: data.surname,
          company: 'kipyatcom',
          cover: data.cover,
          cover2: data.cover2
        };

        delete data._id;
        delete data.binaryCover;
        delete data.email;
        delete data.cover;
        delete data.cover2;
        delete data.name;
        delete data.surname;
        delete data.passwordConfirmation;

        UsersCollection.update({_id}, { $set: data });
        Roles.setUserRoles(_id, data.role);

        data.password !== 'no-password' && Accounts.setPassword(_id, data.password);
        return ;
      });
    },
    'user.changePassword'(_id, data) {
      if(!Roles.userIsInRole(Meteor.userId(), 'masterAdmin')) return;

      UsersCollection.update({_id}, { $set: data});
    }
  });
}
