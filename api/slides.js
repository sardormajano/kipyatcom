import { Mongo } from 'meteor/mongo';

import {DocsCollection} from '/api/docs';

export const SlidesCollection = new Mongo.Collection('slides');

const mfIdTolink = _id => {
  return `/photodocs/DocsCollection/${_id}/original/${_id}.jpg`;
}

const mfLinkToId = link => {
  return link.split('/')[3];
}

if (Meteor.isServer) {
  Meteor.publish('slides', function(){
    return SlidesCollection.find({deleted: {$ne: true}});
  });

  Meteor.publish('slides.home', function(){
    return SlidesCollection.find({
        deleted: {$ne: true}
      },
      {
        sort: {createdAt: -1}
      }
    );
  });

  Meteor.publish('slide', function(_id){
    return SlidesCollection.find({deleted: {$ne: true}, _id});
  });

  Meteor.methods({
    'slide.create'(data) {
      const fileBuffer = new Buffer(data.binaryCover, 'binary'),
            _id = Random.id(),
            theUser = Meteor.user();

      DocsCollection.write(fileBuffer, {
        fileName: `cover-${_id}`,
        type: 'image/jpeg'
      }, (err, fileRef) => {

        data.cover = mfIdTolink(fileRef._id);
        data.cityId = theUser.profile.city;

        delete data.binaryCover;

        SlidesCollection.insert(data);
      });
    },
    'slide.delete'(_id) {
      SlidesCollection.update({_id}, { $set: {deleted: true}});
    },
    'slide.update'(_id, data) {
      const fileBuffer = new Buffer(data.binaryCover, 'binary'),
            theUser = Meteor.user();

      DocsCollection.write(fileBuffer, {
        fileName: `cover-${_id}`,
        type: 'image/jpeg'
      }, (err, fileRef) => {

        data.cover = mfIdTolink(fileRef._id);
        data.cityId = theUser.profile.city;

        delete data.binaryCover;

        SlidesCollection.update({_id}, { $set: data});
      });
    }
  });
}
