import { Mongo } from 'meteor/mongo';

export const ArtistsCollection = new Mongo.Collection('artists');

if (Meteor.isServer) {
  Meteor.publish('artists', function(){
    return ArtistsCollection.find({deleted: {$ne: true}});
  });

  Meteor.publish('artist', function(_id){
    return ArtistsCollection.find({deleted: {$ne: true}, _id});
  });

  Meteor.methods({
    'artist.create'(data) {
      data.createdAt = new Date();
      data.createdBy = Meteor.userId();

      ArtistsCollection.insert(data);
    },
    'artist.delete'(_id) {
      ArtistsCollection.update({_id}, { $set: {deleted: true}});
    },
    'artist.update'(_id, data) {
      // console.log(_id);
      ArtistsCollection.update({_id}, { $set: data});
    }
  });
}
