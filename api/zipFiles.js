import { Mongo } from 'meteor/mongo';

export const ZipFilesCollection = new Mongo.Collection('zipFiles');

if (Meteor.isServer) {
  Meteor.publish('zipFiles', function(){
    return ZipFilesCollection.find({deleted: {$ne: true}});
  });

  Meteor.publish('zipFile', function(_id){
    return ZipFilesCollection.find({deleted: {$ne: true}, _id});
  });

  Meteor.methods({
    'zipFile.update'(_id, data) {
      ZipFilesCollection.update({_id}, { $set: data});
    }
  });
}
