import { Mongo } from 'meteor/mongo';

import {DocsCollection} from '/api/docs';
import convert from 'cyrillic-to-latin';

export const ArticlesCollection = new Mongo.Collection('articles');

const mfIdTolink = _id => {
  return `/photodocs/DocsCollection/${_id}/original/${_id}.jpg`;
}

if (Meteor.isServer) {
  function writeBuffer(binary) {
    const fileBuffer = new Buffer(binary, 'binary'),
          _id = Random.id(),
          options = {
            fileName: `cover-${_id}`,
            type: 'image/jpeg'
          };

    return new Promise((resolve, reject) => {
      DocsCollection.write(fileBuffer, options, (err, fileRef) => {
        if(err) {
          reject(err);
        }
        else {
          resolve(mfIdTolink(fileRef._id));
        }
      });
    });
  }

  Meteor.publish('articles.all', (number, filterData, cityId) => {
    if(!filterData) {
      return ArticlesCollection.find({
        deleted: {$ne: true}, hidden: {$ne: true},
        isTop: {$ne: true}
      }, {
        fields: {
          photos: 0,
          thumbnails: 0,
          previews: 0,
          contentEnd: 0,
          contentStart: 0
        },
        sort: {date: -1},
        limit: number
      });
    }
    else {
      const filter = {};

      filter.deleted = {$ne: true};
      filter.isTop = {$ne: true};

      if(filterData.filterDate) {
        filter.date = {$gte: filterData.prevDay, $lt: filterData.nextDay}
      }

      if(filterData.filterWord) {
        filter['$or'] =
          [
            { title: {$regex: filterData.filterWord, $options: 'i' } },
            { subtitle: {$regex: filterData.filterWord, $options: 'i' } }
          ]
      }

      if(filterData.filterTag) {
        filter.tags = filterData.filterTag;
      }

      return ArticlesCollection.find(filter, {
        fields: {
          contentStart: 0,
          contentEnd: 0,
          photos: 0,
          photosToDelete: 0,
          videoCode: 0
        },
        sort: {date: -1},
        limit: number
      });
    }
  });

  Meteor.publish('articles', function(){
    return ArticlesCollection.find(
      {
        deleted: {$ne: true}
      }, {
        sort: {date: -1},
        fields: {
          cover: 1,
          title: 1,
          subtitle: 1
        }
      });
  });

  Meteor.publish('articles.home', function(){
    return ArticlesCollection.find({
        deleted: {$ne: true}, hidden: {$ne: true},
        isMain: true
      },
      {
        sort: {createdAt: -1},
        limit: 1
      }
    );
  });

  Meteor.publish('articles.top', function(){
    return ArticlesCollection.find({
        deleted: {$ne: true}, hidden: {$ne: true},
        isTop: true
      },
      {
        fields: {
          contentStart: 0,
          contentEnd: 0,
          photos: 0,
          photosToDelete: 0,
          videoCode: 0
        },
        sort: {createdAt: -1},
        limit: 6
      }
    );
  });

  Meteor.publish('article', function(_id){
    return ArticlesCollection.find({
      deleted: {$ne: true}, _id
    }, {
      fields: {
        binaryCover: 0,
        photosToDelete: 0,
      }
    });
  });

  Meteor.publish('article.to.compare.positions', function() {
    return ArticlesCollection.find({deleted: {$ne: true}, hidden: {$ne: true}}, {
      fields: {
        mainPosition: 1,
        topPosition: 1,
      },
    });
  });

  Meteor.methods({
    'article.toggle.visibility'(_id) {
      const theArticle = ArticlesCollection.findOne(_id);

      ArticlesCollection.update({_id}, { $set: {hidden: !theArticle.hidden}});
    },
    'article.create'(data) {
      function createArticle(countDown) {
        if(countDown)
          return ;

        delete data.binaryPhotos;
        delete data.binaryCover;

        ArticlesCollection.insert(data);
      }

      let countDown = 6;

      const suffix = parseInt(new Date().getTime() / 1000);
      data.titleLatin = convert(`${data.title}_${suffix}`.replace(' ', '_'));

      if(data.binaryCover) {
        writeBuffer(data.binaryCover)
        .then(result => {
          data.cover = result;
          createArticle(--countDown);
        });
      } else {
        createArticle(--countDown);
      }

      if(data.binaryPhotos) {
        data.photos = [];

        data.binaryPhotos.forEach(bPhoto => {
          if(!bPhoto) {
            createArticle(--countDown);
            return ;
          }

          writeBuffer(bPhoto)
          .then(result => {
            data.photos.push(result);
            createArticle(--countDown);
          });
        });
      } else {
        createArticle(--countDown);
      }

      return true;
    },
    'article.delete'(_id) {
      ArticlesCollection.update({_id}, { $set: {deleted: true}});
    },
    'article.update'(_id, data) {
      function updateArticle(countDown) {
        if(countDown)
          return ;

        delete data.binaryPhotos;
        delete data.binaryCover;
        ArticlesCollection.update({_id}, { $set: data});
      }

      let countDown = 6;

      const suffix = parseInt(new Date().getTime() / 1000);
      data.titleLatin = convert(`${data.title}_${suffix}`.replace(' ', '_'));

      if(data.binaryCover) {
        writeBuffer(data.binaryCover)
        .then(result => {
          data.cover = result;
          updateArticle(--countDown);
        });
      } else {
        updateArticle(--countDown);
      }

      if(data.binaryPhotos) {
        data.binaryPhotos.forEach(bPhoto => {
          if(!bPhoto) {
            updateArticle(--countDown);
            return ;
          }

          writeBuffer(bPhoto)
          .then(result => {
            data.photos.push(result);
            updateArticle(--countDown);
          });
        });
      } else {
        updateArticle(--countDown);
      }

      return true;
    },
    'article.filtered.count'(filterData) {
      const filter = {};

      filter.deleted = {$ne: true};
      filter.isTop = {$ne: true};

      if(filterData.filterDate) {
        filter.date = {$gte: filterData.prevDay, $lt: filterData.nextDay}
      }

      if(filterData.filterWord) {
        filter['$or'] =
          [
            { title: {$regex: filterData.filterWord, $options: 'i' } },
            { subtitle: {$regex: filterData.filterWord, $options: 'i' } }
          ]
      }

      if(filterData.filterTag) {
        filter.tags = filterData.filterTag;
      }

      return ArticlesCollection.find(filter, {
        fields: {
          _id: 1,
        }
      }).count();
    },
  });
}
