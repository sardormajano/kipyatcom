import { Mongo } from 'meteor/mongo';

export const ContactsCollection = new Mongo.Collection('contacts');

if (Meteor.isServer) {
  Meteor.publish('contacts', function(){
    return ContactsCollection.find({deleted: {$ne: true}});
  });

  Meteor.methods({
    'contact.create'(data) {
      ContactsCollection.insert(data);
    },
    'contact.delete'(_id) {
      if(_id === '0')
        return ;
        
      ContactsCollection.update({_id}, { $set: {deleted: true}});
    },
    'contact.update'(_id, data) {
      ContactsCollection.update({_id}, { $set: data});
    }
  });
}
