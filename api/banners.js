import { Mongo } from 'meteor/mongo';

export const BannersCollection = new Mongo.Collection('banners');

if (Meteor.isServer) {
  Meteor.publish('banners', function(){
    return BannersCollection.find({deleted: {$ne: true}});
  });

  Meteor.publish('banner', function(_id){
    return BannersCollection.find({deleted: {$ne: true}, _id});
  });

  Meteor.methods({
    'banner.update'(_id, data) {
      BannersCollection.update({_id}, { $set: data}, {upsert: true});
    }
  });
}
