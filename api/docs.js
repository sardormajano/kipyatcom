import { FilesCollection } from 'meteor/ostrio:files';
import sharp from 'meteor/c9s:sharp';
import fs from 'fs';

export const DocsCollection = new FilesCollection({
  storagePath: '/photodocs',
  downloadRoute: '/photodocs',
  permissions: 0774,
	parentDirPermissions: 0774,
  collectionName: 'DocsCollection',
  allowClientCode: false, // Disallow remove files from Client
  onBeforeUpload(file) {
    return true;
  }
});

if(Meteor.isServer) {
  Meteor.publish('docs.all', function () {
    return DocsCollection.find().cursor;
  });

  Meteor.methods({
    'thumbnail.this.shit'(imgId) {
      fs.writeFileSync(`${process.env.PWD}/public/thumbnails/${imgId}-thumbnail.jpg`, new Buffer([0x62]));
      sharp(`/photodocs/${imgId}.jpg`)
        .resize(200)
        .toFile(`${process.env.PWD}/public/thumbnails/${imgId}-thumbnail.jpg`);
    }
  });
}
