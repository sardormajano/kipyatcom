import { Mongo } from 'meteor/mongo';

export const TagsCollection = new Mongo.Collection('tags');

if (Meteor.isServer) {
  Meteor.publish('tags', function(category){
    if(category === undefined)
      return TagsCollection.find({deleted: {$ne: true}});

    return TagsCollection.find({deleted: {$ne: true}, category});
  });

  Meteor.methods({
    'tag.create'(data) {
      data.createdAt = new Date();
      data.createdBy = Meteor.userId();

      TagsCollection.insert(data);
    },
    'tag.delete'(_id) {
      TagsCollection.update({_id}, { $set: {deleted: true}});
    },
    'tag.update'(_id, data) {
      TagsCollection.update({_id}, { $set: data});
    }
  });
}
