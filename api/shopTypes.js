import { Mongo } from 'meteor/mongo';

export const ShopTypesCollection = new Mongo.Collection('shopTypes');

if (Meteor.isServer) {
  Meteor.publish('shopTypes', function(){
    return ShopTypesCollection.find({deleted: {$ne: true}});
  });

  Meteor.methods({
    'shopType.create'(data) {
      ShopTypesCollection.insert(data);
    },
    'shopType.delete'(_id) {
      ShopTypesCollection.update({_id}, { $set: {deleted: true}});
    },
    'shopType.update'(_id, data) {
      ShopTypesCollection.update({_id}, { $set: data});
    }
  });
}
