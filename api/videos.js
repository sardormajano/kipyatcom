import { Mongo } from 'meteor/mongo';

import {DocsCollection} from '/api/docs';
import convert from 'cyrillic-to-latin';

const mfIdTolink = _id => {
  return `/photodocs/DocsCollection/${_id}/original/${_id}.jpg`;
}

export const VideosCollection = new Mongo.Collection('videos');

if (Meteor.isServer) {
  Meteor.publish('videos', function(){
    return VideosCollection.find({
      deleted: {$ne: true},
    }, {
      sort: {date: -1}
    });
  });

  Meteor.publish('videos.all', (number) => {
    return VideosCollection.find({
      deleted: {$ne: true},
      isTop: {$ne: true}
    }, {
      sort: {date: -1},
      limit: number,
    });
  });

  Meteor.publish('videos.home', function(){
    return VideosCollection.find({
        deleted: {$ne: true},
        isMain: true
      },
      {
        sort: {createdAt: -1},
        limit: 1
      }
    );
  });

  Meteor.publish('videos.top', function(){
    return VideosCollection.find({
        deleted: {$ne: true},
        isTop: true
      },
      {
        sort: {createdAt: -1},
      }
    );
  });

  Meteor.publish('video', function(_id){
    return VideosCollection.find({deleted: {$ne: true}, _id});
  });

  Meteor.publish('videos.to.compare.positions', function() {
    return VideosCollection.find({deleted: {$ne: true}}, {
      fields: {
        mainPosition: 1,
        topPosition: 1,
      },
    });
  });

  Meteor.methods({
    'video.create'(data) {
      const suffix = parseInt(new Date().getTime() / 1000);

      data.titleLatin = convert(`${data.title}_${suffix}`.replace(' ', '_'));

      const fileBuffer = new Buffer(data.binaryCover, 'binary'),
            _id = Random.id();

      DocsCollection.write(fileBuffer, {
        fileName: `cover-${_id}`,
        type: 'image/jpeg'
      }, (err, fileRef) => {

        data._id = _id;
        data.cover = mfIdTolink(fileRef._id);
        delete data.binaryCover;

        VideosCollection.insert(data);
      });

      return _id;
    },
    'video.delete'(_id) {
      VideosCollection.update({_id}, { $set: {deleted: true}});
    },
    'video.update'(_id, data) {
      VideosCollection.update({_id}, { $set: data});
    }
  });
}
