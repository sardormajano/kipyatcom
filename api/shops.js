import { Mongo } from 'meteor/mongo';

import { DocsCollection } from '../api/docs';

export const ShopsCollection = new Mongo.Collection('shops');

const mfIdTolink = _id => {
  return `/photodocs/DocsCollection/${_id}/original/${_id}.jpg`;
}

const mfLinkToId = link => {
  return link.split('/')[3];
}

if (Meteor.isServer) {
  function writeBuffer(binary) {
    const fileBuffer = new Buffer(binary, 'binary'),
          _id = Random.id(),
          options = {
            fileName: `cover-${_id}`,
            type: 'image/jpeg'
          };

    return new Promise((resolve, reject) => {
      DocsCollection.write(fileBuffer, options, (err, fileRef) => {
        if(err) {
          reject(err);
        }
        else {
          resolve(mfIdTolink(fileRef._id));
        }
      });
    });
  }

  Meteor.publish('shops', function(){
    return ShopsCollection.find({deleted: {$ne: true}, hidden: {$ne: true},});
  });

  Meteor.publish('shop.names', function(){
    return ShopsCollection.find({
        deleted: {$ne: true}, hidden: {$ne: true},
      },
      {
        sort: {
          createdAt: -1,
        },
        fields: {
          name: 1
        }
      }
    );
  });

  Meteor.publish('shops.top', function(){
    return ShopsCollection.find({
        deleted: {$ne: true}, hidden: {$ne: true},
        isMain: true,
      },
      {
        sort: {createdAt: -1},
        fields: {
          cover2: 0
        }
      }
    );
  });

  Meteor.publish('shops.home', function(){
    return ShopsCollection.find({
        deleted: {$ne: true}, hidden: {$ne: true},
        isMain: true,
      },
      {
        sort: {createdAt: -1},
        fields: {
          isMain: 1,
          position: 1,
          cover2: 1
        }
      }
    );
  });

  Meteor.publish('shop', function(_id){
    return ShopsCollection.find({deleted: {$ne: true}, _id});
  });

  Meteor.publish('shops.all', (number) => {
    return ShopsCollection.find({
      deleted: {$ne: true},
      hidden: {$ne: true},
      isMain: {$ne: true}
    },
    {
      sort: {
        name: 1
      },
      limit: number
    });
  });

  Meteor.publish('shops.by.page', function(page){
    const pageSize = 8;

    return ShopsCollection.find({
        deleted: {$ne: true}
      },
      {
        sort: {name: -1},
        skip: pageSize * (page - 1),
        limit: pageSize
      }
    );
  });

  Meteor.methods({
    'shop.toggle.visibility'(_id) {
      const theShop = ShopsCollection.findOne(_id);

      ShopsCollection.update({_id}, { $set: {hidden: !theShop.hidden}});
    },
    'shop.create'(data) {
      let countDown = 2;

      function createShop(countDown) {
        if(countDown)
          return ;

        delete data.binaryCover;
        delete data.binaryCover2;
        ShopsCollection.insert(data);
      }

      if(data.binaryCover) {
        writeBuffer(data.binaryCover)
          .then(response => {
            data.cover = response;
            createShop(--countDown);
          });
      }
      else {
        createShop(--countDown);
      }
      if(data.binaryCover2) {
        writeBuffer(data.binaryCover2)
        .then(response => {
          data.cover2 = response;
          createShop(--countDown);
        });
      }
      else {
        createShop(--countDown);
      }

      return true;
    },
    'shop.delete'(_id) {
      ShopsCollection.update({_id}, { $set: {deleted: true}});
    },
    'shop.update'(_id, data) {
      let countDown = 2;

      function updateShop(countDown) {
        if(countDown)
          return ;

        delete data.binaryCover;
        delete data.binaryCover2;
        ShopsCollection.update({_id}, { $set: data});
      }

      if(data.binaryCover) {
        writeBuffer(data.binaryCover)
          .then(response => {
            data.cover = response;
            updateShop(--countDown);
          });
      }
      else {
        updateShop(--countDown);
      }
      if(data.binaryCover2) {
        writeBuffer(data.binaryCover2)
        .then(response => {
          data.cover2 = response;
          updateShop(--countDown);
        });
      }
      else {
        updateShop(--countDown);
      }

      return true;
    },
    'shops.page.count'() {
      const pageSize = 8,
            shopsCount = ShopsCollection.find({deleted: {$ne: true}}).count(),
            remainder = shopsCount % pageSize;

      let pagesCount = parseInt(shopsCount / pageSize);

      if(remainder)
        pagesCount ++;

      return pagesCount;
    },
  });
}
