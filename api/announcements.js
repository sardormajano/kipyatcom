import { Mongo } from 'meteor/mongo';

export const AnnouncementsCollection = new Mongo.Collection('announcements');

if (Meteor.isServer) {
  Meteor.publish('announcements', function(){
    return AnnouncementsCollection.find({deleted: {$ne: true}});
  });

  Meteor.publish('announcements.home', function(){
    return AnnouncementsCollection.find({
        deleted: {$ne: true}
      },
      {
        sort: {createdAt: -1},
        limit: 2
      }
    );
  });

  Meteor.publish('announcement', function(_id){
    return AnnouncementsCollection.find({deleted: {$ne: true}, _id});
  });

  Meteor.methods({
    'announcement.create'(data) {
      AnnouncementsCollection.insert(data);
    },
    'announcement.delete'(_id) {
      AnnouncementsCollection.update({_id}, { $set: {deleted: true}});
    },
    'announcement.update'(_id, data) {
      AnnouncementsCollection.update({_id}, { $set: data});
    }
  });
}
