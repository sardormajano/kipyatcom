import { Meteor } from 'meteor/meteor';

import { AlbumsCollection } from '../api/albums';
import { BannersCollection } from '../api/banners';
import { CitiesCollection } from '../api/cities';
import { ContactsCollection } from '../api/contacts';
import { DocsCollection } from '../api/docs';
import { ShopTypesCollection } from '../api/shopTypes';
import { TagsCollection } from '../api/tags';
import { ZipFilesCollection } from '../api/zipFiles';

import Fiber from 'fibers';
import fetch from 'node-fetch';

import fs from 'fs';

const fetchInsta = callback => {
  fetch('https://www.instagram.com/kipyatcom/media/')
    .then(function(res) {
        return res.json();
    }).then(function(json) {
        callback('', json);
    });
}

const mfLinkToId = link => {
  return link.split('/')[3];
}

const headForAdmin = `
  <title>Кипятком. Администратор</title>
  <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/bootstrap/css/bootstrap-datetimepicker.css" />
  <link rel="stylesheet" href="/bootstrap/css/select2.min.css" />
  <link rel="stylesheet" href="/bootstrap/css/select2-bootstrap.css" />
  <link rel="stylesheet" href="/cropper/cropper.min.css" />

  <script src="/bootstrap/js/jquery.js"></script>
  <script src="/js/jquery-ui/jquery-ui.min.js"></script>
  <script src="/bootstrap/js/moment.js"></script>
  <script src="/bootstrap/js/bootstrap-datetimepicker.js"></script>
  <script src="/bootstrap/js/bootstrap.min.js"></script>
  <script src="/bootstrap/js/bootstrap-confirmation.min.js"></script>
  <script src="/bootstrap/js/select2.full.js"></script>
  <script src="/js/jquery.dotdotdot.min.js" charset="utf-8"></script>
  <script src="/ckeditor/ckeditor.js"></script>
  <script src="/cropper/cropper.min.js"></script>

`,
    headForClient = `
  <title>Кипятком</title>
  <!-- CSS -->
  <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="/js/menu-mob/css/menu-mob.css" />
  <link rel="stylesheet" href="/css/kipyatcom.css"/>
  <link rel="stylesheet" href="/css/style.css"/>
  <link rel="stylesheet" href="/css/bootstrap.css" />

  <link href="/css/animate.css" rel="stylesheet" />
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicon/apple-touch-icon-144x144.png" />
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicon/apple-touch-icon-152x152.png" />
  <link rel="icon" type="image/png" href="/favicon/favicon-32x32.png" sizes="32x32" />
  <link rel="icon" type="image/png" href="/favicon/favicon-16x16.png" sizes="16x16" />

  <meta name="application-name" content="kipyat.com"/>
  <meta name="msapplication-TileColor" content="#" />

  <link href="/photoswipe/photoswipe.css" rel="stylesheet" />
  <link href="/photoswipe/default-skin/default-skin.css" rel="stylesheet" />

  <script src="/js-slider-1/jquery-1.9.1.min.js"></script>
  <script src="/js-slider-1/jquery.glide.js"></script>
  <script src="/js-slider-1/jquery.elastislide.js"></script>
  <script src="/bootstrap/js/select2.full.js"></script>
  <script src="http://lightwidget.com/widgets/lightwidget.js"></script>
  <script src="/js/jquery.dotdotdot.min.js" charset="utf-8"></script>
  <script src="/photoswipe/photoswipe.js"></script>
  <script src="/photoswipe/photoswipe-ui-default.js"></script>
  <script src="/js/modal-window.js"></script>

  <link href="/datestyle/datedropper.css" rel="stylesheet" type="text/css" />
  <script src="/datestyle/datedropper.js"></script>
`;

Meteor.methods({
  'fetch.insta'() {
    const syncFetchInsta =  Meteor.wrapAsync(fetchInsta);

    return syncFetchInsta();
  },
  'zip.album'(_id) {
    const theAlbum = AlbumsCollection.findOne(_id),
          fileZip = new JSZip(),
          fileId = ZipFilesCollection.findOne({_id: '0'}).fileId,
          zipFileName = `/photodocs/${fileId}.zip`;

    theAlbum.photos.forEach(photo => {
      fileZip.file(`${mfLinkToId(photo.photo || photo)}.jpg`, fs.readFileSync(`/photodocs/${mfLinkToId(photo.photo || photo)}.jpg`));
    });

    const syncSaveAs = Meteor.wrapAsync(fileZip.saveAs.bind(fileZip, zipFileName));

    syncSaveAs();

    return fileId;
  }
});

Meteor.startup(() => {
  if ( Meteor.users.find().count() <= 1 ) {
    const defaultUsers = [
      {
          username: 'master',
          email: 'master@gmail.com',
          password: 'master',
          profile: {
              first_name: 'Master',
              last_name: 'Master',
              company: 'Kipyatcom',
          }
      },
      {
          username: 'contentManager',
          email: 'contentManager@gmail.com',
          password: 'contentManager',
          profile: {
              first_name: 'Content',
              last_name: 'Manager',
              company: 'Kipyatcom',
          }
      },
      {
          username: 'journalist',
          email: 'journalist@gmail.com',
          password: 'journalist',
          profile: {
              first_name: 'Journalist',
              last_name: 'Journalist',
              company: 'Kipyatcom',
          }
      }
    ];

    defaultUsers.forEach(user => {
      let tuser = Accounts.createUser(user);
      Roles.addUsersToRoles(tuser, user.username);
    });
  }

  if(ContactsCollection.find().count() === 0) {
    let cContacts = ContactsCollection.insert({
      cityId: '0',
      domain: 'kipyat.com',
      email: 'astana@kipyat.com',
      phone: '+7 701 227 09 88',
      insta: 'instagram.com/kipyatcom'
    });
  }

  if (CitiesCollection.find().count() === 0) {
    let cCity = CitiesCollection.insert({_id: '0', name: "Астана"});
  }

  if (ShopTypesCollection.find().count() === 0) {
    const types = [
      {name: 'Рестораны'},
      {name: 'Клубы'},
      {name: 'Бары,Кафе'},
      {name: 'Летние террасы'},
      {name: 'Караоке'},
      {name: 'Кофейни'},
      {name: 'Другое'},
    ];

    types.forEach(item => ShopTypesCollection.insert(item));
  }

  if (BannersCollection.find().count() === 0) {
    const banners = [
      {_id: '1-1', name: 'Баннер 1-1', photos: [], links: []},
      {_id: '1-2', name: 'Баннер 1-2', photos: [], links: []},
      {_id: '2-1', name: 'Баннер 2-1', photos: [], links: []},
      {_id: '2-2', name: 'Баннер 2-2', photos: [], links: []},
      {_id: 'a', name: 'Баннер "А"', photos: [], links: []},
      {_id: '3-1', name: 'Баннер 3-1', photos: [], links: []},
      {_id: '3-2', name: 'Баннер 3-2', photos: [], links: []},
      {_id: 'b', name: 'Баннер "B"', photos: [], links: []},
      {_id: '4-1', name: 'Баннер 4-1', photos: [], links: []},
      {_id: '4-2', name: 'Баннер 4-2', photos: [], links: []},
      {_id: '5-1', name: 'Баннер 5-1', photos: [], links: []},
      {_id: '5-2', name: 'Баннер 5-2', photos: [], links: []},
    ];

    banners.forEach(item => BannersCollection.insert(item));
  }

  if (TagsCollection.find({name: 'ночной дозор'}).count() === 0) {
    TagsCollection.insert({name: 'ночной дозор', category: 1});
    TagsCollection.insert({name: 'ночной дозор', category: 3});
    TagsCollection.insert({name: 'ночной дозор', category: 0});
    TagsCollection.insert({name: 'ночной дозор', category: 4});
  }

  if (TagsCollection.find({name: 'светская хроника'}).count() === 0) {
    TagsCollection.insert({name: 'светская хроника', category: 1});
    TagsCollection.insert({name: 'светская хроника', category: 3});
    TagsCollection.insert({name: 'светская хроника', category: 0});
    TagsCollection.insert({name: 'светская хроника', category: 4});
  }

  if (ZipFilesCollection.find({_id: '0'}).count() === 0) {
    DocsCollection.write(new Buffer('hello', 'binary'), {
      fileName: 'album.zip',
      type: 'application/zip'
    }, (error, fileRef) => {
      ZipFilesCollection.insert({
        _id: '0',
        fileId: fileRef._id
      });
    });
  }

  WebApp.connectHandlers.use(function(req, res, next) {
    Fiber(function(){
      if(req.originalUrl.includes('admin') || req.originalUrl.includes('login')) {
        req.dynamicHead = headForAdmin;
      }
      else {
        req.dynamicHead = headForClient;
      }
      next();
    }).run();
  });
});
